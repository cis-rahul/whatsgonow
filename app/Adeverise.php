<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adeverise extends Model
{
    protected $table = "wgn_advertise";
    
    protected $fillable = [
        'ads_images','ads_link','created_at', 'updated_at'
    ];
}
