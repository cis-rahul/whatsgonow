<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    protected $table = "wgn_item";
    
    
    
    protected $casts = [
        'created_at' => 'date',
    ];
    
}
