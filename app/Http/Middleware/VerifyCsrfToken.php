<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'notify-url','cancel-url','success-url','admin/users-data','admin/change-img-link','contact','admin/final-cahnge-lang','admin/add-category','admin/edit-categories'
    ];
}
