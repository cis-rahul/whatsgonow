<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Session;
use Illuminate\Support\Facades\Config;

class LanguageSwitcher {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $language=  Session::get('localei'); 
        $locale = Session::has('localei') ? $language : 'en';
        App::setLocale($locale);
        return $next($request);
    }

}
