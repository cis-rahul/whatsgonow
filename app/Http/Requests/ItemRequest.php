<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */    
    
    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null) {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
//        echo "<pre>";
//        print_r($_POST);die;
    }
    
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {         
        $action = $_POST['actionType'];
        
        $rules = [
            'Create' => [
                'item_image1'=>'required_without_all:item_image2,item_image3,item_image4',
                'item_image2'=>'required_without_all:item_image1,item_image3,item_image4',
                'item_image3' => 'required_without_all:item_image2,item_image1,item_image4',
                'item_image4' => 'required_without_all:item_image2,item_image3,item_image1',                
                'item_length' => 'numeric',                
                'item_height' => 'numeric',
                'item_width' =>  'numeric',
                'item_weight' => 'numeric',
                'item_pieces' => 'required',
                'category_id' => 'required',
                'item_description' => 'required',
                'item_value' => 'required|numeric',
                'currency_type' => 'required',
                'from_postal_code' => 'required|numeric',
                'from_city' => 'required',
                'from_country' => 'required',            
                'to_postal_code' => 'required|numeric',
                'to_city' => 'required',
                'to_country' => 'required',
                'item_currency' => 'required',
                'offer_type' => 'required',
                'car_type' => 'required',
                'item_price'=>'required',
                'product_name'=>'required',
                'seller_email'=>'required',
                'from_street'=>'required',
                'to_street'=>'required',            
                'seller_phone_number' => 'required'
            ],            
            'Edit' => [                                
                'item_length' => 'numeric',                
                'item_height' => 'numeric',
                'item_width'  => 'numeric',
                'item_weight' => 'numeric',
                'item_pieces' => 'required',
                'category_id' => 'required',
                'item_description' => 'required',
                'item_value' => 'required|numeric',
                'currency_type' => 'required',
                'from_postal_code' => 'required|numeric',
                'from_city' => 'required',
                'from_country' => 'required',            
                'to_postal_code' => 'required|numeric',
                'to_city' => 'required',
                'to_country' => 'required',
                'item_currency' => 'required',
                'offer_type' => 'required',
                'car_type' => 'required',
                'item_price'=>'required',
                'product_name'=>'required',
                'seller_email'=>'required',
                'from_street'=>'required',
                'to_street'=>'required',            
                'seller_phone_number' => 'required'
            ],            
        ];        
        return $rules[$action];
    }    
}
