<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TripRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    
    
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trip_from'=>'required',
            'trip_to' => 'required',
            'trip_date' => 'required',
            'trip_time' => 'required',
            'city_from' => 'required',
            'city_to' => 'required',
            'country_from' => 'required',
            'country_to' => 'required',
            'postalcode_from' => 'required',
            'postalcode_to' => 'required',                        
        ];
    }
}
