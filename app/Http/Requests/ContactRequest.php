<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContactRequest extends Request {
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'cname' => 'required',
            'cemail' => 'required|email',
            'cphone' => 'required|min:10|numeric',
            'comment' => 'required',
        ];
    }

    public function messages() {
        return [
            'cname.required' => 'Name Field Required.',
            'cemail.required' => 'Email Field Required.',
            'cemail.email' => 'Please Enter Valid Email.',
            'cphone.required' => 'Phone Email Required.',
            'cphone.numeric' => 'Phone Number Must Be Numeric.',
        ];
    }

}
