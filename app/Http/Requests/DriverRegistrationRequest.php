<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DriverRegistrationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'=>'required',
            'lastname'=>'required',            
            'firstname' => 'required',
            'lastname' => 'required', 
            'userimage' => 'required|mimes:jpeg,bmp,png,jpg',            
            'postalcode'=>'required|numeric',
            'city' => 'required',
            'street'=>'required',
            'housenumber' => 'required',
            'country' => 'required',            
            'licenceno' => 'required',
            'licenceissueplace' => 'required',
            'licenceissuedt' => 'required',
            'identificationmethod'=>'required',
            'paypal_email_id' => 'required|email',
            'dob' => 'required'
        ];
    }
}
