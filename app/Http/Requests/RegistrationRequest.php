<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegistrationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function __construct(){
        
    }
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'=>'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:3|confirmed',
            'password_confirmation' => 'required|min:3',
            'firstname' => 'required',
            'lastname' => 'required', 
            'userimage' => 'required|mimes:jpeg,bmp,png,jpg',
            'phone' => 'required|numeric',
            'postalcode'=>'required|numeric',
            'city' => 'required|required',
            'country' => 'required|required',
        ];
    }
    
}