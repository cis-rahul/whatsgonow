<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', ['uses'=>'HomeController@index', 'as' => 'home']);
Route::post('/switch', [function() {$t = $_POST['localei'];$currURl = trim($_POST['currURl']);Session::put('localei', $t);App::setLocale($t);return Redirect::route($currURl);}, 'as' => 'user.mylang']);
Route::get('about', ['uses'=>'HomeController@about', 'as' => 'user.about']);
Route::get('driver-list', ['uses'=>'HomeController@driverList', 'as' => 'user.driver-list']);
Route::get('trip-list', ['uses'=>'HomeController@tripSearch', 'as' => 'user.trip-list']);
Route::get('trip-list-map', ['uses'=>'HomeController@tripListmap', 'as' => 'user.trip-list-map']);
Route::get('item-list', ['uses'=>'HomeController@itemList', 'as' => 'user.item-list']);
Route::get('item-list-map', ['uses'=>'HomeController@itemListmap', 'as' => 'user.item-list-map']);

Route::get('item-view/{item_id}', ['uses'=>'HomeController@itemView', 'as' => 'user.item-view']);
Route::get('driver-detail/{driver_id}', ['uses'=>'HomeController@driverDetail', 'as' => 'user.driver-detail']);
Route::get('signin', ['uses'=>'HomeController@getlogin', 'as' => 'user.signin']);
Route::post('signin', ['uses'=>'HomeController@signin', 'as' => 'user.signin']);
Route::get('signup', ['uses'=>'HomeController@signup', 'as' => 'user.signup']);
Route::post('signup', ['uses'=>'HomeController@dosignup', 'as' => 'user.signup']);
Route::get('forgot-password', ['uses'=>'HomeController@forgotPassword', 'as' => 'user.forgotpassword']);
Route::post('forgot-password', ['uses'=>'HomeController@sentVerificationCode', 'as' => 'user.forgotpassword']);
Route::get('verification-code', ['uses'=>'HomeController@verificationCode', 'as' => 'user.verify_code']);
Route::post('verification-code', ['uses'=>'HomeController@checkVerifyCode', 'as' => 'user.verify_code']);
Route::get('change-password', ['uses'=>'HomeController@changePassword', 'as' => 'user.change.password']);
Route::post('change-password', ['uses'=>'HomeController@editPassword', 'as' => 'user.change.password']);
Route::get('logout', ['uses'=>'HomeController@logout', 'as' => 'user.logout']);
Route::get('disclaimer', ['uses'=>'HomeController@Disclaimer', 'as' => 'user.disclaimer']);
Route::get('terms-and-conditions', ['uses'=>'HomeController@TermsAndCondtions', 'as' => 'user.terms-and-conditions']);
Route::get('contact', ['uses'=>'HomeController@Contact', 'as' => 'user.contact']);
Route::post('contact', ['uses'=>'HomeController@AddContact', 'as' => 'user.contact']);


Route::group(['middleware' => 'check_role'], function() {    
    Route::get('dashboard', ['uses'=>'HomeController@dashboard', 'as' => 'user.dashboard']);
    Route::get('myprofile', ['uses'=>'UserController@myprofile', 'as' => 'user.profile']);
    Route::post('myprofile', ['uses'=>'UserController@updateprofile', 'as' => 'user.profile']);
    /*
     * Get Carmodel by car type id
     * Created Dt: 02-Nov-2016   
     */
    Route::get('driver-profile', ['uses'=>'UserController@showDriverProfile', 'as' => 'user.driver_profile']);
    Route::post('driver-profile', ['uses'=>'UserController@addProfileAsDriver', 'as' => 'user.driver_profile']);
    /*
     * Get Carmodel by car type id
     * Created Dt: 03-Nov-2016
     */
    Route::get('driver-registration', ['uses'=>'UserController@showDriverRegistration', 'as' => 'user.driver_registration']);
    Route::post('driver-registration', ['uses'=>'UserController@addDriverRegistration', 'as' => 'user.driver_registration']);
    /*
     * Get Carmodel by car type id
     * Created Dt: 03-Nov-2016   
     */
    Route::post('carmodel', ['uses'=>'UserController@carmodelByCarTypeID', 'as' => 'carmodel']);
    /*
     * Trip Model
     * Created Dt: 04-Nov-2016   
     */
     Route::resource('trip', 'TripController', ['except' => ['destroy','update']]);
    /*
     * Get trip information by Trip ID
     * Method : POST BY AJAX
     */
    Route::post('tripinfo', ['uses'=>'TripController@tripInfoByID', 'as'=>'tripinfo']);    
    Route::put('trip', ['uses'=>'TripController@update', 'as'=>'trip.update']);
    Route::post('trip-delete', ['uses'=>'TripController@destroy', 'as'=>'trip_destroy']);
    /*
     * Items Model
     * Created Dt: 05-Nov-2016   
     */
    Route::resource('item', 'ItemController', ['except' => ['destroy']]);
    Route::get('item-delete/{item_id}', ['uses'=>'ItemController@destroy', 'as'=>'item_destroy']);    
    Route::post('subcategorybycatid', ['uses'=>'MainController@subCategoryByCategoryID', 'as' => 'subcategory_by_categoryid']);
    Route::post('subcategorybysubcatid', ['uses'=>'MainController@subCategoryBySubCatID', 'as' => 'subcategory_by_subcatid']);
    Route::get('diling-code/{country_id}', ['uses'=>'HomeController@getCountryDilingCodeByCountryID', 'as' => 'diling_code']);
    /*
     * Notification by driver to user for item
     */
    Route::post('item-request-accept-buyer', ['uses'=>'ItemController@itemRequestAcceptByDriver', 'as'=>'item_request_accept_buyer']);
    Route::get('view-notification/{notifi_id}', ['uses'=>'ItemController@viewNotification', 'as'=>'view_notification']);
    Route::get('show-notification/{notifi_id}', ['uses'=>'ItemController@showNotification', 'as'=>'show_notification']);
    Route::post('make-another-itemoffer', ['uses'=>'ItemController@makeAnotherItemOffer', 'as' => 'make_another_itemoffer']);
    
    /*
     * Notification by buyer to driver for item
     */
    Route::post('buyer-request-driver', ['uses'=>'ItemController@buyerRequestDriverForItem', 'as' => 'buyer_request_driver']);
    /*
     * Deny deriver request
     * Created Dt: 24-Nov-2016     
     */
    Route::get('deny-driver-request/{request_id}', ['uses'=>'ItemController@denyDriverRequest', 'as' => 'deny_driver_request']);
    
    /*
     * 
     */
    Route::get('accept-driver-request/{request_id}', ['uses'=>'ItemController@acceptDriverRequest', 'as' => 'accept_driver_request']);
    
    /*
     * Controller : User
     * Function: contactDriver     
     * Created Date: 28-Nov-2016
     */
    Route::get('contact-driver/{driver_id}', ['uses'=>'UserController@contactDriver', 'as' => 'contact_driver']);
    
    /*
     * Controller : Item
     * Function: userItems by ajax     
     * Created Date: 29-Nov-2016
     */
    Route::post('user-items', ['uses'=>'ItemController@userItems', 'as' => 'user_items']);
    /*
     * Controller : Item
     * Function: userItems by ajax     
     * Created Date: 29-Nov-2016
     */
    Route::post('user-trips', ['uses'=>'TripController@userTrips', 'as' => 'user_trips']);
    Route::post('car-img-upload', ['uses'=>'UserController@carImgUpload', 'as'=>'car_img_upload']);
});
/*
 * Created Date: 28-Nov-2016
 * Created By: Rahul Gupta
 * Admin Section
 */
Route::group(["namespace" => 'Admin', "prefix" => "admin"], function() {    
    Route::get('/', ['uses'=>'HomeController@index', 'as'=>'admin.home']);
    Route::get('login', ['uses'=>'HomeController@signin', 'as'=>'admin.login']);
    Route::post('login', ['uses'=>'HomeController@doLogin', 'as'=>'admin.login']);
    Route::get('forgot-pass', ['uses'=>'HomeController@forgotPass', 'as'=>'admin.forgot_pass']);
    Route::post('forgot-pass', ['uses'=>'HomeController@sendPass', 'as'=>'admin.forgot_pass']);
    Route::get('dashboard', ['uses'=>'HomeController@dashboard', 'as' => 'admin.dashboard']);
    Route::get('logout', ['uses'=>'HomeController@logout', 'as' => 'admin.logout']);
    
    Route::post('users-data', ['uses'=>'HomeController@usersData', 'as'=>'admin.users-data']);
    Route::get('trips-data', ['uses'=>'HomeController@tripList', 'as'=>'admin.trips-data']);
    Route::get('users-list', ['uses'=>'HomeController@usersList', 'as'=>'admin.users-list']);
    Route::get('users-details/{user_id}', ['uses'=>'HomeController@usersDetails', 'as'=>'admin.users-details']);
    Route::get('add-category', ['uses'=>'HomeController@addCategory', 'as'=>'admin.add_category']);
    Route::post('add-category', ['uses'=>'HomeController@insertCategory', 'as'=>'admin.add_category']);
    Route::get('category-list', ['uses'=>'HomeController@categoryList', 'as'=>'admin.category-list']);
    Route::get('sub-category-list/{cat_id?}', ['uses'=>'HomeController@subCategoryLists', 'as'=>'admin.sub-category-list']);
    Route::get('sub-category-list', ['uses'=>'HomeController@subCategoryList', 'as'=>'admin.sub-category-list']);
    Route::get('edit-categories/{catID}', ['uses'=>'HomeController@updateCategory', 'as'=>'admin.edit_categories']);
    Route::get('edit-page', ['uses'=>'HomeController@EditPageCategory', 'as'=>'admin.edit_page']);
    Route::post('edit-categories', ['uses'=>'HomeController@updateCategorySubmit', 'as'=>'admin.update_categories']);
    Route::get('second-sub-category-list/{sub_cat_id}', ['uses'=>'HomeController@SecoundSubCategoryList', 'as'=>'admin.second-sub-category-list']);
    Route::get('user-list/{user_type}', ['uses'=>'HomeController@usersList', 'as'=>'admin.user-list']);
    Route::get('contact-list', ['uses'=>'HomeController@contactList', 'as'=>'admin.contact-list']);
    Route::get('advertise', ['uses'=>'HomeController@Advertise', 'as'=>'admin.advertise']);
    Route::post('change-img-link', ['uses'=>'HomeController@ChangeImageLink', 'as'=>'admin.change-img-link']);
    Route::get('change-language', ['uses'=>'HomeController@changeLanguage', 'as'=>'admin.change_language']);
    Route::get('update-lang-file', ['uses'=>'HomeController@updateLangaugeFile', 'as'=>'admin.update_lang_file']);
    Route::post('language-update-data', ['uses'=>'HomeController@languageUpdateData', 'as'=>'admin.language_update_data']);
    Route::post('final-cahnge-lang', ['uses'=>'HomeController@finalCahngeLang', 'as'=>'admin.final_cahnge_lang']);
    
    
});

//Payment
Route::get('make-payment/{item_id}', ['uses'=>'PaymentController@makePayment', 'as'=>'make_payment']);
Route::get('notify-url', ['uses'=>'PaymentController@payNotify', 'as'=>'notify_url']);
Route::get('cancel-url', ['uses'=>'PaymentController@payCancel', 'as'=>'cancel_url']);
Route::get('success-url', ['uses'=>'PaymentController@payConfirm', 'as'=>'success_url']);
Route::post('success-url', ['uses'=>'PaymentController@payConfirm', 'as'=>'success_url']);



//qr code functions
Route::get('qr-code', ['uses'=>'QrCodeController@qrCodeGenretor', 'as'=>'qr-code']);
Route::get('qr-code-show', ['uses'=>'QrCodeController@showQRcode', 'as'=>'qr-code-show']);
Route::get('confirmation-received/{request_id}', ['uses'=>'HomeController@confirmReceived', 'as'=>'received_confirmation']);
Route::get('confirmation-delivered/{request_id}', ['uses'=>'HomeController@confirmDelivered', 'as'=>'delivered_confirmation']);
Route::post('confirmation-received', ['uses'=>'HomeController@confirmPostReceived', 'as'=>'confirmation_received']);
Route::post('confirmation-delivered', ['uses'=>'HomeController@confirmPostDelivered', 'as'=>'confirmation_delivered']);

