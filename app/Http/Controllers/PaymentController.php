<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\RequestItem;
use App\Items;
use App\User;
use App\Trips;
use config\QrCode\QrCode;
use App\PaymentTransaction;
use Auth;
use DB;
use Mail;
use View;

class PaymentController extends Controller {

    const PAYPAL_PRODUCTION = 'https://www.paypal.com/cgi-bin/webscr';
    const ENDPOINT_PRODUCTION = 'https://api-3t.paypal.com/nvp';
    const PAYPAL_SANDBOX = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
    const ENDPOINT_SANDBOX = 'https://api-3t.sandbox.paypal.com/nvp';

    /**
     * @var string|array
     * The url to return the customer after a successful payment
     */
    public $returnUrl;

    /**
     * @var string|array The url to return the customer if payment was cancelled
     */
    public $cancelUrl;

    /**
     * @var string|array The url to notify url for the paypal
     */
    public $notifyUrl;

    /**
     * @var string Default currency to use
     */
    public $currency = 'USD';

    /**
     * @var string|array server URL which you have to connect for submitting your API request
     */
    public $endPoint;

    /**
     * @var string
     * Define the PayPal URL. This is the URL that the buyer is
     * first sent to to authorize payment with their paypal account
     * change the URL depending if you are testing on the sandbox
     * or going to the live PayPal site
     * For the sandbox, the URL is
     * https://www.sandbox.paypal.com/cgi-bin/webscr
     * For the live site, the URL is
     * https://www.paypal.com/cgi-bin/webscr
     */
    public $paypalUrl;

    /**
     * @var string paypal business/merchant email
     */
    public $businessEmail;
    public $paypalSandbox;

    public function __construct() {
        $countries = DB::table('countries')->where('status', '1')->get();
        $this->data['countries'] = array();
        foreach ($countries as $objCountries) {
            $this->data['countries'][$objCountries->id] = $objCountries->name;
        }
        $cartypes = DB::table('wgn_cartype')->where('status', '1')->orderBy('order', 'ASC')->get();
        $this->data['cartypes'] = array();
        foreach ($cartypes as $objCartype) {
            $this->data['cartypes'][$objCartype->id] = $objCartype->type;
        }
        $this->data['currency'] = array('USD' => '$', 'EUR' => '&#8364;', 'GBP' => '&pound;', 'AUD' => 'A$');
        $this->data['notifi_type'] = array();
        $wgn_notification_type = DB::table('wgn_notification_type')->Where('status', '1')->get();
        foreach ($wgn_notification_type as $objNotificationType) {
            $this->data['notifi_type'][$objNotificationType->type] = $objNotificationType->title;
        }
    }

    public function makePayment($id) {
        $request_id = base64_decode($id);
        $objRequestItem = RequestItem::find($request_id);
        if (Auth::check()) {
            if (!empty($objRequestItem)) {
                $items = Items::Select('wgn_item.*', 'users.firstname', 'users.lastname', 'users.email', 'wgn_currency.currency_type')
                        ->LeftJoin('users', 'wgn_item.userid', '=', 'users.id')
                        ->LeftJoin('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')
                        ->find($objRequestItem->item_id);

                $another_offer = array();
                if ($items->offer_type == 2) {
                    $another_offer = DB::table("wgn_make_another_itemoffer")
                            ->Select('price as new_price')
                            ->Where(array('driverid' => $objRequestItem->driver_id, 'status' => '1'))
                            ->first();
                }

                $trips = DB::table("wgn_trip")->find($objRequestItem->trip_id);

                $userdetails = DB::table('users')
                                ->Select('users.*', 'wgn_usersdetail.*', 'wgn_cartype.type as cartype_title', 'wgn_carmodel.model as carmodel_title')
                                ->Leftjoin('wgn_usersdetail', 'users.id', '=', 'wgn_usersdetail.userid')
                                ->Leftjoin('wgn_cartype', 'wgn_usersdetail.cartype', '=', 'wgn_cartype.id')
                                ->Leftjoin('wgn_carmodel', 'wgn_usersdetail.carmodel', '=', 'wgn_carmodel.id')
                                ->Where('users.id', $objRequestItem->driver_id)->first();

                /* ADD Payment Detail With Pending Status */
                $TotalPrice = !empty($another_offer->new_price) ? $another_offer->new_price : $items->item_price;
                $objPayTran = new PaymentTransaction();
                $objPayTran->user_id = Auth::user()->id;
                $objPayTran->item_id = $items->id;
                $objPayTran->trip_id = $trips->id;
                $objPayTran->request_id = $request_id;
                $objPayTran->mc_gross = $TotalPrice;
                $objPayTran->payment_status = "";
                $objPayTran->item_name = $items->id . "-" . $trips->id;
                $objPayTran->created_at = date('Y-m-d H:i:s');
                $objPayTran->updated_at = date('Y-m-d H:i:s');
                $objPayTran->save();


                $user = Auth::user();
                $userID = $user->id;

                $CurrencyType = $items->currency_type;
                $url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
                $url .= "?business=mayank.a@cisinlabs.com";
                $url .= "&cmd=_xclick";
                $url .= "&item_name=" . urlencode("Payment for Item Shipping");
                $url .= "&amount=" . $TotalPrice;
                $url .= "&custom=" . $objPayTran->id . "-" . $user->id . "-" . $objRequestItem->driver_id . "-" . $items->id . "-" . $trips->id . "-" . $request_id;
                $url .= "&currency_code=" . $CurrencyType;
                $url .= "&notify_url=" . url('notify-url');
                $url .= "&cancel_return=" . url('cancel-url');
                $url .= "&return=" . url('success-url');
                return redirect($url);
            }
        }
    }

    public function payConfirm() {
        //Custom Field : 1. Payment ID, 2. User ID, 3. Item ID, 4. Trip ID 5. Request ID
        if ((isset($_REQUEST["txn_id"]) && isset($_REQUEST["txn_type"]))) {
            $custom = explode("-", $_REQUEST['custom']);
            $userDetail = User::find($custom[1]);
            $objItems = Items::find($custom[3]);
            $objRequest = RequestItem::find($custom[5]);
            $objTrip = Trips::find($custom[4]);
            $driverDetail = array();
            if (!empty($objRequest)) {
                $driverDetail = User::Select('users.*', 'wgn_usersdetail.cartype')
                                ->Join('wgn_usersdetail', 'users.id', '=', 'wgn_usersdetail.userid')
                                ->Where('users.id', $objRequest->driver_id)->first();
            }
            if (isset($_REQUEST['payment_status']) && $_REQUEST['payment_status'] == "Pending") {
                $payTrans = PaymentTransaction::find($custom[0]);
                $payTrans->payment_status = $_REQUEST['payment_status'];
                $payTrans->txn_id = $_REQUEST["txn_id"];
                $payTrans->txn_type = $_REQUEST["txn_type"];
                $payTrans->txn_response = serialize($_REQUEST);
                $payTrans->updated_at = date('Y-m-d H:i:s');
                if ($payTrans->save()) {
                    // Add one new notification                
                    DB::table('wgn_notification')->insert(['request_id' => $custom[5], 'sender_id' => $custom[1], 'item_id' => $custom[3], 'receiver_id' => $custom[2], 'status' => '1', 'notification_type' => 'payment-information-to-driver', 'created_at' => date('Y-m-d H:i:s')]);

                    // Generate QR Code for seller                                
                    $sellerUrl = route('received_confirmation', base64_encode($custom[5]));
                    $userUrl = route('delivered_confirmation', base64_encode($custom[5]));
                    $filenameQRSeller = "received_" . $custom[5] . "_" . date('YmdHis') . ".png";
                    $filenameQRUser = "delivered_" . $custom[5] . "_" . date('YmdHis') . ".png";
                    $filenameQRSeller = $this->qrCodeGenretor($sellerUrl, $filenameQRSeller);
                    $filenameQRUser = $this->qrCodeGenretor($userUrl, $filenameQRUser);

                    // Send Mail to Seller with QR Code Image
                    $to = $objItems->seller_email;
                    $template = "email.sellerMail";
                    $name = "";
                    $sub = "Driver information for item {$objItems->product_name}  - whatsgonow";
                    $driver_address = "";
                    if (!empty($driverDetail->housenumber))
                        $driver_address .= ", " . $driverDetail->housenumber;
                    if (!empty($driverDetail->street))
                        $driver_address .= ", " . $driverDetail->street;
                    if (!empty($driverDetail->city))
                        $driver_address .= ", " . $driverDetail->city;
                    if (!empty($driverDetail->country))
                        $driver_address .= ", " . $this->data['countries'][$driverDetail->country];
                    if (!empty($driverDetail->postalcode))
                        $driver_address .= "(" . $driverDetail->postalcode . ")";
                    $driver_image = route('home') . "/public/uploads/users/itmimg.png";
                    if (!empty($driverDetail->userimage)) {
                        if (file_exists("public/uploads/users/" . $driverDetail->userimage)) {
                            $driver_image = route('home') . "/public/uploads/users/" . $driverDetail->userimage;
                        }
                    }
                    $replaces = array(
                        'item_name' => !empty($objItems->product_name) ? $objItems->product_name : "",
                        'driver_name' => @$driverDetail->firstname . " " . @$driverDetail->lastname,
                        'car_type' => !empty($driverDetail->cartype) ? $this->data['cartypes'][$driverDetail->cartype] : "",
                        'driver_contact_no' => !empty($driverDetail->phone) ? $driverDetail->phone : "",
                        'driver_address' => !empty($driver_address) ? trim($driver_address, ",") : "",
                        'trip_date' => !empty($objTrip->trip_date) ? $objTrip->trip_date : "",
                        'driver_image' => !empty($driver_image) ? $driver_image : ""
                    );
                    $file_path = route('home') . "/public/uploads/qrcodes/" . $filenameQRSeller;
                    $this->sendMail($to, $name, $sub, $template, $replaces, null, $file_path);
                    // End
                    //Mail                
                    $to = $userDetail->email;
                    $template = "email.paymentConfirmation";
                    $name = $userDetail->firstname . " " . $userDetail->lastname;
                    $sub = 'Payment Status  - whatsgonow';
                    $replaces = array(
                        'user_name' => $name,
                        'item_name' => ucfirst($objItems->product_name),
                        'payment_status' => $_REQUEST['payment_status'],
                        'amount' => $_REQUEST['mc_gross'],
                        'transaction_id' => $_REQUEST["txn_id"]
                    );
                    $file_path = route('home') . "/public/uploads/qrcodes/" . $filenameQRUser;
                    $this->sendMail($to, $name, $sub, $template, $replaces, null, $file_path);
                    //End
                    $flash = array('flash_alert_notice' => 'Payment successfully Completed, Please check the mail, we have sent a QR Code for delivery confirmation!', 'flash_action' => 'success');
                    return Redirect::route('home')->with($flash)->withInput();
                } else {
                    $flash = array('flash_alert_notice' => 'Payment not done successfully!', 'flash_action' => 'danger');
                    return Redirect::route('home')->with($flash)->withInput();
                }
            } else {
                $flash = array('flash_alert_notice' => 'Payment not done successfully!', 'flash_action' => 'danger');
                return Redirect::route('home')->with($flash)->withInput();
            }
        } else {
            $flash = array('flash_alert_notice' => 'Payment not done successfully!', 'flash_action' => 'danger');
            return Redirect::route('home')->with($flash)->withInput();
        }
    }

    /*
     * Function for notify url
     */

    public function payNotify() {
        $custom = explode("-", $_REQUEST['custom']);
        $userDetail = User::find($custom[1]);
        $objItems = Items::find($custom[3]);
        $payTrans = PaymentTransaction::find($custom[0]);
        $payTrans->payment_status = $_REQUEST['payment_status'];
        $payTrans->txn_id = $_REQUEST["txn_id"];
        $payTrans->txn_type = $_REQUEST["txn_type"];
        $payTrans->txn_response = serialize($_REQUEST);
        $payTrans->updated_at = date('Y-m-d H:i:s');
        $payTrans->mc_gross = $_REQUEST['mc_gross'];
        $to = $userDetail->email;
        $template = "email.paymentConfirmation";
        $name = $userDetail->firstname . " " . $userDetail->lastname;
        $sub = 'Payment Status  - whatsgonow';
        $replaces = array(
            'user_name' => $name,
            'item_name' => ucfirst($objItems->product_name),
            'payment_status' => $_REQUEST['payment_status'],
            'amount' => $_REQUEST['mc_gross'],
            'transaction_id' => $_REQUEST["txn_id"]
        );
        if ($payTrans->save()) {
            $this->sendMail($to, $name, $sub, $template, $replaces);
        }
    }

    /*
     * Function for cancel url
     */

    public function payCancel() {
        $flash = array('flash_alert_notice' => 'Payment successfully cancelled!', 'flash_action' => 'warninig');
        return Redirect::route('home')->with($flash)->withInput();
    }

    public function sendMail($to, $to_name, $subject, $template, $keys = null, $addbcc = NULL, $file_path = "") {
        Mail::send($template, $keys, function($message) use($to, $to_name, $subject, $file_path) {
            $message->to($to, $to_name)->subject($subject);
            if (!empty($file_path)) {
                $message->attach($file_path);
            }
        });
        return TRUE;
    }

    public function qrCodeGenretor($url = "", $filename = '') {
        $qr = new QrCode();
        $qr->setText($url);
        $qr->setSize("200");
        $qr->render("public/uploads/qrcodes/" . $filename);
        return $filename;
    }

}