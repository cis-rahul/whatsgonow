<?php

/*
 * Created by Rahul Gupta
 * Created Dt: 02-Nov-2016
 */

namespace App\Http\Controllers;

use App\Http\Controllers\MainController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Http\Requests\DriverRegistrationRequest;
use App\Countries;
use App\UserDetail;
use App\User;
use App\Trips;
use App\Items;
use Auth;
use View;
use Input;
use DB;
use Validator;
use Pagination;

class UserController extends MainController {

    public function __construct() {        
        parent::__construct();
    }

    public function myprofile() {
        //$this->data['countries'] = Countries::all()->toArray();
        $this->data['userinfo'] = Auth::user();
        $this->data['useritems'] = Items::Where('userid', $this->data['userinfo']->id)->Where('status', '1')->orderBy('id', 'Desc')->get()->toArray();
        return View::make('myprofile', $this->data);
    }

    public function updateprofile(UserRequest $request) {
        $userdetail = Auth::user();        
        $userinfo = User::find($userdetail->id);
        $fileName = "";
        if (!empty(Input::file('userimage')) && Input::file('userimage')->getError() == 0) {
            $path = base_path() . '/public/uploads/users';
            $fileName = $this->fileUpload(Input::file('userimage'), $path, $userdetail->userimage);
            $userinfo->userimage = $fileName;
        }
        $userinfo->city = $request->input('city');
        $userinfo->country = $request->input('country');
        $userinfo->postalcode = $request->input('postalcode');
        $userinfo->phone = $request->input('phone');
        $userinfo->street = $request->input('street');
        $userinfo->housenumber = $request->input('housenumber');
        if ($userinfo->save()) {
            $flash = array('flash_alert_notice' => 'User Profile updated successfully !', 'flash_action' => 'success');
            return Redirect::route('user.profile')->with($flash)->withInput();
            ;
        } else {
            $flash = array('flash_alert_notice' => 'Somthing went wrong!', 'flash_action' => 'danger');
            return Redirect::route('user.profile')->with($flash)->withInput();            
        }
    }

    public function showDriverRegistration() {
        //$this->data['countries'] = Countries::all()->toArray();
        $this->data['cartype'] = DB::table('wgn_cartype')->where('status','1')->orderBy('order', 'ASC')->get();
        $this->data['userinfo'] = Auth::user();
        return View::make('driverregistration', $this->data);
    }

    public function addDriverRegistration(DriverRegistrationRequest $request) {
        $userdetail = Auth::user();
        $userinfo = User::find($userdetail->id);
        $fileName = "";
        if (!empty(Input::file('userimage')) && Input::file('userimage')->getError() == 0) {
            $path = base_path() . '/public/uploads/users';
            $fileName = $this->fileUpload(Input::file('userimage'), $path, $userdetail->userimage);
            $userinfo->userimage = $fileName;
        }
        $userinfo->role = $request->input('role');
        $userinfo->firstname = $request->input('firstname');
        $userinfo->lastname = $request->input('lastname');
        $userinfo->postalcode = $request->input('postalcode');
        $userinfo->city = $request->input('city');
        $userinfo->dob = date('Y-m-d', strtotime($request->input('dob')));
        $userinfo->driversince = date('Y-m-d H:i:s');
        $userinfo->country = $request->input('country');
        if ($userinfo->save()) {
            $userexdetail = new UserDetail();
            $userexdetail->userid = $userinfo->id;
            $userexdetail->cartype = $request->input('cartype');
            $userexdetail->carmodel = $request->input('carmodel');
            $userexdetail->cardesc = $request->input('cardesc');
            $userexdetail->street = $request->input('street');
            $userexdetail->housenumber = $request->input('housenumber');
            $userexdetail->licenceno = $request->input('licenceno');
            $userexdetail->licenceissueplace = $request->input('licenceissueplace');
            $userexdetail->licenceissuedt = date('Y-m-d' , strtotime($request->input('licenceissuedt')));
            $userexdetail->identificationmethod = $request->input('identificationmethod');
            $userexdetail->paypal_email_id = $request->input('paypal_email_id');
            $userexdetail->save();
            $flash = array('flash_alert_notice' => 'User Profile updated successfully !', 'flash_action' => 'success');
            return Redirect::route('user.driver_profile')->with($flash)->withInput();
        } else {
            $flash = array('flash_alert_notice' => 'Somthing went wrong!', 'flash_action' => 'danger');
            return Redirect::route('user.driver_registration')->with($flash)->withInput();
        }
    }

    public function carmodelByCarTypeID(Request $request) {
        if ($request->ajax()) {
            $status = false;
            $carmodels = array();
            if (!empty($request->input('cartypeid'))) {
                $carmodels = DB::table('wgn_carmodel')->where('cartype', $request->input('cartypeid'))->get();
                $status = false;
                if (!empty($carmodels)) {
                    $status = true;
                }
            }
            echo json_encode(array(
                'status' => $status,
                'data' => $carmodels
            ));
        }
    }

    public function showDriverProfile() {
        if (Auth::User()->role == 2) {
            $userDetailObj = UserDetail::Where('userid', Auth::user()->id)->first();
            if(empty($userDetailObj)){                    
                return Redirect::route('user.driver_registration');
            }
            $this->data['items'] = DB::table('wgn_item')
                ->leftjoin('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')
                ->select('wgn_item.id', 'wgn_item.item_currency', 'wgn_item.item_pieces', 'wgn_item.item_image1', 'wgn_item.item_image2', 'wgn_item.item_image3', 'wgn_item.from_city', 'wgn_item.from_postal_code', 'wgn_item.from_postal_code', 'wgn_item.from_country', 'wgn_item.to_city', 'wgn_item.to_postal_code', 'wgn_item.to_country', 'wgn_item.item_price', 'wgn_currency.currency_type')
                ->Where(array('userid'=>$this->data['userinfo']->id, 'wgn_item.status'=>'1'))
                ->orderBy('wgn_item.id', 'desc')
                ->get();            

            $this->data['userdetail'] = UserDetail::Where('userid', $this->data['userinfo']->id)->first();
            $this->data['trips'] = Trips::Select('id','trip_time', DB::Raw("DATE_FORMAT(trip_date,'%d-%c-%Y') trip_date"), 'trip_from', 'trip_to')
                    ->Where('status', '1')
                    ->Where('userid', Auth::User()->id)
                    ->whereRaw("CONCAT(trip_date, ' ', trip_time) >='" . date('Y-m-d H:i:s') . "'")
                    ->orderBy('id', 'Desc')
                    ->get()->toArray();            
            return View::make('driverprofile', $this->data);
        } else {
            return Redirect::route('home');
        }
    }

    public function addProfileAsDriver(Request $request) {                
        $file = array('phone' => $request->input('phone'), 'postalcode' => $request->input('phone'));
        $rules = array('phone' => 'numeric', 'postalcode' => 'numeric');        
        if(!empty(Input::file('car_picture'))){
            $i = 1;
            foreach(Input::file('car_picture') as $obj){
                $file["image_$i"] = $obj;
                $rules["image_$i"] = 'mimes:jpeg,bmp,png,jpg';
                $i++;
            }
        }
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
            return Redirect::route('user.driver_profile')->withErrors($validator)->withInput();
        } else {            
            if (Auth::User()->role == '2') {                
                $userinfo = User::find($request->input('userid'));
                $path = base_path() . '/public/uploads/users';
                if ($request->input('firstname')) {
                    $userinfo->firstname = $request->input('firstname');
                }
                if ($request->input('lastname')) {
                    $userinfo->lastname = $request->input('lastname');
                }
                if ($request->input('phone')) {
                    $userinfo->phone = $request->input('phone');
                }
                if ($request->input('city')) {
                    $userinfo->city = $request->input('city');
                }
                if ($request->input('country')) {
                    $userinfo->country = $request->input('country');
                }
                if ($request->input('dob')) {
                    $userinfo->dob = $request->input('dob');
                }
                if ($request->input('membersince')) {
                    $userinfo->membersince = $request->input('membersince');
                }
                if ($request->input('driversince')) {
                    $userinfo->driversince = $request->input('driversince');
                }                
                if ($userinfo->save()) {                    
                    $i = 1;
                    $userDetailObj = UserDetail::Select('id')->Where('userid', $request->input('userid'))->first();
                    if ($request->input('street')) {
                        $userDetailObj->street = $request->input('street');
                    }
                    if ($request->input('housenumber')) {
                        $userDetailObj->housenumber = $request->input('housenumber');
                    }
                    foreach (Input::file('car_picture') as $objCarPick) {                        
                        $fileName = "";
                        $image = "image" . $i;             
                        if (!empty($objCarPick) && $objCarPick->getError() == 0) {
                            $fileName = $this->fileUpload($objCarPick, $path, $userDetailObj->$image);
                            $userDetailObj->$image = $fileName;
                        }
                        $i++;
                    }
                    $userDetailObj->save();
                }
                $trip_id = $request->input('trip_id');
                $trip_from = $request->input('trip_from');
                $trip_to = $request->input('trip_to');
                $trip_date = $request->input('trip_date');
                $trip_time = $request->input('trip_time');
                $j = 0;
                if(isset($trip_id) && !empty($trip_id)) {                    
                    foreach ($trip_id as $objTrip) {
                        $tripobj = Trips::find($objTrip);                        
                        if($tripobj->userid == $userinfo->id){                            
                            $tripobj->trip_from = $trip_from[$j];
                            $tripobj->trip_to = $trip_to[$j];
                            $tripobj->trip_date = date('Y-m-d', strtotime($trip_date[$j]));
                            $tripobj->trip_time = $trip_time[$j];
                            $tripobj->save();                            
                        }
                        $j++;
                    }
                }
                $flash = array('flash_alert_notice' => 'Driver profile updated successfully!', 'flash_action' => 'success');
                return Redirect::route('user.driver_profile')->with($flash);
            } else {
                return Redirect::route('home');
            }
        }
    }
    /*
     * Contact Driver
     */
    public function contactDriver($driver_id){
        die("Hi");
    }
    
    public function carImgUpload(Request $request){  
        if($request->ajax()){
            $userDetailObj = UserDetail::Where('userid',$request->input('userid'))->first();
            $column_name = $request->input('column_name');
            $fileName = "";
            $status = false;
            if (!empty(Input::file('file')) && Input::file('file')->getError() == 0) {
                $path = base_path() . '/public/uploads/users';
                $fileName = $this->fileUpload(Input::file('file'), $path, $userDetailObj->$column_name);
                $userDetailObj->$column_name = $fileName;
                $status = false;
                if($userDetailObj->save()){
                    $status = true;
                }
            }
            echo json_encode(array(
                'status'=>$status
            ));
        }
    }
}
