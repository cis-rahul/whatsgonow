<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\UserDetail;
//use config\QrCode\QrCode;
use Auth;
use Input;
use DB;
use Mail;

class MainController extends Controller
{
    protected $data = array();
    
    public function __construct() {                 
        if(!Auth::check()){            
            return Redirect::route('home')->send();
        }else{            
            $this->data['userinfo'] = Auth::User();
            //Notification
            $this->data['user_notification'] = DB::table('wgn_notification')
                    ->select('wgn_notification.*', 'users.firstname as senderfirstname','users.lastname as senderlastname', 'wgn_item.product_name')
                    ->Leftjoin('users', 'wgn_notification.sender_id', '=', 'users.id')
                    ->Leftjoin('wgn_item', 'wgn_notification.item_id', '=', 'wgn_item.id')
                    ->Where(array('wgn_notification.status'=>'1', 'wgn_notification.receiver_id'=>$this->data['userinfo']->id))
                    ->orderBy('wgn_notification.id', 'Desc')
                    ->get();            
        }
        $countries = DB::table('countries')->get();
        $this->data['countries'] = array();
        foreach($countries as $objCountries){
            $this->data['countries'][$objCountries->id] = $objCountries->name;
        }
        $cartypes = DB::table('wgn_cartype')->get();
        $this->data['cartypes'] = array();
        foreach($cartypes as $objCartype){
            $this->data['cartypes'][$objCartype->id] = $objCartype->type;
        }
        $this->data['currency'] = array('USD'=>'$', 'EUR'=>'&#8364;', 'GBP'=>'&pound;', 'AUD'=>'A$');
        $wgn_notification_type = DB::table('wgn_notification_type')->Where('status', '1')->get();
        $this->data['notifi_type'] = array();
        foreach($wgn_notification_type as $objNotificationType){
            $this->data['notifi_type'][$objNotificationType->type] = $objNotificationType->title;
        }        
    }
    
    public function fileUpload($fileInput, $path, $fileName ="") {        
        $destinationPath = $path; // upload path
        $extension = $fileInput->getClientOriginalExtension(); // getting image extension
        $originalName = $fileInput->getClientOriginalName();
        $originalName = explode("." , $originalName);
        if(empty($fileName))
            $fileName = $originalName[0] . "-" . rand('1', '99') . date('YmdHis') . '.' . $extension; // renameing image
        $fileInput->move($destinationPath, $fileName); // uploading file to given path
    	return $fileName;
    }
    
    public function subCategoryByCategoryID(Request $request){
        if($request->ajax()){
            $cat_id = $request->input('cat_id');
            $subcategory = DB::table('wgn_item_subcategory')->Where('category_id', $cat_id)->get();            
            $status = false;
            if(!empty($subcategory)){
                $status = true;
            }
            echo json_encode(array(
                'status'=>$status,
                'data'=>$subcategory
            ));
        }
    }
    public function subCategoryBySubCatID(Request $request){
        if($request->ajax()){
            $subcat_id = $request->input('subcat_id');
            $subcategory = DB::table('wgn_item_subcategory_two')->Where('subcat_id', $subcat_id)->get();
            $status = false;
            if(!empty($subcategory)){
                $status = true;
            }
            echo json_encode(array(
                'status'=>$status,
                'data'=>$subcategory
            ));
        }
    }
    
    public function showNotification($id){        
        $notification_id = base64_decode($id);
        $notification_detial = DB::table('wgn_notification')->find($notification_id);
        
    }
    
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    public function sendMail($to, $to_name, $subject, $template, $keys = null, $addbcc = NULL) {        
        Mail::send($template,$keys, function($message)  use($to, $to_name,$subject) {           
            $message->to($to, $to_name)->subject($subject);
        });
        return TRUE;
    }
    
    function getLatLong($address){
        if(!empty($address)){
            //Formatted address
            $formattedAddr = str_replace(' ','+',$address);
            $formattedAddr = str_replace(',','+',$formattedAddr);
            //Send request and receive json data by address
            $geocodeFromAddr = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false'); 
            $output = json_decode($geocodeFromAddr);
            //Get latitude and longitute from json data
            $data = array();
            if(!empty($output->results)){
                $data['latitude']  = $output->results[0]->geometry->location->lat; 
                $data['longitude'] = $output->results[0]->geometry->location->lng;
            }
            //Return latitude and longitude of the given address
            if(!empty($data)){
                return $data;
            }else{
                return false;
            }
        }else{
            return false;   
        }
    }
    
//    public function qrCodeGenretor($filename = '') {
//        $amount = '$85';
//        $item = 'Motal';
//        $qr = new QrCode();
//        $qr->setText("http://www.whatsgonow.com?itemName=Mobile&price={$amount}&itemname={$item}")
//        ->setSize("200")
//        ->render("public/uploads/qrcodes/" . $filename);                
//    }        
}