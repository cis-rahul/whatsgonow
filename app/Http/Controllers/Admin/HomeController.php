<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers;
use App\Http\Controllers\Auth\AuthController;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image; // Use this if you want facade style code
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegistrationRequest;
use Illuminate\Support\Facades\Redirect;
use config\DataTable\DataTable;
use View;
use App\User;
use App\Items;
use App\Category;
use App\Countries;
use Session;
use Auth;
use Validator;
///use Input;
use DB;
use Mail;
use File;

//use Input;


class HomeController extends AuthController {

    /**
     * Created By: Rahul Gupta
     * Created Dt: 01-Sep-2016
     */
    protected $data = array();

    public function __construct() {
        
    }

    //Get Method
    public function index() {

        return View::make('admin.login', $this->data);
    }

    public function signin() {
        if (!Auth::check())
            return View::make('admin.login');
        else
            return Redirect::route('admin.dashboard');
    }

    //Post Method

    public function doLogin(LoginRequest $request) {
        if (!Auth::check()) {
            $credentials_un = array('username' => $request->input('username'), 'password' => $request->input('password'), 'role' => '0');
            $credentials_em = array('email' => $request->input('username'), 'password' => $request->input('password'), 'role' => '0');
            if (Auth::attempt($credentials_un) || Auth::attempt($credentials_em)) {
                return Redirect::route('admin.dashboard');
            } else {
                return Redirect::route('admin.login')->with(array('flash_alert_notice' => 'Wrong Username or Password. Try again!', 'flash_action' => 'danger'))->withInput();
            }
        } else {
            return Redirect::route('admin.dashboard');
        }
    }

    public function dashboard() {
        if (Auth::check())
            return View::make('admin.dashboard', $this->data);
        else
            return Redirect::route('admin.home');
    }

    public function fileUpload($fileInput, $path, $fileName = "") {
        $destinationPath = $path; // upload path
        $extension = $fileInput->getClientOriginalExtension(); // getting image extension
        $originalName = $fileInput->getClientOriginalName();
        $originalName = explode(".", $originalName);
        if (empty($fileName))
            $fileName = $originalName[0] . "-" . rand('1', '99') . date('YmdHis') . '.' . $extension; // renameing image
        $fileInput->move($destinationPath, $fileName); // uploading file to given path
        return $fileName;
    }

    public function logout() {
        Auth::logout();
        return Redirect::route('admin.home');
    }

    public function forgotPass() {
        if (!Auth::check())
            return View::make('admin.forgot_pass');
    }

    public function sendPass(Request $request) {
        if (!Auth::check()) {
            $file = array('email' => $request->input('email'));
            $rules = array('email' => 'required|email');
            $validator = Validator::make($file, $rules);
            if ($validator->fails()) {
                return Redirect::route('admin.forgot_pass')->withErrors($validator)->withInput();
            } else {
                $userdetial = User::Where('email', $request->input('email'))->first();
                $name = $userdetial->firstname . " " . $userdetial->lastname;
                $to = $userdetial->email;
                $sub = "Authentication Detail - whatsgonow";
                $template = "email.forgotPassAdmin";
                $replaces = array("name" => $name, 'verification_code' => $verification_code);
                //$this->sendMail($to, $name, $sub, $template, $replaces);
                return Redirect::route('admin.login')->with(array('flash_alert_notice' => 'We have sent authentication detail on your registered mail id, please verifiy !', 'flash_action' => 'success'))->withInput();
            }
        }
    }

    //datatable
    public function usersList($type) {
        $this->data['type'] = $type;
        return View::make('admin.users-list', $this->data);
    }

    public function categoryList() {
        //echo "hii";die;
        return View::make('admin.category-list');
    }

    public function subCategoryList() {
        //echo "hii";die;
        return View::make('admin.sub-category-list');
    }

    public function tripList() {
        return View::make('admin.trip-list');
    }

    public function contactList() {
        return View::make('admin.contact-list');
    }

    public function Advertise() {
        return View::make('admin.advertise');
    }

    public function ChangeImageLink(Request $request) {

        $destinationPathDelete = 'public/uploads/advertise';
        $result = DB::select('select id, ads_images from wgn_advertise WHERE id =1');
        File::delete($destinationPathDelete . '/' . $result[0]->ads_images);

        $link = $request->input('advertise_link');
        $pic_name = '';
        $image = Input::file('advertise_img');
        if (!empty($image)) {
            $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = 'public/uploads/advertise';
            $image->move($destinationPath, $input['imagename']);
            $pic_name = $input['imagename'];
        }
        $data = array(
            'ads_images' => $pic_name,
            'ads_link' => $link
        );
        $updateAdsData = DB::table('wgn_advertise')->where(array('id' => '1'))->update($data);
        return Redirect::route('admin.advertise');
    }

    public function usersData() {
        $dt = new DataTable();
        $data = $dt->fetch_data();
        echo json_encode($data);
    }

    //users detials single
    public function usersDetails($userID) {
        $this->data['users'] = DB::table('users')
                        ->select(DB::raw('id,username,street,phone,status,created_at,userimage,firstname,lastname,email,phone'))
                        ->where('id', $userID)->first();
        return View::make('admin.users-details', $this->data);
    }

    //sub-category-list detials single
    public function subCategoryLists($catID = '') {
        $this->data['category_id'] = $catID;
        return View::make('admin.sub-category-list', $this->data);
    }

    //second sub-category-list detials single
    public function SecoundSubCategoryList($SecondCatID = '') {
        $this->data['sub_category_id'] = $SecondCatID;
        return View::make('admin.second-sub-category-list', $this->data);
    }

    //for language 
    public function updateLangaugeFile() {
        $langData = DB::table('wgn_language')->get();

        $enFile = "resources/lang/en/message.php";
        $deFile = "resources/lang/de/message.php";
        $esFile = "resources/lang/es/message.php";
        $frFile = "resources/lang/fr/message.php";


        $en = "<?php";
        $en .= " return [";
        $de = "<?php";
        $de .= " return [";
        $es = "<?php";
        $es .= " return [";
        $fr = "<?php";
        $fr .= " return [";
        foreach ($langData as $value) {
            $en .= '"' . $value->key . '"' . "=>" . '"' . str_replace('"', "'", $value->en) . '"' . ",";
            $de .= '"' . $value->key . '"' . "=>" . '"' . str_replace('"', "'", $value->de) . '"' . ",";
            $es .= '"' . $value->key . '"' . "=>" . '"' . str_replace('"', "'", $value->es) . '"' . ",";
            $fr .= '"' . $value->key . '"' . "=>" . '"' . str_replace('"', "'", $value->fr) . '"' . ",";
        }
        $en = rtrim($en, ",");
        $en .= "] ?>";
        $de = rtrim($de, ",");
        $de .= "] ?>";
        $es = rtrim($es, ",");
        $es .= "] ?>";
        $fr = rtrim($fr, ",");
        $fr .= "] ?>";

        $file = fopen($enFile, "w");
        fwrite($file, $en);
        fclose($file);

        $file = fopen($deFile, "w");
        fwrite($file, $de);
        fclose($file);

        $file = fopen($esFile, "w");
        fwrite($file, $es);
        fclose($file);

        $file = fopen($frFile, "w");
        fwrite($file, $fr);
        fclose($file);
    }

    //CHANGE LANAUGES
    public function changeLanguage() {
        return View::make('admin.language-list', $this->data);
    }

    //UPDATE LANAUGES
    public function languageUpdateData() {
        $key = $_POST['key_id'];
        $result = DB::table('wgn_language')
                        ->select(DB::raw('*'))
                        ->where('id', $key)->first();
        echo json_encode($result);
    }

    //final lang
    public function finalCahngeLang(Request $request) {

        $lng_id = $request->input('lng_id');
        $en = $request->input('english');
        $de = $request->input('german');
        $fr = $request->input('french');
        $es = $request->input('spain');

        $updateData = array('en' => $en, 'de' => $de, 'fr' => $fr, 'es' => $es);
        $updateAdsData = DB::table('wgn_language')->where(array('id' => $lng_id))->update($updateData);
        if ($updateAdsData) {
            $this->updateLangaugeFile();
            return Redirect::route('admin.change_language')->with(array('flash_alert_notice' => 'Language updated successfully!', 'flash_action' => 'success'))->withInput();
        } else {
            return Redirect::route('admin.change_language')->with(array('flash_alert_notice' => 'Something went wrong. Try again!', 'flash_action' => 'danger'))->withInput();
        }
    }

    //add categoreis
    public function addCategory() {
        return View::make('admin.add-category', $this->data);
    }

    //insert categoreis
    public function insertCategory(Request $request) {

        $frecord = array(
            'category_name' => $request->input('first_cat'),
            'status' => '1'
        );
        $lastInsertedCatID = DB::table('wgn_item_category')->insertGetId($frecord);

        $srecord = array(
            'category_id' => $lastInsertedCatID,
            'subcat_name' => $request->input('second_cat'),
            'status' => '1'
        );
        $lastInsertedSecondCatID = DB::table('wgn_item_subcategory')->insertGetId($srecord);

        $trecord = array(
            'subcat_id' => $lastInsertedSecondCatID,
            'sub_cat_name' => $request->input('third_cat'),
            'status' => '1'
        );
        $final_data = DB::table('wgn_item_subcategory_two')->insertGetId($trecord);
        if ($final_data) {
            return Redirect::route('admin.category-list')->with(array('flash_alert_notice' => 'Category added successfully!', 'flash_action' => 'success'))->withInput();
        }
    }

    //update categories
    public function updateCategory($catID) {
        $checkCateUpdateStaus = explode('_', $catID);
        $statusCat = $checkCateUpdateStaus[1];
        $CatID = $checkCateUpdateStaus[0];
        if ($statusCat == 'catone') {
            $result = DB::select('select category_name from wgn_item_category WHERE id =' . $CatID);
            $catData = array(
                'catData' => $result[0]->category_name,
                'catID' => $CatID,
                'table' => 'wgn_item_category',
                'cat_name' => 'category_name'
            );
            return View::make('admin.edit-category')->with('my_data', $catData);
        }
        if ($statusCat == 'catwo') {
            $result = DB::select('select subcat_name from wgn_item_subcategory WHERE id =' . $CatID);
            $catData = array(
                'catData' => $result[0]->subcat_name,
                'catID' => $CatID,
                'table' => 'wgn_item_subcategory',
                'cat_name' => 'subcat_name'
            );
            return View::make('admin.edit-category')->with('my_data', $catData);
        }
        if ($statusCat == 'cathree') {

            $result = DB::select('select sub_cat_name from wgn_item_subcategory_two WHERE id =' . $CatID);
            $catData = array(
                'catData' => $result[0]->sub_cat_name,
                'catID' => $CatID,
                'table' => 'wgn_item_subcategory_two',
                'cat_name' => 'sub_cat_name'
            );
            return View::make('admin.edit-category')->with('my_data', $catData);
        }
    }

    //edit page cateegory 
    public function EditPageCategory() {
        return View::make('admin.edit-category', $this->data);
    }

    //submit update
    public function updateCategorySubmit() {
        $Data = array($_POST['cat_name'] => $_POST['first_cat']);
        $updateCatData = DB::table($_POST['table'])->where(array('id' => $_POST['catID']))->update($Data);
        if ($updateCatData) {
            return Redirect::route('admin.category-list')->with(array('flash_alert_notice' => 'Category updated successfully!', 'flash_action' => 'success'))->withInput();
        } else {
            return Redirect::route('admin.category-list')->with(array('flash_alert_notice' => 'Something went wrong!', 'flash_action' => 'danger'))->withInput();
        }
    }

}
