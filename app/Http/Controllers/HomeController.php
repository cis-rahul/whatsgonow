<?php

namespace App\Http\Controllers;

use App\Http\Controllers;
use App\Http\Controllers\Auth\AuthController;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\ContactRequest;
use Illuminate\Support\Facades\Redirect;
use View;
use App\User;
use App\Contact;
use App\Items;
use App\Trips;
use App\UserDetail;
use App\Countries;
use App\RequestItem;
use Session;
use Auth;
use Validator;
use Input;
use DB;
use Mail;

//use Input;


class HomeController extends AuthController {

    /**
     * Created By: Rahul Gupta
     * Created Dt: 01-Sep-2016
     */
    protected $data = array();

    public function __construct() {
        if (Auth::check()) {
            if (Auth::user()->role == '2') {
                $userDetailObj = UserDetail::Where('userid', Auth::user()->id)->first();
                if (empty($userDetailObj)) {
                    return Redirect::route('user.driver_registration')->send();
                }
            }
            $this->data['userinfo'] = Auth::User();
            //Notification
            $this->data['user_notification'] = DB::table('wgn_notification')
                    ->select('wgn_notification.*', 'users.firstname as senderfirstname', 'users.lastname as senderlastname', 'wgn_item.product_name')
                    ->Leftjoin('users', 'wgn_notification.sender_id', '=', 'users.id')
                    ->Leftjoin('wgn_item', 'wgn_notification.item_id', '=', 'wgn_item.id')
                    ->Where(array('wgn_notification.status' => '1', 'wgn_notification.receiver_id' => $this->data['userinfo']->id))
                    ->orderBy('wgn_notification.id', 'Desc')
                    ->get();
        }
        $countries = DB::table('countries')->where('status', '1')->get();
        $this->data['countries'] = array();
        foreach ($countries as $objCountries) {
            $this->data['countries'][$objCountries->id] = $objCountries->name;
        }
        $cartypes = DB::table('wgn_cartype')->where('status', '1')->orderBy('order', 'ASC')->get();
        $this->data['cartypes'] = array();
        foreach ($cartypes as $objCartype) {
            $this->data['cartypes'][$objCartype->id] = $objCartype->type;
        }
        $this->data['currency'] = array('USD' => '$', 'EUR' => '&#8364;', 'GBP' => '&pound;', 'AUD' => 'A$');
        $this->data['notifi_type'] = array();
        $wgn_notification_type = DB::table('wgn_notification_type')->Where('status', '1')->get();
        foreach ($wgn_notification_type as $objNotificationType) {
            $this->data['notifi_type'][$objNotificationType->type] = $objNotificationType->title;
        }
    }

    //Get Method
    public function index() {
        $result = DB::select('select id, ads_images, ads_link from wgn_advertise WHERE id =1');
        $this->data['link_data'] = $result;
        return View::make('home', $this->data);
    }

    //Get Method
    public function about() {
        return View::make('about', $this->data);
    }

    public function getlogin() {
        if (!Auth::check())
            return View::make('login');
        else
            return Redirect::route('user.dashboard');
    }

    public function signup() {
        if (!Auth::check()) {
            return View::make('registration', $this->data);
        } else {
            return Redirect::route('user.dashboard');
        }
    }

    //Post Method

    public function signin(LoginRequest $request) {
        if (!Auth::check()) {
            $credentials_un = array('username' => $request->input('username'), 'password' => $request->input('password'));
            $credentials_em = array('email' => $request->input('username'), 'password' => $request->input('password'));
            if (Auth::attempt($credentials_un) || Auth::attempt($credentials_em)) {
                if (Auth::user()->role == '2') {
                    $userDetailObj = UserDetail::Where('userid', Auth::user()->id)->first();
                    if (empty($userDetailObj)) {
                        return Redirect::route('user.driver_registration');
                    }
                }
                return Redirect::route('user.dashboard');
            } else {
                return Redirect::to('signin')->with(array('flash_alert_notice' => 'Wrong Username or Password. Try again!', 'flash_action' => 'danger'))->withInput();
            }
        } else {
            return Redirect::route('user.dashboard');
        }
    }

    public function dosignup(RegistrationRequest $request) {        
        if (!Auth::check()) {
            $userinfo = User::select('username')->where('username', '=', $request->input('username'))->first();
            $flash = "";
            if (empty($userinfo)) {
                if ($users = $this->create($request->all())) {
                    $path = base_path() . '/public/uploads/users';
                    if (!empty(Input::file('userimage')) && Input::file('userimage')->getError() == 0) {
                        $fileName = $this->fileUpload(Input::file('userimage'), $path, "");
                        $userinfo = User::find($users->id);
                        $userinfo->userimage = $fileName;
                        $userinfo->save();
                    }
                    $flash = array('flash_alert_notice' => 'User Registration done successfully !', 'flash_action' => 'success');
                } else {
                    $flash = array('flash_alert_notice' => 'Somthing went wrong!', 'flash_action' => 'danger');
                }
            } else {
                $flash = array('flash_alert_notice' => 'This user already exist !', 'flash_action' => 'success');
            }
            return Redirect::to('signin')->with($flash)->withInput();
        } else {
            return Redirect::route('user.dashboard');
        }
    }

    //drive list page start
    public function driverList() {
        $currentdate = date('Y-m-d');
        $this->data['drivers'] = DB::table('users')
                ->select(DB::raw('count(wgn_trip.userid) as tripCount, users.id,users.userimage,users.firstname,users.lastname,wgn_cartype.type'))
                ->leftJoin('wgn_usersdetail', 'users.id', '=', 'wgn_usersdetail.userid')
                ->leftJoin('wgn_cartype', 'wgn_usersdetail.cartype', '=', 'wgn_cartype.id')
                ->leftJoin('wgn_trip', 'users.id', '=', 'wgn_trip.userid')
                ->where('users.role', 2)
                ->where('users.status', 1)
                //->where('wgn_trip.trip_date', '<', $currentdate)
                ->groupBy('wgn_trip.userid')
                ->simplePaginate(10);

        return View::make('listofdrivers', $this->data);
    }

    //Driver profile view
    public function driverDetail($driverID) {
        $this->data['driverDetails'] = DB::table('users')
                ->select('users.id', 'users.userimage', 'users.city', 'users.firstname', 'users.lastname', 'users.country', 'wgn_usersdetail.cartype')
                ->leftJoin('wgn_usersdetail', 'users.id', '=', 'wgn_usersdetail.userid')
                ->where('users.id', '=', $driverID)
                ->first();

        return View::make('driverDetail', $this->data);
    }

    //trip list by serach
    public function tripSearch(Request $request) {

        //filter for trips

        if (!empty($request->input('filterData'))) {

            $checkboxvalue = $request->input('checkbox');
            switch ($checkboxvalue) {
                case 'search_location':
                    $file = array('location_postalCode' => $request->input('location_postalCode'), 'location_city' => $request->input('location_city'), 'location_country' => $request->input('location_country'));
                    $rules = array('location_postalCode' => 'required', 'location_city' => 'required', 'location_country' => 'required');
                    break;
                case 'search_destination':
                    $file = array('destination_postalCode' => $request->input('destination_postalCode'), 'destination_city' => $request->input('destination_city'), 'destination_country' => $request->input('destination_country'));
                    $rules = array('destination_postalCode' => 'required', 'destination_city' => 'required', 'destination_country' => 'required');
                    break;
                case 'search_date':
                    $file = array('filter_Date' => $request->input('filter_Date'));
                    $rules = array('filter_Date' => 'required');
                    break;
            }
            $validator = Validator::make($file, $rules);
            if ($validator->fails()) {
                return Redirect::route('user.trip-list')->withErrors($validator)->withInput();
            } else {

                $checkboxvalue = $request->input('checkbox');
                switch ($checkboxvalue) {
                    case 'search_location':

                        $lcoaiton_postalCode = $request->input('location_postalCode');
                        $lcoaiton_City = $request->input('location_city');
                        $lcoaiton_Country = $request->input('location_country');

                        if (isset($lcoaiton_postalCode) || isset($lcoaiton_City) || isset($lcoaiton_Country)) {
                            if ($validator->fails()) {
                                return Redirect::route('user.trip-list')->withErrors($validator)->withInput();
                            } else {
                                $fromAddress = $lcoaiton_postalCode . ', ' . $lcoaiton_City . ', ' . $this->data['countries'][$lcoaiton_Country];

                                $store_addressFrom = str_replace(' ', '+', $fromAddress);

                                $geocode = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . $store_addressFrom . "&sensor=false");
                                $output = json_decode($geocode);

                                if (isset($output->results) && !empty($output->results)) {
                                    $fromlat = isset($output->results[0]) ? $output->results[0]->geometry->location->lat : '';
                                    $fromlng = isset($output->results[0]) ? $output->results[0]->geometry->location->lng : '';
                                } else {
                                    Return Redirect::route('user.item-list')->with(array('flash_alert_notice' => 'Please enter valid data for seach!', 'flash_action' => 'warning'))->withInput();
                                }

                                if ((isset($fromlat) && !empty($fromlat)) && (isset($fromlng) && !empty($fromlng))) {


                                    $this->data['trips'] = Trips::Select('wgn_trip.*', 'users.userimage', 'users.firstname', DB::raw('(3959 * acos (
                                              cos ( radians(' . $fromlat . ') )
                                              * cos( radians( from_latitude ) )
                                              * cos( radians( from_longitude ) - radians(' . $fromlng . ') )
                                              + sin ( radians(' . $fromlat . ') )
                                              * sin( radians( from_latitude ) )
                                            )
                                    ) AS distance'))
                                                    ->having('distance', '<', '5')
                                                    ->orderBy('distance', 'ASC')
                                                    ->whereRaw("CONCAT(trip_date, ' ', trip_time) >='" . date('Y-m-d H:i:s') . "'")
                                                    ->leftJoin('users', 'users.id', '=', 'wgn_trip.userid')->simplePaginate(10);

                                    if (empty($this->data['trips'])) {
                                        return Redirect::route('user.trip-list')->with(array('flash_alert_notice' => 'No such item found!', 'flash_action' => 'warning'))->withInput();
                                    }
                                    return View::make('listoftrips', $this->data);
                                } else {
                                    return Redirect::route('user.trip-list')->with(array('flash_alert_notice' => 'No such item found!', 'flash_action' => 'warning'))->withInput();
                                }
                            }
                        }
                        break;
                    case 'search_destination':

                        $destination_postalCode = $request->input('destination_postalCode');
                        $destination_City = $request->input('destination_city');
                        $destination_Country = $request->input('destination_country');

                        if (isset($destination_postalCode) || isset($destination_City) || isset($destination_Country)) {
                            if ($validator->fails()) {
                                return Redirect::route('user.trip-list')->withErrors($validator)->withInput();
                            } else {
                                $fromAddress = $destination_postalCode . ', ' . $destination_City . ', ' . $this->data['countries'][$destination_Country];
                                $store_addressFrom = str_replace(' ', '+', $fromAddress);

                                $geocode = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . $store_addressFrom . "&sensor=false");
                                $output = json_decode($geocode);
                                if (isset($output->results) && !empty($output->results)) {
                                    $fromlat = isset($output->results[0]) ? $output->results[0]->geometry->location->lat : '';
                                    $fromlng = isset($output->results[0]) ? $output->results[0]->geometry->location->lng : '';
                                } else {
                                    Return Redirect::route('user.trip-list')->with(array('flash_alert_notice' => 'Please enter valid data for seach!', 'flash_action' => 'warning'))->withInput();
                                }

                                if ((isset($fromlat) && !empty($fromlat)) && (isset($fromlng) && !empty($fromlng))) {
                                    $this->data['trips'] = Trips::Select('wgn_trip.*', 'users.userimage', 'users.firstname', DB::raw('(3959 * acos (
                                              cos ( radians(' . $fromlat . ') )
                                              * cos( radians( to_latitude ) )
                                              * cos( radians( to_longitude ) - radians(' . $fromlng . ') )
                                              + sin ( radians(' . $fromlat . ') )
                                              * sin( radians( to_latitude ) )
                                            )
                                    ) AS distance'))
                                                    ->having('distance', '<', '5')
                                                    ->orderBy('distance', 'ASC')
                                                    ->whereRaw("CONCAT(trip_date, ' ', trip_time) >='" . date('Y-m-d H:i:s') . "'")
                                                    ->leftJoin('users', 'users.id', '=', 'wgn_trip.userid')->simplePaginate(10);


                                    if (empty($this->data['trips'])) {
                                        return Redirect::route('user.trip-list')->with(array('flash_alert_notice' => 'No such item found!', 'flash_action' => 'warning'))->withInput();
                                    }
                                    return View::make('listoftrips', $this->data);
                                } else {
                                    return Redirect::route('user.trip-list')->with(array('flash_alert_notice' => 'No such item found!', 'flash_action' => 'warning'))->withInput();
                                }
                            }
                        }
                        break;
                    case 'search_date':
                        $Filter_Date = $request->input('filter_Date');
                        $this->data['trips'] = Trips::Select('wgn_trip.*', 'users.userimage', 'users.firstname')
                                ->orderBy('id', 'Desc')
                                ->leftJoin('users', 'users.id', '=', 'wgn_trip.userid')
                                ->where('wgn_trip.trip_date', 'Like', $Filter_Date . '%')
                                ->whereRaw("CONCAT(trip_date, ' ', trip_time) >='" . date('Y-m-d H:i:s') . "'")
                                ->simplePaginate(10);
                        return View::make('listoftrips', $this->data);
                        break;
                }
            }
        }
        //end of filter trips



        $file = array('fromPostalCode' => $request->input('fromPostalCode'), 'toPostalCode' => $request->input('toPostalCode'), 'fromCity' => $request->input('fromCity'), 'toCity' => $request->input('toCity'), 'fromCountry' => $request->input('fromCountry'), 'toCountry' => $request->input('toCountry'));
        $rules = array('fromPostalCode' => 'required', 'toPostalCode' => 'required', 'fromCity' => 'required', 'toCity' => 'required', 'fromCountry' => 'required', 'toCountry' => 'required');
        $validator = Validator::make($file, $rules);
        if (isset($_GET['fromPostalCode']) || isset($_GET['toPostalCode']) || isset($_GET['fromCity']) || isset($_GET['toCity']) || isset($_GET['fromCountry']) || isset($_GET['toCountry'])) {
            if ($validator->fails()) {
                return Redirect::route('home')->with(array('flash_alert_notice' => 'All fields are maindatory!', 'flash_action' => 'warning'))->withInput();
            } else {
                $fromAddress = $_GET['fromPostalCode'] . ', ' . $_GET['fromCity'] . ', ' . $this->data['countries'][$_GET['fromCountry']];
                $toAddress = $_GET['toPostalCode'] . ', ' . $_GET['toCity'] . ', ' . $this->data['countries'][$_GET['toCountry']];

                $store_addressFrom = str_replace(' ', '+', $fromAddress);
                $store_addressTo = str_replace(' ', '+', $toAddress);

                $geocode = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . $store_addressFrom . "&sensor=false");
                $output = json_decode($geocode);

                if (isset($output->results) && !empty($output->results)) {
                    $fromlat = $output->results[0]->geometry->location->lat;
                    $fromlng = $output->results[0]->geometry->location->lng;
                } else {
                    Return Redirect::route('home')->with(array('flash_alert_notice' => 'Please enter valid data for seach!', 'flash_action' => 'danger'))->withInput();
                }

                $geocodeTo = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . $store_addressTo . "&sensor=false");
                $outputTo = json_decode($geocodeTo);

                if (isset($output->results) && !empty($output->results)) {
                    $tolat = $outputTo->results[0]->geometry->location->lat;
                    $tolng = $outputTo->results[0]->geometry->location->lng;
                } else {
                    return Redirect::route('home');
                }
                if ((isset($tolat) && !empty($tolat)) && (isset($tolng) && !empty($tolng)) && (isset($fromlat) && !empty($fromlat)) && (isset($fromlng) && !empty($fromlng))) {
                    $this->data['trip_from'] = Trips::Select('wgn_trip.*', 'users.userimage', 'users.firstname', DB::raw('(3959 * acos (
                                              cos ( radians(' . $fromlat . ') )
                                              * cos( radians( from_latitude ) )
                                              * cos( radians( from_longitude ) - radians(' . $fromlng . ') )
                                              + sin ( radians(' . $fromlat . ') )
                                              * sin( radians( from_latitude ) )
                                            )
                                    ) AS distance'))
                            ->leftJoin('users', 'users.id', '=', 'wgn_trip.userid')
                            ->whereRaw("CONCAT(trip_date, ' ', trip_time) >='" . date('Y-m-d H:i:s') . "'")
                            ->having('distance', '<', '5');




                    $this->data['trips'] = Trips::Select('wgn_trip.*', 'users.userimage', 'users.firstname', DB::raw('(3959 * acos (
                                          cos ( radians(' . $tolat . ') )
                                          * cos( radians( to_latitude ) )
                                          * cos( radians( to_longitude ) - radians(' . $tolng . ') )
                                          + sin ( radians(' . $tolat . ') )
                                          * sin( radians( to_latitude ))
                                        )) as distance'))
                                    ->union($this->data['trip_from'])
                                    ->orderBy('distance', 'ASC')
                                    ->leftJoin('users', 'users.id', '=', 'wgn_trip.userid')
                                    ->whereRaw("CONCAT(trip_date, ' ', trip_time) >='" . date('Y-m-d H:i:s') . "'")
                                    ->having('distance', '<', '5')->simplePaginate(10);
                    return View::make('listoftrips', $this->data);
                } else {
                    return Redirect::route('home')->with(array('flash_alert_notice' => 'No such item found!', 'flash_action' => 'warning'))->withInput();
                }
            }
        }
        $this->data['trips'] = Trips::Select('wgn_trip.*', 'users.userimage', 'users.firstname')
                ->orderBy('id', 'Desc')
                ->leftJoin('users', 'users.id', '=', 'wgn_trip.userid')
                ->whereRaw("CONCAT(trip_date, ' ', trip_time) >='" . date('Y-m-d H:i:s') . "'")
                ->simplePaginate(10);
        return View::make('listoftrips', $this->data);
    }

    //trip mape view//mape view for items
    function tripListmap(Request $request) {


        //filter for trips

        if (!empty($request->input('filterData'))) {

            $checkboxvalue = $request->input('checkbox');
            switch ($checkboxvalue) {
                case 'search_location':
                    $file = array('location_postalCode' => $request->input('location_postalCode'), 'location_city' => $request->input('location_city'), 'location_country' => $request->input('location_country'));
                    $rules = array('location_postalCode' => 'required', 'location_city' => 'required', 'location_country' => 'required');
                    break;
                case 'search_destination':
                    $file = array('destination_postalCode' => $request->input('destination_postalCode'), 'destination_city' => $request->input('destination_city'), 'destination_country' => $request->input('destination_country'));
                    $rules = array('destination_postalCode' => 'required', 'destination_city' => 'required', 'destination_country' => 'required');
                    break;
                case 'search_date':
                    $file = array('filter_Date' => $request->input('filter_Date'));
                    $rules = array('filter_Date' => 'required');
                    break;
            }
            $validator = Validator::make($file, $rules);
            if ($validator->fails()) {
                return Redirect::route('user.trip-list')->withErrors($validator)->withInput();
            } else {

                $checkboxvalue = $request->input('checkbox');
                switch ($checkboxvalue) {
                    case 'search_location':

                        $lcoaiton_postalCode = $request->input('location_postalCode');
                        $lcoaiton_City = $request->input('location_city');
                        $lcoaiton_Country = $request->input('location_country');

                        if (isset($lcoaiton_postalCode) || isset($lcoaiton_City) || isset($lcoaiton_Country)) {
                            if ($validator->fails()) {
                                return Redirect::route('user.trip-list')->withErrors($validator)->withInput();
                            } else {
                                $fromAddress = $lcoaiton_postalCode . ', ' . $lcoaiton_City . ', ' . $this->data['countries'][$lcoaiton_Country];

                                $store_addressFrom = str_replace(' ', '+', $fromAddress);

                                $geocode = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . $store_addressFrom . "&sensor=false");
                                $output = json_decode($geocode);

                                if (isset($output->results) && !empty($output->results)) {
                                    $fromlat = isset($output->results[0]) ? $output->results[0]->geometry->location->lat : '';
                                    $fromlng = isset($output->results[0]) ? $output->results[0]->geometry->location->lng : '';
                                } else {
                                    Return Redirect::route('user.item-list')->with(array('flash_alert_notice' => 'Please enter valid data for seach!', 'flash_action' => 'warning'))->withInput();
                                }

                                if ((isset($fromlat) && !empty($fromlat)) && (isset($fromlng) && !empty($fromlng))) {


                                    $this->data['trips'] = Trips::Select('wgn_trip.*', 'users.userimage', 'users.firstname', DB::raw('(3959 * acos (
                                              cos ( radians(' . $fromlat . ') )
                                              * cos( radians( from_latitude ) )
                                              * cos( radians( from_longitude ) - radians(' . $fromlng . ') )
                                              + sin ( radians(' . $fromlat . ') )
                                              * sin( radians( from_latitude ) )
                                            )
                                    ) AS distance'))
                                                    ->having('distance', '<', '5')
                                                    ->orderBy('distance', 'ASC')
                                                    ->whereRaw("CONCAT(trip_date, ' ', trip_time) >='" . date('Y-m-d H:i:s') . "'")
                                                    ->leftJoin('users', 'users.id', '=', 'wgn_trip.userid')->simplePaginate(10);

                                    if (empty($this->data['trips'])) {
                                        return Redirect::route('user.trip-list')->with(array('flash_alert_notice' => 'No such item found!', 'flash_action' => 'warning'))->withInput();
                                    }
                                    return View::make('listoftripsmap', $this->data);
                                } else {
                                    return Redirect::route('user.trip-list')->with(array('flash_alert_notice' => 'No such item found!', 'flash_action' => 'warning'))->withInput();
                                }
                            }
                        }
                        break;
                    case 'search_destination':

                        $destination_postalCode = $request->input('destination_postalCode');
                        $destination_City = $request->input('destination_city');
                        $destination_Country = $request->input('destination_country');

                        if (isset($destination_postalCode) || isset($destination_City) || isset($destination_Country)) {
                            if ($validator->fails()) {
                                return Redirect::route('user.trip-list')->withErrors($validator)->withInput();
                            } else {
                                $fromAddress = $destination_postalCode . ', ' . $destination_City . ', ' . $this->data['countries'][$destination_Country];
                                $store_addressFrom = str_replace(' ', '+', $fromAddress);

                                $geocode = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . $store_addressFrom . "&sensor=false");
                                $output = json_decode($geocode);
                                if (isset($output->results) && !empty($output->results)) {
                                    $fromlat = isset($output->results[0]) ? $output->results[0]->geometry->location->lat : '';
                                    $fromlng = isset($output->results[0]) ? $output->results[0]->geometry->location->lng : '';
                                } else {
                                    Return Redirect::route('user.trip-list')->with(array('flash_alert_notice' => 'Please enter valid data for seach!', 'flash_action' => 'warning'))->withInput();
                                }

                                if ((isset($fromlat) && !empty($fromlat)) && (isset($fromlng) && !empty($fromlng))) {
                                    $this->data['trips'] = Trips::Select('wgn_trip.*', 'users.userimage', 'users.firstname', DB::raw('(3959 * acos (
                                              cos ( radians(' . $fromlat . ') )
                                              * cos( radians( to_latitude ) )
                                              * cos( radians( to_longitude ) - radians(' . $fromlng . ') )
                                              + sin ( radians(' . $fromlat . ') )
                                              * sin( radians( to_latitude ) )
                                            )
                                    ) AS distance'))
                                                    ->having('distance', '<', '5')
                                                    ->orderBy('distance', 'ASC')
                                                    ->whereRaw("CONCAT(trip_date, ' ', trip_time) >='" . date('Y-m-d H:i:s') . "'")
                                                    ->leftJoin('users', 'users.id', '=', 'wgn_trip.userid')->simplePaginate(10);


                                    if (empty($this->data['trips'])) {
                                        return Redirect::route('user.trip-list')->with(array('flash_alert_notice' => 'No such item found!', 'flash_action' => 'warning'))->withInput();
                                    }
                                    return View::make('listoftripsmap', $this->data);
                                } else {
                                    return Redirect::route('user.trip-list')->with(array('flash_alert_notice' => 'No such item found!', 'flash_action' => 'warning'))->withInput();
                                }
                            }
                        }
                        break;
                    case 'search_date':
                        $Filter_Date = $request->input('filter_Date');
                        $this->data['trips'] = Trips::Select('wgn_trip.*', 'users.userimage', 'users.firstname')
                                ->orderBy('id', 'Desc')
                                ->leftJoin('users', 'users.id', '=', 'wgn_trip.userid')
                                ->where('wgn_trip.trip_date', 'Like', $Filter_Date . '%')
                                ->whereRaw("CONCAT(trip_date, ' ', trip_time) >='" . date('Y-m-d H:i:s') . "'")
                                ->get();
                        return View::make('listoftripsmap', $this->data);
                        break;
                }
            }
        }
        //end of filter trips




        $file = array('fromPostalCode' => $request->input('fromPostalCode'), 'toPostalCode' => $request->input('toPostalCode'), 'fromCity' => $request->input('fromCity'), 'toCity' => $request->input('toCity'), 'fromCountry' => $request->input('fromCountry'), 'toCountry' => $request->input('toCountry'));
        $rules = array('fromPostalCode' => 'required', 'toPostalCode' => 'required', 'fromCity' => 'required', 'toCity' => 'required', 'fromCountry' => 'required', 'toCountry' => 'required');
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
            return Redirect::route('home')->withErrors($validator)->withInput();
        } else {
            $fromAddress = $_GET['fromPostalCode'] . ', ' . $_GET['fromCity'] . ', ' . $this->data['countries'][$_GET['fromCountry']];
            $toAddress = $_GET['toPostalCode'] . ', ' . $_GET['toCity'] . ', ' . $this->data['countries'][$_GET['toCountry']];

            $store_addressFrom = str_replace(' ', '+', $fromAddress);
            $store_addressTo = str_replace(' ', '+', $toAddress);

            $geocode = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . $store_addressFrom . "&sensor=false");
            $output = json_decode($geocode);

            $fromlat = isset($output->results[0]) ? $output->results[0]->geometry->location->lat : '';
            $fromlng = isset($output->results[0]) ? $output->results[0]->geometry->location->lng : '';


            $geocodeTo = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . $store_addressTo . "&sensor=false");
            $outputTo = json_decode($geocodeTo);

            $tolat = $outputTo->results[0]->geometry->location->lat;
            $tolng = $outputTo->results[0]->geometry->location->lng;

            //echo "$fromlat, $fromlng === $tolat, $tolng";
//                if ((isset($tolat) && !empty($tolat)) || (isset($tolng) && !empty($tolng))) {
//                    $data['fromRecords'] = DB::select(DB::raw("SELECT wgn_trip.*,users.userimage,users.firstname,
//                                            (
//                                              3959 * acos (
//                                              cos ( radians($fromlat) )
//                                              * cos( radians( from_latitude ) )
//                                              * cos( radians( from_longitude ) - radians($fromlng) )
//                                              + sin ( radians($fromlat) )
//                                              * sin( radians( from_latitude ) )
//                                            )
//                                    ) AS distance 
//                                    FROM wgn_trip
//                                    left join users on users.id = wgn_trip.userid
//                                    HAVING distance < 5
//                                    ORDER BY distance "));
//
//
//
//                    $data['toRecords'] = DB::select(DB::raw("SELECT wgn_trip.*,users.userimage,users.firstname,
//                                            (
//                                              3959 * acos (
//                                              cos ( radians($tolat) )
//                                              * cos( radians( to_latitude ) )
//                                              * cos( radians( to_longitude ) - radians($tolng) )
//                                              + sin ( radians($tolat) )
//                                              * sin( radians( to_latitude ) )
//                                            )
//                                    ) AS distance 
//                                    FROM wgn_trip
//                                    left join users on users.id = wgn_trip.userid
//                                    HAVING distance < 5
//                                    ORDER BY distance "));
//
//                    $this->data['trips'] = array_merge($data['fromRecords'], $data['toRecords']);
//                    return View::make('listoftripsmap', $this->data);
//                }
            if ((isset($tolat) && !empty($tolat)) && (isset($tolng) && !empty($tolng)) && (isset($fromlat) && !empty($fromlat)) && (isset($fromlng) && !empty($fromlng))) {
                $this->data['trip_from'] = Trips::Select('wgn_trip.*', 'users.userimage', 'users.firstname', DB::raw('(3959 * acos (
					  cos ( radians(' . $fromlat . ') )
					  * cos( radians( from_latitude ) )
					  * cos( radians( from_longitude ) - radians(' . $fromlng . ') )
					  + sin ( radians(' . $fromlat . ') )
					  * sin( radians( from_latitude ) )
					)
				) AS distance'))
                        ->leftJoin('users', 'users.id', '=', 'wgn_trip.userid')
                        ->whereRaw("CONCAT(trip_date, ' ', trip_time) >='" . date('Y-m-d H:i:s') . "'")
                        ->having('distance', '<', '5');



                $this->data['trips'] = Trips::Select('wgn_trip.*', 'users.userimage', 'users.firstname', DB::raw('(3959 * acos (
                                      cos ( radians(' . $tolat . ') )
                                      * cos( radians( to_latitude ) )
                                      * cos( radians( to_longitude ) - radians(' . $tolng . ') )
                                      + sin ( radians(' . $tolat . ') )
                                      * sin( radians( to_latitude ))
                                    )) as distance'))
                                ->union($this->data['trip_from'])
                                ->having('distance', '<', '5')
                                ->orderBy('distance', 'ASC')
                                ->whereRaw("CONCAT(trip_date, ' ', trip_time) >='" . date('Y-m-d H:i:s') . "'")
                                ->leftJoin('users', 'users.id', '=', 'wgn_trip.userid')->get();
                return View::make('listoftripsmap', $this->data);
            } else {
                Return Redirect::route('home')->with('flash_alert_notice', 'No such items found!')->withInput();
            }
        }
    }

    //Item list page start
    public function itemList(Request $request) {

        if (!empty($request->input('filterData'))) {
            $checkboxvalue = $request->input('checkbox');
            switch ($checkboxvalue) {
                case 'search_location':
                    $file = array('location_postalCode' => $request->input('location_postalCode'), 'location_city' => $request->input('location_city'), 'location_country' => $request->input('location_country'));
                    $rules = array('location_postalCode' => 'required', 'location_city' => 'required', 'location_country' => 'required');
                    break;
                case 'search_destination':
                    $file = array('destination_postalCode' => $request->input('destination_postalCode'), 'destination_city' => $request->input('destination_city'), 'destination_country' => $request->input('destination_country'));
                    $rules = array('destination_postalCode' => 'required', 'destination_city' => 'required', 'destination_country' => 'required');
                    break;
                case 'search_date':
                    $file = array('filter_Date' => $request->input('filter_Date'));
                    $rules = array('filter_Date' => 'required');
                    break;
                case 'search_amount':
                    $file = array('filter_amount' => $request->input('filter_amount'));
                    $rules = array('filter_amount' => 'required');
                    break;
            }
            $validator = Validator::make($file, $rules);
            if ($validator->fails()) {
                return Redirect::route('user.item-list')->withErrors($validator)->withInput();
            } else {

                $checkboxvalue = $request->input('checkbox');
                switch ($checkboxvalue) {
                    case 'search_location':

                        $lcoaiton_postalCode = $request->input('location_postalCode');
                        $lcoaiton_City = $request->input('location_city');
                        $lcoaiton_Country = $request->input('location_country');

                        if (isset($lcoaiton_postalCode) || isset($lcoaiton_City) || isset($lcoaiton_Country)) {
                            if ($validator->fails()) {
                                return Redirect::route('user.item-list')->withErrors($validator)->withInput();
                            } else {
                                $fromAddress = $lcoaiton_postalCode . ', ' . $lcoaiton_City . ', ' . $this->data['countries'][$lcoaiton_Country];

                                $store_addressFrom = str_replace(' ', '+', $fromAddress);

                                $geocode = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . $store_addressFrom . "&sensor=false");
                                $output = json_decode($geocode);

                                if (isset($output->results) && !empty($output->results)) {
                                    $fromlat = isset($output->results[0]) ? $output->results[0]->geometry->location->lat : '';
                                    $fromlng = isset($output->results[0]) ? $output->results[0]->geometry->location->lng : '';
                                } else {
                                    Return Redirect::route('user.item-list')->with(array('flash_alert_notice' => 'Please enter valid data for seach!', 'flash_action' => 'warning'))->withInput();
                                }

                                if ((isset($fromlat) && !empty($fromlat)) && (isset($fromlng) && !empty($fromlng))) {
                                    $this->data['item'] = Items::Select('wgn_item.id', 'wgn_item.item_currency', 'wgn_item.item_pieces', 'wgn_item.item_image1', 'wgn_item.item_image2', 'wgn_item.item_image3', 'wgn_item.from_city', 'wgn_item.from_postal_code', 'wgn_item.from_postal_code', 'wgn_item.from_country', 'wgn_item.to_city', 'wgn_item.to_postal_code', 'wgn_item.to_country', 'wgn_item.item_price', 'wgn_currency.currency_type', DB::raw('(3959 * acos (
					  cos ( radians(' . $fromlat . ') )
					  * cos( radians( from_latitude ) )
					  * cos( radians( from_longitude ) - radians(' . $fromlng . ') )
					  + sin ( radians(' . $fromlat . ') )
					  * sin( radians( from_latitude ) )
					)
                                    ) AS distance'))
                                                    ->having('distance', '<', '5')
                                                    ->orderBy('distance', 'ASC')
                                                    ->where('wgn_item.status', '1')
                                                    ->Join('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')->simplePaginate(10);

                                    if (empty($this->data['item'])) {
                                        return Redirect::route('user.item-list')->with(array('flash_alert_notice' => 'No such item found!', 'flash_action' => 'warning'))->withInput();
                                    }


                                    return View::make('listofitems', $this->data);
                                } else {
                                    return Redirect::route('user.item-list')->with(array('flash_alert_notice' => 'No such item found!', 'flash_action' => 'warning'))->withInput();
                                }
                            }
                        }
                        break;
                    case 'search_destination':

                        $destination_postalCode = $request->input('destination_postalCode');
                        $destination_City = $request->input('destination_city');
                        $destination_Country = $request->input('destination_country');

                        if (isset($destination_postalCode) || isset($destination_City) || isset($destination_Country)) {
                            if ($validator->fails()) {
                                return Redirect::route('user.item-list')->withErrors($validator)->withInput();
                            } else {
                                $fromAddress = $destination_postalCode . ' ' . $destination_City . ' ' . $this->data['countries'][$destination_Country];
                                $store_addressFrom = str_replace(' ', '+', $fromAddress);

                                $geocode = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . $store_addressFrom . "&sensor=false");
                                $output = json_decode($geocode);

                                if (isset($output->results) && !empty($output->results)) {
                                    $fromlat = isset($output->results[0]) ? $output->results[0]->geometry->location->lat : '';
                                    $fromlng = isset($output->results[0]) ? $output->results[0]->geometry->location->lng : '';
                                } else {
                                    Return Redirect::route('user.item-list')->with(array('flash_alert_notice' => 'No result found!', 'flash_action' => 'warning'))->withInput();
                                }

                                if ((isset($fromlat) && !empty($fromlat)) && (isset($fromlng) && !empty($fromlng))) {
                                    $this->data['item'] = Items::Select('wgn_item.id', 'wgn_item.item_currency', 'wgn_item.item_pieces', 'wgn_item.item_image1', 'wgn_item.item_image2', 'wgn_item.item_image3', 'wgn_item.from_city', 'wgn_item.from_postal_code', 'wgn_item.from_postal_code', 'wgn_item.from_country', 'wgn_item.to_city', 'wgn_item.to_postal_code', 'wgn_item.to_country', 'wgn_item.item_price', 'wgn_currency.currency_type', DB::raw('(3959 * acos (
					  cos ( radians(' . $fromlat . ') )
					  * cos( radians( to_latitude ) )
					  * cos( radians( to_longitude ) - radians(' . $fromlng . ') )
					  + sin ( radians(' . $fromlat . ') )
					  * sin( radians( to_latitude ) )
					)
                                    ) AS distance'))
                                                    ->having('distance', '<', '5')
                                                    ->orderBy('distance', 'ASC')
                                                    ->where('wgn_item.status', '1')
                                                    ->Join('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')->simplePaginate(10);

                                    return View::make('listofitems', $this->data);
                                } else {
                                    return Redirect::route('user.item-list')->with(array('flash_alert_notice' => 'No such item found!', 'flash_action' => 'warning'))->withInput();
                                }
                            }
                        }
                        break;
                    case 'search_date':
                        $Filter_Date = $request->input('filter_Date');

                        $this->data['item'] = DB::table('wgn_item')
                                        ->leftjoin('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')
                                        ->select('wgn_item.id', 'wgn_item.item_currency', 'wgn_item.item_pieces', 'wgn_item.item_image1', 'wgn_item.item_image2', 'wgn_item.item_image3', 'wgn_item.from_city', 'wgn_item.from_postal_code', 'wgn_item.from_postal_code', 'wgn_item.from_country', 'wgn_item.to_city', 'wgn_item.to_postal_code', 'wgn_item.to_country', 'wgn_item.item_price', 'wgn_currency.currency_type')
                                        ->where('wgn_item.created_at', 'Like', $Filter_Date . '%')
                                        ->where('wgn_item.status', '=', '1')
                                        ->orderBy('id', 'Desc')->simplePaginate(10);
                        return View::make('listofitems', $this->data);
                        break;

                    case 'search_amount':
                        $Filter_Amount = $request->input('filter_amount');

                        $this->data['item'] = DB::table('wgn_item')
                                        ->leftjoin('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')
                                        ->select('wgn_item.id', 'wgn_item.item_currency', 'wgn_item.item_pieces', 'wgn_item.item_image1', 'wgn_item.item_image2', 'wgn_item.item_image3', 'wgn_item.from_city', 'wgn_item.from_postal_code', 'wgn_item.from_postal_code', 'wgn_item.from_country', 'wgn_item.to_city', 'wgn_item.to_postal_code', 'wgn_item.to_country', 'wgn_item.item_price', 'wgn_currency.currency_type')
                                        ->where('wgn_item.item_price', '=', $Filter_Amount)
                                        ->where('wgn_item.status', '=', '1')
                                        ->orderBy('id', 'Desc')->simplePaginate(10);
                        return View::make('listofitems', $this->data);
                        break;
                }
            }
        }


        $file = array('fromPostalCode' => $request->input('fromPostalCode'), 'toPostalCode' => $request->input('toPostalCode'), 'fromCity' => $request->input('fromCity'), 'toCity' => $request->input('toCity'), 'fromCountry' => $request->input('fromCountry'), 'toCountry' => $request->input('toCountry'));
        $rules = array('fromPostalCode' => 'required', 'toPostalCode' => 'required', 'fromCity' => 'required', 'toCity' => 'required', 'fromCountry' => 'required', 'toCountry' => 'required');
        $validator = Validator::make($file, $rules);
        if (isset($_GET['fromPostalCode']) || isset($_GET['toPostalCode']) || isset($_GET['fromCity']) || isset($_GET['toCity']) || isset($_GET['fromCountry']) || isset($_GET['toCountry'])) {
            if ($validator->fails()) {
                return Redirect::route('home')->with(array('flash_alert_notice' => 'All fields are maindatory!', 'flash_action' => 'warning'))->withInput();
            } else {
                $fromAddress = $_GET['fromPostalCode'] . ', ' . $_GET['fromCity'] . ', ' . $this->data['countries'][$_GET['fromCountry']];
                $toAddress = $_GET['toPostalCode'] . ', ' . $_GET['toCity'] . ', ' . $this->data['countries'][$_GET['toCountry']];

                $store_addressFrom = str_replace(' ', '+', $fromAddress);
                $store_addressTo = str_replace(' ', '+', $toAddress);

                $geocode = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . $store_addressFrom . "&sensor=false");
                $output = json_decode($geocode);

                if (isset($output->results) && !empty($output->results)) {
                    $fromlat = isset($output->results[0]) ? $output->results[0]->geometry->location->lat : '';
                    $fromlng = isset($output->results[0]) ? $output->results[0]->geometry->location->lng : '';
                } else {
                    Return Redirect::route('home')->with(array('flash_alert_notice' => 'Please enter valid data for seach!', 'flash_action' => 'danger'))->withInput();
                }
                $geocodeTo = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . $store_addressTo . "&sensor=false");
                $outputTo = json_decode($geocodeTo);
                if (isset($output->results) && !empty($output->results)) {
                    $tolat = isset($outputTo->results[0]) ? $outputTo->results[0]->geometry->location->lat : '';
                    $tolng = isset($outputTo->results[0]) ? $outputTo->results[0]->geometry->location->lng : '';
                } else {
                    return Redirect::route('home');
                }
                if ((isset($tolat) && !empty($tolat)) && (isset($tolng) && !empty($tolng)) && (isset($fromlat) && !empty($fromlat)) && (isset($fromlng) && !empty($fromlng))) {
                    $this->data['item_from'] = Items::Select('wgn_item.id', 'wgn_item.item_currency', 'wgn_item.item_pieces', 'wgn_item.item_image1', 'wgn_item.item_image2', 'wgn_item.item_image3', 'wgn_item.from_city', 'wgn_item.from_postal_code', 'wgn_item.from_postal_code', 'wgn_item.from_country', 'wgn_item.to_city', 'wgn_item.to_postal_code', 'wgn_item.to_country', 'wgn_item.item_price', 'wgn_currency.currency_type', DB::raw('(3959 * acos (
					  cos ( radians(' . $fromlat . ') )
					  * cos( radians( from_latitude ) )
					  * cos( radians( from_longitude ) - radians(' . $fromlng . ') )
					  + sin ( radians(' . $fromlat . ') )
					  * sin( radians( from_latitude ) )
					)
				) AS distance'))
                            ->leftJoin('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')
                            ->where('wgn_item.status', '1')
                            ->having('distance', '<', '6');
                    $this->data['item'] = Items::Select('wgn_item.id', 'wgn_item.item_currency', 'wgn_item.item_pieces', 'wgn_item.item_image1', 'wgn_item.item_image2', 'wgn_item.item_image3', 'wgn_item.from_city', 'wgn_item.from_postal_code', 'wgn_item.from_postal_code', 'wgn_item.from_country', 'wgn_item.to_city', 'wgn_item.to_postal_code', 'wgn_item.to_country', 'wgn_item.item_price', 'wgn_currency.currency_type', DB::raw('(3959 * acos (
                                      cos ( radians(' . $tolat . ') )
                                      * cos( radians( to_latitude ) )
                                      * cos( radians( to_longitude ) - radians(' . $tolng . ') )
                                      + sin ( radians(' . $tolat . ') )
                                      * sin( radians( to_latitude ))
                                    )) as distance'))
                                    ->union($this->data['item_from'])
                                    ->where('wgn_item.status', '1')
                                    ->having('distance', '<', '6')
                                    ->orderBy('distance', 'ASC')
                                    ->Join('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')->simplePaginate(10);

                    //print_r($this->data['item']);die;

                    return View::make('listofitems', $this->data);
                } else {
                    return Redirect::route('home')->with(array('flash_alert_notice' => 'No such item found!', 'flash_action' => 'warning'))->withInput();
                }
            }
        }
        $this->data['item'] = DB::table('wgn_item')
                        ->leftjoin('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')
                        ->select('wgn_item.id', 'wgn_item.item_currency', 'wgn_item.item_pieces', 'wgn_item.item_image1', 'wgn_item.item_image2', 'wgn_item.item_image3', 'wgn_item.from_city', 'wgn_item.from_postal_code', 'wgn_item.from_postal_code', 'wgn_item.from_country', 'wgn_item.to_city', 'wgn_item.to_postal_code', 'wgn_item.to_country', 'wgn_item.item_price', 'wgn_currency.currency_type')
                        ->where('wgn_item.status', '=', '1')
                        ->orderBy('id', 'Desc')->simplePaginate(10);
        return View::make('listofitems', $this->data);
    }

    //mape view for items
    function itemListmap(Request $request) {
        if ($_GET) {

            if (!empty($request->input('filterData'))) {
                $checkboxvalue = $request->input('checkbox');
                switch ($checkboxvalue) {
                    case 'search_location':
                        $file = array('location_postalCode' => $request->input('location_postalCode'), 'location_city' => $request->input('location_city'), 'location_country' => $request->input('location_country'));
                        $rules = array('location_postalCode' => 'required', 'location_city' => 'required', 'location_country' => 'required');
                        break;
                    case 'search_destination':
                        $file = array('destination_postalCode' => $request->input('destination_postalCode'), 'destination_city' => $request->input('destination_city'), 'destination_country' => $request->input('destination_country'));
                        $rules = array('destination_postalCode' => 'required', 'destination_city' => 'required', 'destination_country' => 'required');
                        break;
                    case 'search_date':
                        $file = array('filter_Date' => $request->input('filter_Date'));
                        $rules = array('filter_Date' => 'required');
                        break;
                    case 'search_amount':
                        $file = array('filter_amount' => $request->input('filter_amount'));
                        $rules = array('filter_amount' => 'required');
                        break;
                }
                $validator = Validator::make($file, $rules);
                if ($validator->fails()) {
                    return Redirect::route('user.item-list')->withErrors($validator)->withInput();
                } else {
                    $checkboxvalue = $request->input('checkbox');
                    switch ($checkboxvalue) {
                        case 'search_location':

                            $lcoaiton_postalCode = $request->input('location_postalCode');
                            $lcoaiton_City = $request->input('location_city');
                            $lcoaiton_Country = $request->input('location_country');

                            if (isset($lcoaiton_postalCode) || isset($lcoaiton_City) || isset($lcoaiton_Country)) {
                                if ($validator->fails()) {
                                    return Redirect::route('user.item-list')->withErrors($validator)->withInput();
                                } else {
                                    $fromAddress = $lcoaiton_postalCode . ', ' . $lcoaiton_City . ', ' . $this->data['countries'][$lcoaiton_Country];

                                    $store_addressFrom = str_replace(' ', '+', $fromAddress);

                                    $geocode = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . $store_addressFrom . "&sensor=false");
                                    $output = json_decode($geocode);

                                    if (isset($output->results) && !empty($output->results)) {
                                        $fromlat = isset($output->results[0]) ? $output->results[0]->geometry->location->lat : '';
                                        $fromlng = isset($output->results[0]) ? $output->results[0]->geometry->location->lng : '';
                                    } else {
                                        Return Redirect::route('user.item-list')->with(array('flash_alert_notice' => 'Please enter valid data for seach!', 'flash_action' => 'warning'))->withInput();
                                    }

                                    if ((isset($fromlat) && !empty($fromlat)) && (isset($fromlng) && !empty($fromlng))) {
                                        $this->data['item'] = Items::Select('wgn_item.id', 'wgn_item.item_currency', 'wgn_item.item_pieces', 'wgn_item.item_image1', 'wgn_item.item_image2', 'wgn_item.item_image3', 'wgn_item.from_city', 'wgn_item.from_postal_code', 'wgn_item.from_postal_code', 'wgn_item.from_country', 'wgn_item.to_city', 'wgn_item.to_postal_code', 'wgn_item.to_country', 'wgn_item.item_price', 'wgn_currency.currency_type', 'wgn_item.from_latitude', 'wgn_item.from_longitude', 'wgn_item.to_latitude', 'wgn_item.to_longitude', DB::raw('(3959 * acos (
					  cos ( radians(' . $fromlat . ') )
					  * cos( radians( from_latitude ) )
					  * cos( radians( from_longitude ) - radians(' . $fromlng . ') )
					  + sin ( radians(' . $fromlat . ') )
					  * sin( radians( from_latitude ) )
					)
                                    ) AS distance'))
                                                        ->having('distance', '<', '6')
                                                        ->where('wgn_item.status', '=', '1')
                                                        ->orderBy('distance', 'ASC')
                                                        ->Join('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')->simplePaginate(10);

                                        if (empty($this->data['item'])) {
                                            return Redirect::route('user.item-list')->with(array('flash_alert_notice' => 'No such item found!', 'flash_action' => 'warning'))->withInput();
                                        }

                                        return View::make('listofitemsmap', $this->data);
                                    } else {
                                        return Redirect::route('user.item-list')->with(array('flash_alert_notice' => 'No such item found!', 'flash_action' => 'warning'))->withInput();
                                    }
                                }
                            }
                            break;

                        case 'search_destination':

                            $destination_postalCode = $request->input('destination_postalCode');
                            $destination_City = $request->input('destination_city');
                            $destination_Country = $request->input('destination_country');

                            if (isset($destination_postalCode) || isset($destination_City) || isset($destination_Country)) {
                                if ($validator->fails()) {
                                    return Redirect::route('user.item-list')->withErrors($validator)->withInput();
                                } else {
                                    $fromAddress = $destination_postalCode . ', ' . $destination_City . ', ' . $this->data['countries'][$destination_Country];
                                    $store_addressFrom = str_replace(' ', '+', $fromAddress);

                                    $geocode = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . $store_addressFrom . "&sensor=false");
                                    $output = json_decode($geocode);
                                    if (isset($output->results) && !empty($output->results)) {
                                        $fromlat = isset($output->results[0]) ? $output->results[0]->geometry->location->lat : '';
                                        $fromlng = isset($output->results[0]) ? $output->results[0]->geometry->location->lng : '';
                                    } else {
                                        Return Redirect::route('user.item-list')->with(array('flash_alert_notice' => 'Please enter valid data for seach!', 'flash_action' => 'warning'))->withInput();
                                    }

                                    if ((isset($fromlat) && !empty($fromlat)) && (isset($fromlng) && !empty($fromlng))) {
                                        $this->data['item'] = Items::Select('wgn_item.id', 'wgn_item.item_currency', 'wgn_item.item_pieces', 'wgn_item.item_image1', 'wgn_item.item_image2', 'wgn_item.item_image3', 'wgn_item.from_city', 'wgn_item.from_postal_code', 'wgn_item.from_postal_code', 'wgn_item.from_country', 'wgn_item.to_city', 'wgn_item.to_postal_code', 'wgn_item.to_country', 'wgn_item.item_price', 'wgn_currency.currency_type', 'wgn_item.from_latitude', 'wgn_item.from_longitude', 'wgn_item.to_latitude', 'wgn_item.to_longitude', DB::raw('(3959 * acos (
					  cos ( radians(' . $fromlat . ') )
					  * cos( radians( to_latitude ) )
					  * cos( radians( to_longitude ) - radians(' . $fromlng . ') )
					  + sin ( radians(' . $fromlat . ') )
					  * sin( radians( to_latitude ) )
					)
                                    ) AS distance'))
                                                        ->having('distance', '<', '5')
                                                        ->where('wgn_item.status', '=', '1')
                                                        ->orderBy('distance', 'ASC')
                                                        ->Join('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')->simplePaginate(10);

                                        return View::make('listofitemsmap', $this->data);
                                    } else {
                                        return Redirect::route('user.item-list')->with(array('flash_alert_notice' => 'No such item found!', 'flash_action' => 'warning'))->withInput();
                                    }
                                }
                            }
                            break;
                        case 'search_date':
                            $Filter_Date = $request->input('filter_Date');

                            $this->data['item'] = DB::table('wgn_item')
                                            ->leftjoin('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')
                                            ->select('wgn_item.id', 'wgn_item.item_currency', 'wgn_item.item_pieces', 'wgn_item.item_image1', 'wgn_item.item_image2', 'wgn_item.item_image3', 'wgn_item.from_city', 'wgn_item.from_postal_code', 'wgn_item.from_postal_code', 'wgn_item.from_country', 'wgn_item.to_city', 'wgn_item.to_postal_code', 'wgn_item.to_country', 'wgn_item.from_latitude', 'wgn_item.from_longitude', 'wgn_item.to_latitude', 'wgn_item.to_longitude', 'wgn_item.item_price', 'wgn_currency.currency_type')
                                            ->where('wgn_item.created_at', 'Like', $Filter_Date . '%')
                                            ->where('wgn_item.status', '=', '1')
                                            ->orderBy('id', 'Desc')->simplePaginate(10);
                            return View::make('listofitemsmap', $this->data);
                            break;
                        case 'search_date':
                            $Filter_Date = $request->input('filter_Date');

                            $this->data['item'] = DB::table('wgn_item')
                                            ->leftjoin('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')
                                            ->select('wgn_item.id', 'wgn_item.item_currency', 'wgn_item.item_pieces', 'wgn_item.item_image1', 'wgn_item.item_image2', 'wgn_item.item_image3', 'wgn_item.from_city', 'wgn_item.from_postal_code', 'wgn_item.from_postal_code', 'wgn_item.from_country', 'wgn_item.to_city', 'wgn_item.to_postal_code', 'wgn_item.to_country', 'wgn_item.item_price', 'wgn_currency.currency_type')
                                            ->where('wgn_item.created_at', 'Like', $Filter_Date . '%')
                                            ->where('wgn_item.status', '=', '1')
                                            ->orderBy('id', 'Desc')->simplePaginate(10);
                            return View::make('listofitemsmap', $this->data);
                            break;

                        case 'search_amount':
                            $Filter_Amount = $request->input('filter_amount');

                            $this->data['item'] = DB::table('wgn_item')
                                            ->leftjoin('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')
                                            ->select('wgn_item.id', 'wgn_item.item_currency', 'wgn_item.item_pieces', 'wgn_item.item_image1', 'wgn_item.item_image2', 'wgn_item.item_image3', 'wgn_item.from_city', 'wgn_item.from_postal_code', 'wgn_item.from_postal_code', 'wgn_item.from_country', 'wgn_item.to_city', 'wgn_item.to_postal_code', 'wgn_item.to_country', 'wgn_item.item_price', 'wgn_currency.currency_type', 'wgn_item.from_latitude', 'wgn_item.from_longitude', 'wgn_item.to_latitude', 'wgn_item.to_longitude')
                                            ->where('wgn_item.item_price', '=', $Filter_Amount)
                                            ->where('wgn_item.status', '=', '1')
                                            ->orderBy('id', 'Desc')->simplePaginate(10);
                            return View::make('listofitemsmap', $this->data);
                            break;
                            $this->data['item'] = DB::table('wgn_item')
                                            ->leftjoin('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')
                                            ->select('wgn_item.id', 'wgn_item.item_currency', 'wgn_item.item_pieces', 'wgn_item.item_image1', 'wgn_item.item_image2', 'wgn_item.item_image3', 'wgn_item.from_city', 'wgn_item.from_postal_code', 'wgn_item.from_postal_code', 'wgn_item.from_country', 'wgn_item.to_city', 'wgn_item.to_postal_code', 'wgn_item.to_country', 'wgn_item.item_price', 'wgn_currency.currency_type')
                                            ->where('wgn_item.item_price', '=', $Filter_Amount)
                                            ->where('wgn_item.status', '=', '1')
                                            ->orderBy('id', 'Desc')->simplePaginate(10);
                            return View::make('listofitemsmap', $this->data);
                            break;
                    }
                }
            }

            $file = array('fromPostalCode' => $request->input('fromPostalCode'), 'toPostalCode' => $request->input('toPostalCode'), 'fromCity' => $request->input('fromCity'), 'toCity' => $request->input('toCity'), 'fromCountry' => $request->input('fromCountry'), 'toCountry' => $request->input('toCountry'));
            $rules = array('fromPostalCode' => 'required', 'toPostalCode' => 'required', 'fromCity' => 'required', 'toCity' => 'required', 'fromCountry' => 'required', 'toCountry' => 'required');
            $validator = Validator::make($file, $rules);
            if ($validator->fails()) {
                return Redirect::route('home')->withErrors($validator)->withInput();
            } else {
                $fromAddress = $_GET['fromPostalCode'] . ', ' . $_GET['fromCity'] . ', ' . $this->data['countries'][$_GET['fromCountry']];
                $toAddress = $_GET['toPostalCode'] . ', ' . $_GET['toCity'] . ', ' . $this->data['countries'][$_GET['toCountry']];

                $store_addressFrom = str_replace(' ', '+', $fromAddress);
                $store_addressTo = str_replace(' ', '+', $toAddress);

                $geocode = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . $store_addressFrom . "&sensor=false");
                $output = json_decode($geocode);

                $fromlat = $output->results[0]->geometry->location->lat;
                $fromlng = $output->results[0]->geometry->location->lng;

                $geocodeTo = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . $store_addressTo . "&sensor=false");
                $outputTo = json_decode($geocodeTo);

                $tolat = $outputTo->results[0]->geometry->location->lat;
                $tolng = $outputTo->results[0]->geometry->location->lng;

                if ((isset($tolat) && !empty($tolat)) && (isset($tolng) && !empty($tolng)) && (isset($fromlat) && !empty($fromlat)) && (isset($fromlng) && !empty($fromlng))) {
                    $this->data['item_from'] = Items::Select('wgn_item.id', 'wgn_item.item_currency', 'wgn_item.item_pieces', 'wgn_item.item_image1', 'wgn_item.item_image2', 'wgn_item.item_image3', 'wgn_item.from_city', 'wgn_item.from_postal_code', 'wgn_item.from_postal_code', 'wgn_item.from_country', 'wgn_item.to_city', 'wgn_item.to_postal_code', 'wgn_item.to_country', 'wgn_item.item_price', 'wgn_item.from_latitude', 'wgn_item.from_longitude', 'wgn_item.to_latitude', 'wgn_item.to_longitude', 'wgn_currency.currency_type', DB::raw('(3959 * acos (
					  cos ( radians(' . $fromlat . ') )
					  * cos( radians( from_latitude ) )
					  * cos( radians( from_longitude ) - radians(' . $fromlng . ') )
					  + sin ( radians(' . $fromlat . ') )
					  * sin( radians( from_latitude ) )
					)
				) AS distance'))
                            ->leftJoin('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')
                            ->where('wgn_item.status', '1')
                            ->having('distance', '<', '6');
                    $this->data['item'] = Items::Select('wgn_item.id', 'wgn_item.item_currency', 'wgn_item.item_pieces', 'wgn_item.item_image1', 'wgn_item.item_image2', 'wgn_item.item_image3', 'wgn_item.from_city', 'wgn_item.from_postal_code', 'wgn_item.from_postal_code', 'wgn_item.from_country', 'wgn_item.to_city', 'wgn_item.to_postal_code', 'wgn_item.to_country', 'wgn_item.item_price', 'wgn_item.from_latitude', 'wgn_item.from_longitude', 'wgn_item.to_latitude', 'wgn_item.to_longitude', 'wgn_currency.currency_type', DB::raw('(3959 * acos (
                                      cos ( radians(' . $tolat . ') )
                                      * cos( radians( to_latitude ) )
                                      * cos( radians( to_longitude ) - radians(' . $tolng . ') )
                                      + sin ( radians(' . $tolat . ') )
                                      * sin( radians( to_latitude ))
                                    )) as distance'))
                                    ->union($this->data['item_from'])
                                    ->where('wgn_item.status', '=', '1')
                                    ->having('distance', '<', '6')
                                    ->orderBy('distance', 'ASC')
                                    ->Join('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')->get();
                    return View::make('listofitemsmap', $this->data);
                } else {
                    Return Redirect::route('home')->with('flash_alert_notice', 'No such items found!')->withInput();
                }
            }
        } else {
            $this->data['item'] = Items::Select('wgn_item.id', 'wgn_item.item_currency', 'wgn_item.item_pieces', 'wgn_item.item_image1', 'wgn_item.item_image2', 'wgn_item.item_image3', 'wgn_item.from_city', 'wgn_item.from_postal_code', 'wgn_item.from_postal_code', 'wgn_item.from_country', 'wgn_item.to_city', 'wgn_item.to_postal_code', 'wgn_item.to_country', 'wgn_item.item_price', 'wgn_item.from_latitude', 'wgn_item.from_longitude', 'wgn_item.to_latitude', 'wgn_item.to_longitude', 'wgn_currency.currency_type')
                            ->Join('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')->get();
            return View::make('listofitemsmap', $this->data);
        }
    }

    //Item itemView
    public function itemView($itemID) {
        $this->data['itemdetails'] = DB::table('wgn_item')
                ->leftjoin('wgn_item_category', 'wgn_item.category_id', '=', 'wgn_item_category.id')
                ->leftjoin('wgn_item_subcategory', 'wgn_item.subcategory_id', '=', 'wgn_item_subcategory.id')
                ->leftjoin('wgn_item_subcategory_two', 'wgn_item.second_subcat_id', '=', 'wgn_item_subcategory_two.id')
                ->leftjoin('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')
                ->leftjoin('wgn_itemvalue', 'wgn_item.item_value', '=', 'wgn_itemvalue.id')
                ->leftjoin('wgn_cartype', 'wgn_item.car_type', '=', 'wgn_cartype.id')
                ->select('wgn_item.*', 'wgn_item_category.category_name', 'wgn_item_subcategory.subcat_name', 'wgn_item_subcategory_two.sub_cat_name', 'wgn_currency.currency_type', 'wgn_itemvalue.value as item_value', 'wgn_cartype.type as cartype')
                ->where('wgn_item.id', '=', $itemID)
                ->first();
        $this->data['driverNewPrice'] = array();
        if (Auth::Check())
            $this->data['driverNewPrice'] = DB::table('wgn_make_another_itemoffer')->Where('driverid', Auth::User()->id)->Where('itemid', $itemID)->first();
        return View::make('itemDetail', $this->data);
    }

    public function dashboard() {
        if (Auth::check()) {
            $result = DB::select('select id, ads_images, ads_link from wgn_advertise WHERE id =1');
            $this->data['link_data'] = $result;
            return View::make('home', $this->data);
        } else {
            return Redirect::route('home');
        }
    }

    public function fileUpload($fileInput, $path, $fileName = "") {
        $destinationPath = $path; // upload path
        $extension = $fileInput->getClientOriginalExtension(); // getting image extension
        $originalName = $fileInput->getClientOriginalName();
        $originalName = explode(".", $originalName);
        if (empty($fileName))
            $fileName = $originalName[0] . "-" . rand('1', '99') . date('YmdHis') . '.' . $extension; // renameing image
        $fileInput->move($destinationPath, $fileName); // uploading file to given path
        return $fileName;
    }

    public function logout() {
        Auth::logout();
        return Redirect::route('home');
    }

    public function forgotPassword() {
        if (!Auth::check())
            return View::make('forgot');
    }

    public function sentVerificationCode(Request $request) {
        if (!Auth::check()) {
            $file = array('email' => $request->input('email'));
            $rules = array('email' => 'required|email');
            $validator = Validator::make($file, $rules);
            if ($validator->fails()) {
                return Redirect::route('user.forgotpassword')->withErrors($validator)->withInput();
            } else {
                $userdetial = User::Where('email', $request->input('email'))->first();
                $verification_code = $this->generateRandomString(6);
                $userdetial->verification_code = $verification_code;
                $userdetial->save();
                $name = $userdetial->firstname . " " . $userdetial->lastname;
                $to = $userdetial->email;
                $sub = "Verification Code - whatsgonow";
                $template = "email.forgotPassword";
                $replaces = array("name" => $name, 'verification_code' => $verification_code);
                $this->sendMail($to, $name, $sub, $template, $replaces);
                return Redirect::route('user.verify_code')->with(array('flash_alert_notice' => 'We have sent verification code on your registered mail id, please verifiy !', 'flash_action' => 'warning'))->withInput();
            }
        }
    }

    public function verificationCode() {
        if (!Auth::check())
            return View::make('verifycode');
    }

    public function checkVerifyCode(Request $request) {
        if (!Auth::check()) {
            $file = array('verification_code' => $request->input('verification_code'));
            $rules = array('verification_code' => 'required');
            $validator = Validator::make($file, $rules);
            if ($validator->fails()) {
                return Redirect::route('user.verify_code')->withErrors($validator)->withInput();
            } else {
                $this->data['verification_code'] = $request->input('verification_code');
                $this->data['userinfo'] = User::Where(array('verification_code' => $request->input('verification_code')))->first();
                if (!empty($this->data['userinfo'])) {
                    return View::make('changepass', $this->data);
                } else {
                    return Redirect::route('user.verify_code')->with(array('flash_alert_notice' => 'Code verification failed!', 'flash_action' => 'danger'))->withInput();
                }
            }
        }
    }

    public function changePassword() {
        if (Auth::check())
            return View::make('changepass', $this->data);
    }

    public function editPassword(Request $request) {
        if (!Auth::check()) {
            if (!empty($request->input('userid')) && !empty($request->input('password'))) {
                $userObj = User::Where(array('id' => $request->input('userid')))->first();
                if (!empty($userObj)) {
                    $userObj->password = bcrypt($request->input('password'));
                    $userObj->verification_code = "";
                    $userObj->save();
                    return Redirect::route('user.signin')->with(array('flash_alert_notice' => 'Your password has been changed successfully!', 'flash_action' => 'success'))->withInput();
                } else {
                    return Redirect::route('user.signin')->with(array('flash_alert_notice' => 'Code verification failed!', 'flash_action' => 'danger'))->withInput();
                }
            } else {
                return Redirect::route('user.signin')->with(array('flash_alert_notice' => 'Code verification failed!', 'flash_action' => 'danger'))->withInput();
            }
        }
    }

    public function sendMail($to, $to_name, $subject, $template, $keys = null, $addbcc = NULL) {
        Mail::send($template, $keys, function($message) use($to, $to_name, $subject) {
            $message->to($to, $to_name)->subject($subject);
        });
        return TRUE;
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@-*%#!&';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function Disclaimer() {
        return View::make('disclaimer', $this->data);
    }

    function TermsAndCondtions() {
        return View::make('terms-and-conditions', $this->data);
    }

    function Contact() {
        return View::make('contact', $this->data);
    }

    function AddContact(ContactRequest $request) {
        if ($_POST) {
            $contactData = new Contact(array(
                'name' => $request->input('cname'),
                'email' => $request->input('cemail'),
                'phone' => $request->input('cphone'),
                'comment' => $request->input('comment')
            ));
            $succ = $contactData->save();
            if ($succ) {
                return Redirect::to(route('user.contact'))->with(array('flash_alert_notice' => 'Contact saved!', 'flash_action' => 'success'));
            } else {
                return Redirect::to(route('user.contact'))->with(array('flash_alert_notice' => 'Somethig went wrong!', 'flash_action' => 'danger'));
            }
        } 
    }

    function confirmReceived($request_id) {
        $requestInfo = RequestItem::find(base64_decode($request_id));
        if (!empty($requestInfo)) {
            return Redirect::route('home')->with(array('popup' => 'item_received', 'request_id' => $request_id));
        } else {
            return Redirect::route('home')->with(array('popup' => 'error-msg'));
        }
    }

    function confirmDelivered($request_id) {
        $requestInfo = RequestItem::find(base64_decode($request_id));
        if (!empty($requestInfo)) {
            return Redirect::route('home')->with(array('popup' => 'item_delivered', 'request_id' => $request_id));
        } else {
            return Redirect::route('home')->with(array('popup' => 'error-msg'));
        }
    }

    function confirmPostReceived(Request $request) {
        $request_id = base64_decode($request->input('request_id'));
        $requestInfo = RequestItem::find($request_id);
        if (!empty($requestInfo)) {
            $itemObj = Items::find($requestInfo->item_id);
            $itemObj->status = "2";
            $itemObj->save();
            DB::table('wgn_notification')->insert(['sender_id' => $requestInfo->driver_id, 'item_id' => $requestInfo->item_id, 'receiver_id' => $requestInfo->buyer_id, 'status' => '1', 'notification_type' => 'driver-receive-item-from-seller', 'created_at' => date('Y-m-d H:i:s'), 'request_id' => $request_id]);
            return Redirect::route('home')->with(array('flash_alert_notice' => 'Item receiving entry has been successfully completed!', 'flash_action' => 'success'));
        }
    }

    function confirmPostDelivered(Request $request) {
        $request_id = base64_decode($request->input('request_id'));
        $requestInfo = RequestItem::find($request_id);
        if (!empty($requestInfo)) {
            $itemObj = Items::find($requestInfo->item_id);
            $itemObj->status = "3";
            $itemObj->save();
            $requestInfo->status = "complete";
            $requestInfo->save();
            DB::table('wgn_notification')->insert(['sender_id' => '0', 'item_id' => $requestInfo->item_id, 'receiver_id' => $requestInfo->driver_id, 'status' => '1', 'notification_type' => 'driver-payment-info-item', 'created_at' => date('Y-m-d H:i:s'), 'request_id' => $request_id]);
            return Redirect::route('home')->with(array('flash_alert_notice' => 'Item delivery by driver entry has been successfully completed!', 'flash_action' => 'success'));
        }
    }

    function getCountryDilingCodeByCountryID(Request $request, $id) {
        if ($request->ajax()) {
            $countryDetail = Countries::Select('*')->find($id);
            $status = false;
            $data = "";
            if (!empty($countryDetail)) {
                $status = true;
                $data = $countryDetail;
            }
            echo json_encode(array(
                'status' => $status,
                'data' => $data
            ));
        }
    }

    function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if (isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
}
