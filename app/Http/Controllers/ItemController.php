<?php

namespace App\Http\Controllers;

use App\Http\Controllers\MainController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ItemRequest;
use App\Items;
use App\UserDetail;
use App\RequestItem;
use Auth;
use View;
use Input;
use DB;
use Validator;

class ItemController extends MainController {

    public function __construct() {
        parent::__construct();         
        if(Auth::check() && Auth::user()->role == '2'){            
            $userDetailObj = UserDetail::Where('userid', Auth::user()->id)->first();
            if (empty($userDetailObj)) {
                return Redirect::route('user.driver_registration')->send();
            }            
        }
    }

    public function index() {
        
    }

    public function create() {
        $this->data['item_category'] = DB::table('wgn_item_category')->Where('status', '1')->orderBy('order', 'ASC')->get();
        $this->data['currency'] = DB::table('wgn_currency')->Where('status', '1')->get();
        $this->data['countries'] = DB::table('countries')->Where('status', '1')->get();
        $this->data['itemvalue'] = DB::table('wgn_itemvalue')->Where('status', '1')->get();
        $this->data['cartype'] = DB::table('wgn_cartype')->Where('status', '1')->orderBy('order', 'ASC')->get();
        return View::make('item', $this->data);
    }

    public function store(ItemRequest $request) {

        $itemObj = new Items();
        $itemObj->userid = Auth::User()->id;
        if (!empty(Input::file('item_image1')) && Input::file('item_image1')->getError() == 0) {
            $path = base_path() . '/public/uploads/items';
            $fileName = $this->fileUpload(Input::file('item_image1'), $path, "");
            $itemObj->item_image1 = $fileName;
        }
        if (!empty(Input::file('item_image2')) && Input::file('item_image2')->getError() == 0) {
            $path = base_path() . '/public/uploads/items';
            $fileName = $this->fileUpload(Input::file('item_image2'), $path, "");
            $itemObj->item_image2 = $fileName;
        }
        if (!empty(Input::file('item_image3')) && Input::file('item_image3')->getError() == 0) {
            $path = base_path() . '/public/uploads/items';
            $fileName = $this->fileUpload(Input::file('item_image3'), $path, "");
            $itemObj->item_image3 = $fileName;
        }
        if (!empty(Input::file('item_image4')) && Input::file('item_image4')->getError() == 0) {
            $path = base_path() . '/public/uploads/items';
            $fileName = $this->fileUpload(Input::file('item_image4'), $path, "");
            $itemObj->item_image4 = $fileName;
        }
        $itemObj->measures = $request->input('measures');
        $itemObj->item_length = $request->input('item_length');
        $itemObj->measures = $request->input('measures');
        $itemObj->item_height = $request->input('item_height');
        $itemObj->item_width = $request->input('item_width');
        $itemObj->item_weight = $request->input('item_weight');
        $itemObj->item_pieces = $request->input('item_pieces');
        $itemObj->category_id = $request->input('category_id');
        $itemObj->subcategory_id = $request->input('subcategory_id');
        $itemObj->second_subcat_id = $request->input('second_subcat_id');
        $itemObj->item_description = $request->input('item_description');
        $itemObj->item_value = $request->input('item_value');
        $itemObj->currency_type = $request->input('currency_type');
        $itemObj->from_postal_code = $request->input('from_postal_code');
        $itemObj->from_city = $request->input('from_city');
        $itemObj->from_country = $request->input('from_country');
        $itemObj->to_postal_code = $request->input('to_postal_code');
        $itemObj->to_city = $request->input('to_city');
        $itemObj->to_country = $request->input('to_country');
        $itemObj->item_currency = $request->input('item_currency');
        $itemObj->offer_type = $request->input('offer_type');
        $itemObj->car_type = $request->input('car_type');
        $itemObj->car_model = $request->input('car_model');
        $itemObj->item_price = $request->input('item_price');
        $itemObj->product_name = $request->input('product_name');
        $itemObj->seller_email = $request->input('seller_email');
        $itemObj->seller_phone_number = $request->input('seller_phone_number');
        $itemObj->from_street = $request->input('from_street');
        $itemObj->to_street = $request->input('to_street');
        $itemObj->from_street_number = $request->input('from_street_number');
        $itemObj->to_street_number = $request->input('to_street_number');                

        $fromAddress = $request->input('from_postal_code') . ', ' . $request->input('from_city') . ', ' . $this->data['countries'][$request->input('from_country')];
        $toAddress = $request->input('to_postal_code') . ', ' . $request->input('to_city') . ', ' . $this->data['countries'][$request->input('to_country')];

        $store_addressFrom = str_replace(' ', '+', $fromAddress);
        $store_addressTo = str_replace(' ', '+', $toAddress);

        $from_address = $this->getLatLong($store_addressFrom);
        $to_address = $this->getLatLong($store_addressTo);
        if (!empty($to_address)) {
            $itemObj->to_latitude = $to_address['latitude'];
            $itemObj->to_longitude = $to_address['longitude'];
        }
        if (!empty($from_address)) {
            $itemObj->from_latitude = $from_address['latitude'];
            $itemObj->from_longitude = $from_address['longitude'];
        }        
        if ($itemObj->save()) {
            $flash = array('flash_alert_notice' => 'Item added successfully!', 'flash_action' => 'success');
            if (Auth::User()->role == '1') {
                return Redirect::route('user.profile')->with($flash)->withInput();
            } else {
                return Redirect::route('user.driver_profile')->with($flash)->withInput();
            }
        } else {
            $flash = array('flash_alert_notice' => 'Item not saved successfully!', 'flash_action' => 'danger');
            return Redirect::route('item.create')->with($flash)->withInput();
        }
    }
    
    //Edit delete Get Method
    public function edit($id){        
        $this->data['items'] = Items::find($id);
        if($this->data['items']['userid'] == $this->data['userinfo']->id){
            $this->data['item_category'] = DB::table('wgn_item_category')->Where('status', '1')->orderBy('order', 'ASC')->get();
            $this->data['currency'] = DB::table('wgn_currency')->Where('status', '1')->get();
            $this->data['countries'] = DB::table('countries')->Where('status', '1')->get();
            $this->data['itemvalue'] = DB::table('wgn_itemvalue')->Where('status', '1')->get();
            $this->data['cartype'] = DB::table('wgn_cartype')->Where('status', '1')->orderBy('order', 'ASC')->get();
            return View::make('item', $this->data);
        }else{
            $flash = array('flash_alert_notice' => 'Sorry, This item not found in your item list!', 'flash_action' => 'danger');
            if (Auth::User()->role == '1') {                
                return Redirect::route('user.profile')->with($flash)->withInput();
            } else {                
                return Redirect::route('user.driver_profile')->with($flash)->withInput();
            }            
        }                
    }
    
    // Edit delete Post Method
    public function update(ItemRequest $request){        
        $itemObj = Items::find($request->input('item_id'));
        if($itemObj['userid'] == $this->data['userinfo']->id){
            $itemObj->userid = Auth::User()->id;
            if (!empty(Input::file('item_image1')) && Input::file('item_image1')->getError() == 0) {
                $path = base_path() . '/public/uploads/items';
                $fileName = $this->fileUpload(Input::file('item_image1'), $path, $itemObj['item_image1']);
                $itemObj->item_image1 = $fileName;
            }
            if (!empty(Input::file('item_image2')) && Input::file('item_image2')->getError() == 0) {
                $path = base_path() . '/public/uploads/items';
                $fileName = $this->fileUpload(Input::file('item_image2'), $path, $itemObj['item_image2']);
                $itemObj->item_image2 = $fileName;
            }
            if (!empty(Input::file('item_image3')) && Input::file('item_image3')->getError() == 0) {
                $path = base_path() . '/public/uploads/items';
                $fileName = $this->fileUpload(Input::file('item_image3'), $path, $itemObj['item_image3']);
                $itemObj->item_image3 = $fileName;
            }
            if (!empty(Input::file('item_image4')) && Input::file('item_image4')->getError() == 0) {
                $path = base_path() . '/public/uploads/items';
                $fileName = $this->fileUpload(Input::file('item_image4'), $path, $itemObj['item_image4']);
                $itemObj->item_image4 = $fileName;
            }
            $itemObj->measures = $request->input('measures');
            $itemObj->item_length = $request->input('item_length');
            $itemObj->measures = $request->input('measures');
            $itemObj->item_height = $request->input('item_height');
            $itemObj->item_width = $request->input('item_width');
            $itemObj->item_weight = $request->input('item_weight');
            $itemObj->item_pieces = $request->input('item_pieces');
            $itemObj->category_id = $request->input('category_id');
            $itemObj->subcategory_id = $request->input('subcategory_id');
            $itemObj->second_subcat_id = $request->input('second_subcat_id');
            $itemObj->item_description = $request->input('item_description');
            $itemObj->item_value = $request->input('item_value');
            $itemObj->currency_type = $request->input('currency_type');
            $itemObj->from_postal_code = $request->input('from_postal_code');
            $itemObj->from_city = $request->input('from_city');
            $itemObj->from_country = $request->input('from_country');
            $itemObj->to_postal_code = $request->input('to_postal_code');
            $itemObj->to_city = $request->input('to_city');
            $itemObj->to_country = $request->input('to_country');
            $itemObj->item_currency = $request->input('item_currency');
            $itemObj->offer_type = $request->input('offer_type');
            $itemObj->car_type = $request->input('car_type');
            $itemObj->car_model = $request->input('car_model');
            $itemObj->item_price = $request->input('item_price');
            $itemObj->product_name = $request->input('product_name');
            $itemObj->seller_email = $request->input('seller_email');
            $itemObj->seller_phone_number = $request->input('seller_phone_number');
            $itemObj->from_street = $request->input('from_street');
            $itemObj->to_street = $request->input('to_street');
            $itemObj->from_street_number = $request->input('from_street_number');
            $itemObj->to_street_number = $request->input('to_street_number');


            $fromAddress = $request->input('from_postal_code') . ', ' . $request->input('from_city') . ', ' . $this->data['countries'][$request->input('from_country')];
            $toAddress = $request->input('to_postal_code') . ', ' . $request->input('to_city') . ', ' . $this->data['countries'][$request->input('to_country')];

            $store_addressFrom = str_replace(' ', '+', $fromAddress);
            $store_addressTo = str_replace(' ', '+', $toAddress);

            $from_address = $this->getLatLong($store_addressFrom);
            $to_address = $this->getLatLong($store_addressTo);
            if (!empty($to_address)) {
                $itemObj->to_latitude = $to_address['latitude'];
                $itemObj->to_longitude = $to_address['longitude'];
            }
            if (!empty($from_address)) {
                $itemObj->from_latitude = $from_address['latitude'];
                $itemObj->from_longitude = $from_address['longitude'];
            }        
            if ($itemObj->save()) {
                $flash = array('flash_alert_notice' => 'Item updated successfully!', 'flash_action' => 'success');
                if (Auth::User()->role == '1') {
                    return Redirect::route('user.profile')->with($flash)->withInput();
                } else {
                    return Redirect::route('user.driver_profile')->with($flash)->withInput();
                }
            } else {
                $flash = array('flash_alert_notice' => 'Item updation failed!', 'flash_action' => 'danger');
                return Redirect::route('item.edit', $request->input('item_id'))->with($flash)->withInput();
            }
        }else{
            $flash = array('flash_alert_notice' => 'Sorry, This item not found in your item list!', 'flash_action' => 'danger');
            return Redirect::route('item.edit', $request->input('item_id'))->with($flash)->withInput();
        }    
    }

    

    public function viewNotification($id) {        
        $id = base64_decode($id);
        $notification_info = DB::table('wgn_notification')->find($id);
        if(!empty($notification_info)){
            if($notification_info->receiver_id == $this->data['userinfo']->id){
                // Code for read notification
                DB::table('wgn_notification')->where('id', $id)->update(['status'=>'0']);                
                return Redirect::route('show_notification', $id);
            }else{
                $flash = array('flash_alert_notice' => 'Sorry, This notification not available!', 'flash_action' => 'danger');
                return Redirect::route('home')->with($flash)->withInput();
            }
        }else{
            $flash = array('flash_alert_notice' => 'Sorry, This notification not available!', 'flash_action' => 'danger');
            return Redirect::route('home')->with($flash)->withInput();
        }    
    }
    
    public function showNotification($id){
        $notification_info = $this->data['notification_info'] = DB::table('wgn_notification')->find($id);        
        if(!empty($notification_info)){
            if($notification_info->receiver_id == $this->data['userinfo']->id){
                if(!empty($notification_info->request_id)){
                    $this->data['requestinfo'] = DB::table('wgn_request')->find($notification_info->request_id);
                }
                $this->data['items'] = Items::Select('wgn_item.*', 'users.firstname', 'users.lastname', 'users.email', 'wgn_currency.currency_type')
                    ->LeftJoin('users', 'wgn_item.userid', '=', 'users.id')
                    ->LeftJoin('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')
                    ->find($notification_info->item_id);

                $this->data['userdetails'] = DB::table('users')
                    ->Select('users.*', 'wgn_usersdetail.*', 'wgn_cartype.type as cartype_title', 'wgn_carmodel.model as carmodel_title')
                    ->Leftjoin('wgn_usersdetail', 'users.id', '=', 'wgn_usersdetail.userid')
                    ->Leftjoin('wgn_cartype', 'wgn_usersdetail.cartype', '=', 'wgn_cartype.id')
                    ->Leftjoin('wgn_carmodel', 'wgn_usersdetail.carmodel', '=', 'wgn_carmodel.id')
                    ->Where('users.id', $notification_info->sender_id)->first();

                if(!empty($this->data['requestinfo']->trip_id)){
                    $this->data['trips'] = DB::table("wgn_trip")->find($this->data['requestinfo']->trip_id);
                }

                $this->data['another_offer'] = array();

                if (!empty($this->data['items'])) {
                    if($this->data['items']->offer_type == 2){
                        $this->data['another_offer'] = DB::table("wgn_make_another_itemoffer")
                        ->Select('price as new_price')
                        ->Where(array('driverid' => $notification_info->sender_id, 'status' => '1'))
                        ->first();
                    }    
                    Return View::make('notification.' . $notification_info->notification_type, $this->data);
                }else{
                    Return View::make('notification.item-not-found', $this->data);
                }
            }else{
                $flash = array('flash_alert_notice' => 'Sorry, This notification not available!', 'flash_action' => 'danger');
                return Redirect::route('home')->with($flash)->withInput();
            }
        }else{
            $flash = array('flash_alert_notice' => 'Sorry, This notification not available!', 'flash_action' => 'danger');
            return Redirect::route('home')->with($flash)->withInput();
        }
    }

    public function makeAnotherItemOffer(Request $request) {
        $file = array('new_price' => $request->input('new_price'));
        $rules = array('new_price' => 'required');
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
            return Redirect::route('user.item-view', $request->input('itemid'))->withErrors($validator)->withInput();
        } else {
            $items = Items::Select('wgn_item.*', 'users.firstname', 'users.lastname', 'users.email', 'wgn_currency.currency_type')
                ->LeftJoin('users', 'wgn_item.userid', '=', 'users.id')
                ->LeftJoin('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')
                ->find($request->input('itemid'));
            $userdetails = DB::table('users')
                ->Select('users.*', 'wgn_usersdetail.*', 'wgn_cartype.type as cartype_title', 'wgn_carmodel.model as carmodel_title')
                ->Leftjoin('wgn_usersdetail', 'users.id', '=', 'wgn_usersdetail.userid')
                ->Leftjoin('wgn_cartype', 'wgn_usersdetail.cartype', '=', 'wgn_cartype.id')
                ->Leftjoin('wgn_carmodel', 'wgn_usersdetail.carmodel', '=', 'wgn_carmodel.id')
                ->Where('users.id', $this->data['userinfo']->id)->first();
            if ($request->input('new_price') < $items->item_price) {
                return Redirect::route('user.item-view', $request->input('itemid'))->with(array('flash_alert_notice' => 'New price can not be lessthan old price!', 'flash_action' => 'warning'))->withInput();
            } else {
                $item_notification = DB::table('wgn_notification')->Where(['item_id' => $request->input('itemid'), 'sender_id' => $this->data['userinfo']->id, 'receiver_id' => $items->userid])->first();
                if (empty($item_notification)) {
                    // Add Driver offer for item of buyer
                    DB::table('wgn_make_another_itemoffer')
                            ->where(array('driverid' => $request->input('driver_id'), 'itemid' => $request->input('itemid')))
                            ->update(
                                    ['status' => '0', 'updated_at' => date('Y-m-d H:i:s')]
                    );
                    $make_another_id = DB::table('wgn_make_another_itemoffer')->insertGetId(
                            ['driverid' => $request->input('driver_id'), 'itemid' => $request->input('itemid'), 'price' => $request->input('new_price'), 'created_at' => date('Y-m-d H:i:s')]
                    );
                    // Deny all previous request by driver for same items of buyer
                    DB::table('wgn_request')
                            ->where(array('buyer_id' => $items->userid, 'driver_id' => $request->input('driver_id')))
                            ->update(['status' => 'deny', 'updated_at' => date('Y-m-d H:i:s')]);
                    // Add one more request by driver for same items of buyer

                    $objItemRequest = new RequestItem();
                    $objItemRequest->request_by = 'driver';
                    $objItemRequest->buyer_id = $items->userid;
                    $objItemRequest->driver_id = $request->input('driver_id');
                    $objItemRequest->item_id = $request->input('itemid');
                    $objItemRequest->created_at = date('Y-m-d H:i:s');
                    $objItemRequest->trip_id = $request->input('trip_id');
                    $objItemRequest->save();

                    // Add one new notification
                    DB::table('wgn_notification')->insert(['sender_id' => $this->data['userinfo']->id, 'item_id' => $request->input('itemid'), 'receiver_id' => $items->userid, 'status' => '1', 'notification_type' => 'driver-another-offer-request', 'created_at' => date('Y-m-d H:i:s'), 'request_id' => $objItemRequest->id, 'another_offer_id' => $make_another_id]);
                    $flash = array('flash_alert_notice' => 'New price added successfully!', 'flash_action' => 'success');

                    //Mail
                    $to = $items->email;
                    $template = "email.anotherOffer";
                    $name = $items->firstname . " " . $items->lastname;
                    $sub = "Driver accept your request for item with another offer - whatsgonow";
                    $mail_message = "has send you a user bid {$this->data['currency'][$items->currency_type]} {$request->input('new_price')}";
                    $userimage = asset('public/images/drvimg.png');
                    if (!empty($userdetails->userimage)) {
                        $userimage = route('home') . "/public/uploads/users/" . $userdetails->userimage;
                    }
                    $replaces = array(
                        'name' => $name,
                        'driver_name' => $this->data['userinfo']->firstname . " " . $this->data['userinfo']->lastname,
                        'driver_city' => $this->data['userinfo']->city,
                        'vehicle_type' => !empty($userdetails->cartype_title) ? $userdetails->cartype_title : "",
                        'vehicle_model' => !empty($userdetails->carmodel_title) ? $userdetails->carmodel_title : "",
                        'mail_message' => $mail_message,
                        'currency' => $this->data['currency'][$items->currency_type],
                        'new_offer_cost' => $request->input('new_price'),
                        'userimage' => $userimage
                    );
                    $this->sendMail($to, $name, $sub, $template, $replaces);
                    return Redirect::route('user.item-view', $request->input('itemid'))->with($flash)->withInput();
                } else {
                    return Redirect::route('user.item-view', $request->input('itemid'))->with(array('flash_alert_notice' => 'Your Request already sent for this item!', 'flash_action' => 'warning'))->withInput();
                }
            }
        }
    }

    public function itemRequestAcceptByDriver(Request $request) {
        $file = array('itemid' => $request->input('itemid'), 'trip_id' => $request->input('trip_id'));
        $rules = array('itemid' => 'required', 'trip_id' => 'required');
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
            return Redirect::route('user.item-view', $request->input('itemid'))->with(array('popup' => false))->withErrors($validator)->withInput();
        } else {
            $items = Items::Select('wgn_item.*', 'users.firstname', 'users.lastname', 'users.email', 'wgn_currency.currency_type')
                    ->LeftJoin('users', 'wgn_item.userid', '=', 'users.id')
                    ->LeftJoin('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')
                    ->find($request->input('itemid'));

            $userdetails = DB::table('users')
                            ->Select('users.*', 'wgn_usersdetail.*', 'wgn_cartype.type as cartype_title', 'wgn_carmodel.model as carmodel_title')
                            ->Leftjoin('wgn_usersdetail', 'users.id', '=', 'wgn_usersdetail.userid')
                            ->Leftjoin('wgn_cartype', 'wgn_usersdetail.cartype', '=', 'wgn_cartype.id')
                            ->Leftjoin('wgn_carmodel', 'wgn_usersdetail.carmodel', '=', 'wgn_carmodel.id')
                            ->Where('users.id', $this->data['userinfo']->id)->first();

            $item_notification = DB::table('wgn_notification')->Where(['item_id' => $request->input('itemid'), 'sender_id' => $this->data['userinfo']->id, 'receiver_id' => $request->input('userid')])->first();
            $mail_message = "has send you a user bid {$this->data['currency'][$items->currency_type]} {$items->item_price}";
            if (empty($item_notification)) {
                $objItemRequest = RequestItem::Where(array('buyer_id' => $items->userid, 'driver_id' => $this->data['userinfo']->id, 'request_by' => 'buyer', 'status' => Null, 'item_id' => $items->id))->first();
                if (empty($objItemRequest)) {
                    $objItemRequest = new RequestItem();
                    $objItemRequest->request_by = 'driver';
                    $objItemRequest->buyer_id = $items->userid;
                    $objItemRequest->driver_id = $this->data['userinfo']->id;
                    $objItemRequest->item_id = $request->input('itemid');
                    $objItemRequest->created_at = date('Y-m-d H:i:s');
                    $objItemRequest->trip_id = $request->input('trip_id');
                } else {
                    $objItemRequest->status = 'accept';
                    $objItemRequest->trip_id = $request->input('trip_id');
                }
                $objItemRequest->save();
                DB::table('wgn_notification')->insert(['sender_id' => $this->data['userinfo']->id, 'item_id' => $request->input('itemid'), 'receiver_id' => $items->userid, 'status' => '1', 'notification_type' => 'driver-accept-request', 'created_at' => date('Y-m-d H:i:s'), 'request_id' => $objItemRequest->id]);
                //Mail
                $to = $items->email;
                $name = $items->firstname . " " . $items->lastname;
                $sub = "Driver accept your request for item - whatsgonow";
                $template = "email.anotherOffer";
                $mail_message = "has accept your offer {$this->data['currency'][$items->currency_type]} {$items->item_price}";
                $userimage = asset('public/images/drvimg.png');
                if (!empty($userdetails->userimage)) {
                    $userimage = route('home') . "/public/uploads/users/" . $userdetails->userimage;
                }
                $replaces = array(
                    'name' => $name,
                    'driver_name' => $this->data['userinfo']->firstname . " " . $this->data['userinfo']->lastname,
                    'driver_city' => $this->data['userinfo']->city,
                    'vehicle_type' => !empty($userdetails->cartype_title) ? $userdetails->cartype_title : "",
                    'vehicle_model' => !empty($userdetails->carmodel_title) ? $userdetails->carmodel_title : "",
                    'mail_message' => $mail_message,
                    'userimage' => $userimage
                );
                $this->sendMail($to, $name, $sub, $template, $replaces);
                return Redirect::route('user.item-view', $request->input('itemid'))->with(array('flash_alert_notice' => 'Request has been sent successfully!', 'flash_action' => 'success', 'popup' => true))->withInput();
            } else {
                return Redirect::route('user.item-view', $request->input('itemid'))->with(array('flash_alert_notice' => 'Your Request already sent for this item!', 'flash_action' => 'warning', 'popup' => true))->withInput();
            }
        }
    }

    public function denyDriverRequest($id) {
        $request_id = base64_decode($id);
        $objRequestItem = RequestItem::find($request_id);
        if (!empty($objRequestItem)) {
            if ($objRequestItem->status != 'deny') {
                $this->data['items'] = Items::Select('wgn_item.*', 'users.firstname', 'users.lastname', 'users.email', 'wgn_currency.currency_type', 'wgn_make_another_itemoffer.price as new_offer')
                        ->LeftJoin('users', 'wgn_item.userid', '=', 'users.id')
                        ->LeftJoin('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')
                        ->LeftJoin('wgn_make_another_itemoffer', 'wgn_item.id', '=', 'wgn_make_another_itemoffer.itemid')
                        ->find($objRequestItem->item_id);

                $this->data['userdetails'] = DB::table('users')
                                ->Select('users.*', 'wgn_usersdetail.*', 'wgn_cartype.type as cartype_title', 'wgn_carmodel.model as carmodel_title')
                                ->Leftjoin('wgn_usersdetail', 'users.id', '=', 'wgn_usersdetail.userid')
                                ->Leftjoin('wgn_cartype', 'wgn_usersdetail.cartype', '=', 'wgn_cartype.id')
                                ->Leftjoin('wgn_carmodel', 'wgn_usersdetail.carmodel', '=', 'wgn_carmodel.id')
                                ->Where('users.id', $objRequestItem->driver_id)->first();

                $objRequestItem->status = "deny";
                $objRequestItem->updated_at = date('Y-m-d H:i:s');
                if ($objRequestItem->save()) {
                    //Notification                                
                    DB::table('wgn_notification')->insert(['sender_id' => $this->data['items']->userid, 'item_id' => $this->data['items']->id, 'receiver_id' => $objRequestItem->driver_id, 'status' => '1', 'notification_type' => 'buyer-deny-driver-offer', 'created_at' => date('Y-m-d H:i:s'), 'request_id' => $objRequestItem->id]);
                    //Mail
                    $to = $this->data['userdetails']->email;
                    $name = $this->data['userdetails']->firstname . " " . $this->data['userdetails']->lastname;
                    $sub = "Buyer deny your request for item - whatsgonow";
                    $template = "email.denyDriverRequestByBuyer";
                    $replaces = array(
                        'buyer_name' => $this->data['items']->firstname . " " . $this->data['items']->lastname,
                        'item_name' => $this->data['items']->product_name,
                    );
                    $this->sendMail($to, $name, $sub, $template, $replaces);
                    Return Redirect::route('user.item-view', $objRequestItem->item_id)->with(array('flash_alert_notice' => 'Driver request deny successfully!', 'flash_action' => 'success'))->withInput();
                } else {
                    Return Redirect::route('user.item-list')->with(array('flash_alert_notice' => 'System Error!', 'flash_action' => 'warning'))->withInput();
                }
            } else {
                Return Redirect::route('user.item-view', $objRequestItem->item_id)->with(array('flash_alert_notice' => 'You already deny this request!', 'flash_action' => 'warning', 'popup' => true))->withInput();
            }
        } else {
            Return Redirect::route('user.item-list')->with(array('flash_alert_notice' => 'Item id not exist in our system!', 'flash_action' => 'warning'))->withInput();
        }
    }

    public function acceptDriverRequest($id) {
        $request_id = base64_decode($id);
        $objRequestItem = RequestItem::find($request_id);        
        $this->data['request_id'] = $id;        
        if (!empty($objRequestItem)) {
            $this->data['items'] = Items::Select('wgn_item.*', 'users.firstname', 'users.lastname', 'users.email', 'wgn_currency.currency_type')
                ->LeftJoin('users', 'wgn_item.userid', '=', 'users.id')
                ->LeftJoin('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')
                ->find($objRequestItem->item_id);

            $this->data['another_offer'] = array();
            if ($this->data['items']->offer_type == 2) {
                $this->data['another_offer'] = DB::table("wgn_make_another_itemoffer")
                ->Select('price as new_price')
                ->Where(array('driverid' => $objRequestItem->driver_id, 'status' => '1'))
                ->first();
            }

            $this->data['trips'] = DB::table("wgn_trip")->find($objRequestItem->trip_id);


            $this->data['userdetails'] = DB::table('users')
                            ->Select('users.*', 'wgn_usersdetail.*', 'wgn_cartype.type as cartype_title', 'wgn_carmodel.model as carmodel_title')
                            ->Leftjoin('wgn_usersdetail', 'users.id', '=', 'wgn_usersdetail.userid')
                            ->Leftjoin('wgn_cartype', 'wgn_usersdetail.cartype', '=', 'wgn_cartype.id')
                            ->Leftjoin('wgn_carmodel', 'wgn_usersdetail.carmodel', '=', 'wgn_carmodel.id')
                            ->Where('users.id', $objRequestItem->driver_id)->first();
            if ($objRequestItem->status != 'accept') {
                $objRequestItem->status = "accept";
                $objRequestItem->updated_at = date('Y-m-d H:i:s');
                if ($objRequestItem->save()) {
                    Return View::make('notification.buyer-accept-driver-offer', $this->data);
                } else {
                    Return Redirect::route('user.item-list')->with(array('flash_alert_notice' => 'System Error!', 'flash_action' => 'warning'))->withInput();
                }
            } else if ($objRequestItem->status == 'accept') {
                Return View::make('notification.buyer-accept-driver-offer', $this->data);
            } else {
                Return Redirect::route('user.item-view', $objRequestItem->item_id)->with(array('flash_alert_notice' => 'You already accept this request!', 'flash_action' => 'warning', 'popup' => true))->withInput();
            }
        } else {
            Return Redirect::route('user.item-list')->with(array('flash_alert_notice' => 'Item id not exist in our system!', 'flash_action' => 'warning'))->withInput();
        }
    }

    public function userItems(Request $request) {
        if ($request->ajax()) {
            $userdetail = Items::select('product_name', 'id')
                    ->selectRaw("DATE_FORMAT(created_at,'%d %b, %Y') as posted_date")
                    ->Where('userid', $this->data['userinfo']->id)
                    ->Where('status', '1')
                    ->get()
                    ->toArray();
            $flag = false;
            if (!empty($userdetail)) {
                $flag = true;
            }
            echo json_encode(array('status' => $flag, 'data' => $userdetail));
        }
    }

    public function buyerRequestDriverForItem(Request $request) {
        $file = array('item_id' => $request->input('item_id'), 'trip_id' => $request->input('trip_id'), 'driver_id' => $request->input('driver_id'));
        $rules = array('item_id' => 'required', 'trip_id' => 'required', 'driver_id' => 'required');
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
            return Redirect::route('user.driver-detail', $request->input('driver_id'))->with(array('popup' => true))->withErrors($validator)->withInput();
        } else {
            $item_notification = DB::table('wgn_notification')->Where(['item_id' => $request->input('itemid'), 'sender_id' => $this->data['userinfo']->id, 'receiver_id' => $request->input('driver_id')])->first();
            if (empty($item_notification)) {
                $objRequest = new RequestItem();
                $objRequest->buyer_id = $this->data['userinfo']->id;
                $objRequest->driver_id = $request->input('driver_id');
                $objRequest->request_by = 'buyer';
                $objRequest->item_id = $request->input('item_id');
                $objRequest->created_at = date('Y-m-d H:i:s');

                $this->data['items'] = Items::Select('wgn_item.*', 'users.firstname', 'users.lastname', 'users.email', 'wgn_currency.currency_type')
                        ->LeftJoin('users', 'wgn_item.userid', '=', 'users.id')
                        ->LeftJoin('wgn_currency', 'wgn_item.currency_type', '=', 'wgn_currency.id')
                        ->find($request->input('item_id'));

                if ($objRequest->save()) {
                    //Notification                                
                    DB::table('wgn_notification')->insert(
                            ['sender_id' => $this->data['userinfo']->id, 'item_id' => $request->input('item_id'), 'receiver_id' => $request->input('driver_id'), 'status' => '1', 'notification_type' => 'buyer-request-driver-for-item', 'created_at' => date('Y-m-d H:i:s'), 'request_id' => $objRequest->id]
                    );
                    //Mail
                    $driveInfo = DB::table('users')->find($request->input('driver_id'));
                    $to = $driveInfo->email;
                    $name = $driveInfo->firstname . " " . $driveInfo->lastname;
                    $sub = "Buyer request for item shipping - whatsgonow";
                    $template = "email.buyerRequestDriverForItem";
                    $replaces = array(
                        'buyer_name' => $this->data['userinfo']->firstname . " " . $this->data['userinfo']->lastname,
                        'item_name' => $this->data['items']->product_name,
                    );
                    $this->sendMail($to, $name, $sub, $template, $replaces);

                    return Redirect::route('user.driver-detail', $request->input('driver_id'))->with(array('flash_alert_notice' => 'Request for item shipping successfully sent!', 'flash_action' => 'success', 'popup' => true))->withInput();
                } else {
                    return Redirect::route('user.driver-detail', $request->input('driver_id'))->with(array('flash_alert_notice' => 'System Error!', 'flash_action' => 'danger', 'popup' => true))->withInput();
                }
            } else {
                return Redirect::route('user.driver-detail', $request->input('driver_id'))->with(array('flash_alert_notice' => 'Request for item shipping already sent!', 'flash_action' => 'warning', 'popup' => true))->withInput();
            }
        }
        
    }
    
    public function destroy($id){
        $deletedItems = Items::find($id);
        if($deletedItems->userid == $this->data['userinfo']->id){
            $deletedItems->status = "0";
            if ($deletedItems->save()) {
                $flash = array('flash_alert_notice' => 'Item deleted successfully !', 'flash_action' => 'success');                
            }else{
                $flash = array('flash_alert_notice' => 'Item not deleted successfully !', 'flash_action' => 'danger');                
            }
        }else{
           $flash = array('flash_alert_notice' => 'Sorry, This item not found in your item list !', 'flash_action' => 'warning');           
        } 
        if (Auth::User()->role == '1') {                
            return Redirect::route('user.profile')->with($flash)->withInput();
        } else {                
            return Redirect::route('user.driver_profile')->with($flash)->withInput();
        }
    }
    
    
    

}
