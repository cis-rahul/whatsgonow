<?php

namespace App\Http\Controllers;

use App\Http\Controllers\MainController;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\TripRequest;
use App\UserDetail;
use View;
use App\Trips;
use Validator;
use Input;
use Auth;
use DB;

class TripController extends MainController {

    public function __construct() {
        parent::__construct();
        if (Auth::check() && Auth::user()->role == '2') {
            $userDetailObj = UserDetail::Where('userid', Auth::user()->id)->first();
            if (empty($userDetailObj)) {
                return Redirect::route('user.driver_registration')->send();
            }
        }
    }

    public function index() {
        
    }

    public function create() {
        
    }

    public function store(Request $request) {
        if ($request->ajax()) {
            $file = array('trip_from' => $request->input('trip_from'), 'trip_to' => $request->input('trip_to'), 'trip_date' => $request->input('trip_date'), 'trip_time' => $request->input('trip_time'), 'country_from' => $request->input('country_from'), 'country_to' => $request->input('country_to'), 'postalcode_from' => $request->input('postalcode_from'), 'postalcode_to' => $request->input('postalcode_to'));
            $rules = array('trip_from' => 'required', 'trip_to' => 'required', 'trip_date' => 'required', 'trip_time' => 'required', 'country_from' => 'required', 'country_to' => 'required', 'postalcode_from' => 'required', 'postalcode_to' => 'required');
            $validator = Validator::make($file, $rules);
            if ($validator->fails()) {
                $status = 0;
                $data = $validator->errors();
            } else {
                $status = 1;
                $objtrip = new Trips();
                $objtrip->trip_from = $request->input('trip_from');
                $objtrip->trip_to = $request->input('trip_to');
                $objtrip->trip_date = date('Y-m-d', strtotime($request->input('trip_date')));
                $objtrip->trip_time = date("H:i:s", strtotime($request->input('trip_time')));
                $objtrip->from_postalcode = $request->input('postalcode_from');
                $objtrip->to_postalcode = $request->input('postalcode_to');
                $objtrip->from_country = $request->input('country_from');
                $objtrip->to_country = $request->input('country_to');
                $objtrip->userid = Auth::User()->id;                                


                $from_data = $request->input('postalcode_from') . ', ' . $request->input('trip_from') . ', ' . $this->data['countries'][$request->input('country_from')];
                $to_data = $request->input('postalcode_to') . ', ' . $request->input('trip_to') . ', ' . $this->data['countries'][$request->input('country_to')];

                $from_address = $this->getLatLong($from_data);
                $to_address = $this->getLatLong($to_data);
                //echo $from_address . '-> '. $to_address; die;
                if (empty($to_address) && empty($from_address)) {
                    $status = 2;
                    $data = 'Please enter valid address!';
                } else {
                    $objtrip->from_latitude = $from_address['latitude'];
                    $objtrip->from_longitude = $from_address['longitude'];
                    $objtrip->to_latitude = $to_address['latitude'];
                    $objtrip->to_longitude = $to_address['longitude'];

                    if ($objtrip->save()) {
                        $data = Trips::Select('id','trip_time', DB::Raw("DATE_FORMAT(trip_date,'%d-%c-%Y') trip_date"), 'trip_from', 'trip_to')
                                ->find($objtrip->id);
                    } else {
                        $status = 0;
                    }
                }
            }
            echo json_encode(array(
                'status' => $status,
                'data' => $data,
            ));
        }
    }

    public function edit($id) {
        
    }

    public function update(Request $request) {
        if ($request->ajax()) {
            $file = array('trip_from' => $request->input('trip_from'), 'trip_to' => $request->input('trip_to'), 'trip_date' => $request->input('trip_date'), 'trip_time' => $request->input('trip_time'), 'country_from' => $request->input('country_from'), 'country_to' => $request->input('country_to'), 'postalcode_from' => $request->input('postalcode_from'), 'postalcode_to' => $request->input('postalcode_to'));
            $rules = array('trip_from' => 'required', 'trip_to' => 'required', 'trip_date' => 'required', 'trip_time' => 'required', 'country_from' => 'required', 'country_to' => 'required', 'postalcode_from' => 'required', 'postalcode_to' => 'required');
            $validator = Validator::make($file, $rules);
            if ($validator->fails()) {
                $status = 0;
                $data = $validator->errors();
            } else {
                $status = 1;
                $objtrip = Trips::find($request->input('trip_id'));
                $objtrip->trip_from = $request->input('trip_from');
                $objtrip->trip_to = $request->input('trip_to');
                $objtrip->trip_date = date('Y-m-d', strtotime($request->input('trip_date')));
                $objtrip->trip_time = date("H:i:s", strtotime($request->input('trip_time')));
                $objtrip->from_postalcode = $request->input('postalcode_from');
                $objtrip->to_postalcode = $request->input('postalcode_to');
                $objtrip->from_country = $request->input('country_from');
                $objtrip->to_country = $request->input('country_to');
                $objtrip->userid = Auth::User()->id;                                


                $from_data = $request->input('postalcode_from') . ', ' . $request->input('trip_from') . ', ' . $this->data['countries'][$request->input('country_from')];
                $to_data = $request->input('postalcode_to') . ', ' . $request->input('trip_to') . ', ' . $this->data['countries'][$request->input('country_to')];

                $from_address = $this->getLatLong($from_data);
                $to_address = $this->getLatLong($to_data);
                //echo $from_address . '-> '. $to_address; die;
                if (empty($to_address) && empty($from_address)) {
                    $status = 2;
                    $data = 'Please enter valid address!';
                } else {
                    $objtrip->from_latitude = $from_address['latitude'];
                    $objtrip->from_longitude = $from_address['longitude'];
                    $objtrip->to_latitude = $to_address['latitude'];
                    $objtrip->to_longitude = $to_address['longitude'];

                    if ($objtrip->save()) {
                        $data = Trips::Select('id','trip_time', DB::Raw("DATE_FORMAT(trip_date,'%d-%c-%Y') trip_date"), 'trip_from', 'trip_to')
                                ->find($objtrip->id);
                    } else {
                        $status = 0;
                    }
                }
            }
            echo json_encode(array(
                'status' => $status,
                'data' => $data,
            ));
        }
    }

    public function show($id) {
        
    }

    public function userTrips(Request $request) {
        if ($request->ajax()) {
            //DB::enableQueryLog();
            $tripdetail = Trips::Select('id', 'trip_from', 'trip_to', 'trip_date', 'trip_time')
                    ->Where('userid', $this->data['userinfo']->id)
                    ->Where('status', '1')
                    ->whereRaw("CONCAT(trip_date, ' ', trip_time) >='" . date('Y-m-d H:i:s') . "'")
                    ->get()
                    ->toArray();
//          print_r(DB::getQueryLog());die;
            $flag = false;
            if (!empty($tripdetail)) {
                $flag = true;
            }
            echo json_encode(array('status' => $flag, 'data' => $tripdetail));
        }
    }
    public function tripInfoByID(Request $request){        
        if($request->ajax()){
            $tripInfo = Trips::Select('id','trip_time', DB::Raw("DATE_FORMAT(trip_date,'%d-%c-%Y') trip_date"), 'trip_from', 'trip_to','from_country','to_country','from_postalcode', 'to_postalcode')
                    ->find($request->input('trip_id'));
            $status = false;
            $data = array();
            if(!empty($tripInfo)){
                $status = true;
                $data = $tripInfo;
                $request->session()->flash('flash_alert_notice', 'Trip successfully updated!');
                $request->session()->flash('flash_action', 'success');
            }
        }
        echo json_encode(array(
            'status'=>$status,
            'data' => $data
        ));
    }
    public function destroy(Request $request){
        $trip_id = $request->input('trip_id');
        $tripObj = Trips::find($trip_id);
        $tripObj->status = '0';
        if($tripObj->save()){
            echo true;
        }else{
            echo false;
        }
        
    }

}
