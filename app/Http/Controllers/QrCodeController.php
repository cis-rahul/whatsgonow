<?php

namespace App\Http\Controllers;

use App\Http\Controllers;
use App\Http\Controllers\Auth\AuthController;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegistrationRequest;
use Illuminate\Support\Facades\Redirect;
use config\QrCode\QrCode;
use View;
use App\User;
use App\Items;
use App\UserDetail;
use App\Countries;
use Session;
use Auth;
use Validator;
use Input;
use DB;
use Mail;
use Response;

//use Input;


class QrCodeController extends MainController {

    /**
     * Created By: Lokendra 
     * Created Dt: 12-Desc-2016
     */
    public function __construct() {
        
    }

    //Get Method
    public function qrCodeGenretor() {
        $amount = '$85';
        $item = 'Motal';
        $qr = new QrCode();
        $qr->setText("http://www.whatsgonow.com?itemName=Mobile&price={$amount}&itemname={$item}")
        ->setSize("200")
        ->render("public/uploads/qrcodes/" . "seller_QrCode_".time());                
    }    

}
