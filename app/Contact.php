<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
   protected $table = "wgn_contacts";
    protected $fillable = ['id','name','email','phone','comment','created_at','updated_at'];
}
