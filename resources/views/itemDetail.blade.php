@extends('layouts/home')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
<link href="{{ asset('public/css/datepicker.css') }}" rel="stylesheet">
<link href="{{ asset('public/css/bootstrap-timepicker.min.css') }}" rel="stylesheet">
<style type='text/css'>
    .driv_prof {  border-radius: 20%;  height:50px;}
</style>
@stop
{{-- Page content --}}
@section('content')
<!-- Start content -->
<section>
    <!-- Start content -->
    <div class="container">
        <div class="col-md-12 wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0s">
            <div class="title-area">
                <h2 class="title">Item Details</h2>
                <span class="line"></span>
            </div>
        </div>
        <div class="col-md-12 byfrmot">
            @if (session('popup'))
            <div class="flash-message">
                <div class="alert alert-{{session('flash_action')}}">
                    <p>{{session('flash_alert_notice')}}</p>
                </div>
            </div>
            @endif
            @if (!empty($errors->all()) && session('popup'))
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form id='frm_driver_accept' method='POST' action='{{route('item_request_accept_buyer')}}'>                
                <input type='hidden' name='_token' id='userid' value='{{csrf_token()}}'/>
                <input type='hidden' name='userid' id='userid' value='{{$itemdetails->userid}}'/>
                <input type='hidden' name='itemid' id='itemid' value='{{$itemdetails->id}}'/>
                <input type="hidden" id="trip_id" name="trip_id"/>
                <div class="col-md-9 col-sm-12 rgtbarnw upldfrm wow zoomIn" data-wow-duration="0.8s" data-wow-delay="0s">
                    <div class="row">
                        <div class="uplimgs">
                            <div class="col-sm-3 col-xs-3">
                                <div class="imgs1">
                                    <span>
                                        @if(!empty($itemdetails->item_image1))
                                        <?php $path = route('home') . "/public/uploads/items/" . $itemdetails->item_image1; ?>
                                        <img style="width:180px;height:180px;" class="img-responsive" src="{{ $path }}" alt=""/>
                                        @else
                                        <img style="width:180px;height:180px;" class="img-responsive" src="{{ asset('public/images/imgs1.jpg') }}" alt=""/>
                                        @endif
                                    </span>                                    
                                </div>
                            </div>

                            <div class="col-sm-3 col-xs-3">
                                <div class="imgs1">
                                    <span>
                                        @if(!empty($itemdetails->item_image2))
                                        <?php $path = route('home') . "/public/uploads/items/" . $itemdetails->item_image2; ?>
                                        <img style="width:180px;height:180px;" class="img-responsive" src="{{ $path }}" alt=""/>
                                        @else
                                        <img style="width:180px;height:180px;" class="img-responsive" src="{{ asset('public/images/imgs1.jpg') }}" alt=""/>
                                        @endif
                                    </span>                                    
                                </div>
                            </div>

                            <div class="col-sm-3 col-xs-3">
                                <div class="imgs1">
                                    <span>
                                        @if(!empty($itemdetails->item_image3))
                                        <?php $path = route('home') . "/public/uploads/items/" . $itemdetails->item_image3; ?>
                                        <img style="width:180px;height:180px;" class="img-responsive" src="{{ $path }}" alt=""/>
                                        @else
                                        <img style="width:180px;height:180px;" class="img-responsive" src="{{ asset('public/images/imgs1.jpg') }}" alt=""/>
                                        @endif
                                    </span>
                                </div>
                            </div>

                            <div class="col-sm-3 col-xs-3">
                                <div class="imgs1">
                                    <span>
                                        @if(!empty($itemdetails->item_image4))
                                        <?php $path = route('home') . "/public/uploads/items/" . $itemdetails->item_image4; ?>
                                        <img style="width:180px;height:180px;" class="img-responsive" src="{{ $path }}" alt=""/>
                                        @else
                                        <img style="width:180px;height:180px;" class="img-responsive" src="{{ asset('public/images/imgs1.jpg') }}" alt=""/>
                                        @endif
                                    </span>
                                </div>
                            </div>

                        </div><!-- End upload images -->

                        <div class="mesuresec">
                            <div class="col-xs-4 col-sm-2 form-group">
                                <label>Measures</label>                                
                                <select class="form-control" id="measures" name="measures" disabled="disabled">
                                    <option value="">Please Select</option>
                                    <option value="1" <?php echo (isset($itemdetails->measures) && $itemdetails->measures == 1) ? "selected='selected'" : "" ?>>CM</option>
                                    <option value="2" <?php echo (isset($itemdetails->measures) && $itemdetails->measures == 2) ? "selected='selected'" : "" ?>>Inch</option>
                                </select>                                
                            </div>

                            <div class="col-xs-4 col-sm-2 form-group" >
                                <label>Length</label>
                                <input type="text" class="form-control" placeholder="10" value="{{!empty($itemdetails->item_length) ? $itemdetails->item_length : '' }}">
                            </div>

                            <div class="col-xs-4 col-sm-2 form-group">
                                <label>High</label>
                                <input type="text" class="form-control" placeholder="07" value="{{!empty($itemdetails->item_height) ? $itemdetails->item_height : '' }}">
                            </div>

                            <div class="col-xs-4 col-sm-2 form-group">
                                <label>Width</label>
                                <input type="text" class="form-control" placeholder="05" value="{{!empty($itemdetails->item_width) ? $itemdetails->item_width : '' }}">
                            </div>

                            <div class="col-xs-4 col-sm-2 form-group">
                                <label>Weight/KG</label>
                                <input type="text" class="form-control" placeholder="35" value="{{!empty($itemdetails->item_weight) ? $itemdetails->item_weight : '' }}">
                            </div>

                            <div class="col-xs-4 col-sm-2 form-group">
                                <label>Pieces</label>
                                <input type="text" class="form-control" placeholder="0-100" value="{{!empty($itemdetails->item_pieces) ? $itemdetails->item_pieces : '' }}">
                            </div>
                        </div><!--Measurement-->
                        <div class="ctgrys">
                            <div class="col-xs-12 col-sm-4 form-group">
                                <label>Product Name</label>
                                <input type="text" class="form-control" placeholder="Select Product Value"   value="{{!empty($itemdetails->product_name) ? $itemdetails->product_name : '' }} ">
                            </div>
                            <div class="col-xs-12 col-sm-4 form-group">
                                <label>Product Value</label>
                                <input type="text" class="form-control" placeholder="Select Product Value"   value="{{!empty($itemdetails->item_value) ? $itemdetails->item_value : '' }} ">
                            </div>

                            <div class="col-xs-12 col-sm-4 form-group">
                                <label>Currency</label>
                                <input type="text" class="form-control" placeholder="Select Currency" value="{{!empty($itemdetails->currency_type) ? $itemdetails->currency_type: '' }}" >
                            </div>

                        </div><!--category-->
                        <div class="ctgrys">
                            <div class="col-xs-12 col-sm-4 form-group">
                                <label>Category</label>
                                <input type="text" class="form-control" placeholder="Select Category" value="{{!empty($itemdetails->category_id) ? DB::table('wgn_item_category')->find($itemdetails->category_id)->category_name : '' }}" >
                            </div>

                            <div class="col-xs-12 col-sm-4 form-group">
                                <label>Sub Category 1</label>
                                <input type="text" class="form-control" placeholder="Select Sub Category" value="{{!empty($itemdetails->subcategory_id) ? DB::table('wgn_item_subcategory')->find($itemdetails->subcategory_id)->subcat_name : '' }}">
                            </div>

                            <div class="col-xs-12 col-sm-4 form-group">
                                <label>Sub Category 2</label>
                                <input type="text" class="form-control" placeholder="Select Sub Category" value="{{!empty($itemdetails->second_subcat_id) ? DB::table('wgn_item_subcategory_two')->find($itemdetails->second_subcat_id)->sub_cat_name : '' }}">
                            </div>

                        </div><!--category-->

                        <div class="col-xs-12 form-group">
                            <label>Item Description</label>
                            <textarea name="itemDesc" disabled="disabled" class="form-control" placeholder="Enter Item Description">{{!empty($itemdetails->item_description) ? $itemdetails->item_description : '' }}</textarea>
                        </div>
                        <div class="col-xs-12 form-group frmsec">
                            <label>From</label>
                            <div class="row frmsec">
                                <div class="col-xs-12 col-sm-2 form-group">
                                    <input type="text" class="form-control" placeholder="Number" id="from_street_number" name="from_street_number" value="{{(!empty($itemdetails->from_street_number))?$itemdetails->from_street_number:''}}">
                                </div>
                                <div class="col-xs-12 col-sm-3 form-group">
                                    <input type="text" class="form-control" placeholder="Street" id="from_street" name="from_street" value="{{(!empty($itemdetails->from_street))?$itemdetails->from_street:''}}">
                                </div> 
                                <div class="col-xs-12 col-sm-2 form-group">
                                    <input type="text" class="form-control" placeholder="Postal Code" value="{{!empty($itemdetails->from_postal_code) ? $itemdetails->from_postal_code : '' }}">
                                </div>

                                <div class="col-xs-12 col-sm-2 form-group">
                                    <input type="text" class="form-control" placeholder="City" value="{{!empty($itemdetails->from_city) ? $itemdetails->from_city : '' }}">
                                </div>

                                <div class="col-xs-12 col-sm-3 form-group">
                                    <input type="text" class="form-control" placeholder="Country" value="{{ !empty($itemdetails->from_country) ? $countries[$itemdetails->from_country] : '' }}">
                                </div>
                            </div>

                        </div>

                        <div class="col-xs-12 form-group frmsec">
                            <label>To</label>
                            <div class="row frmsec">
                                <div class="col-xs-12 col-sm-2 form-group">
                                    <input type="text" class="form-control" placeholder="Number" id="to_street_number" name="to_street_number" value="{{!empty($itemdetails->to_street_number) ? $itemdetails->to_street_number : '' }}">
                                </div>
                                <div class="col-xs-12 col-sm-3 form-group">
                                    <input type="text" class="form-control" placeholder="Street" id="to_street" name="to_street" value="{{!empty($itemdetails->to_street) ? $itemdetails->to_street : '' }}">
                                </div>
                                <div class="col-xs-12 col-sm-2 form-group">
                                    <input type="text" class="form-control" placeholder="Postal Code" value="{{!empty($itemdetails->to_postal_code) ? $itemdetails->to_postal_code : '' }}">
                                </div>

                                <div class="col-xs-12 col-sm-2 form-group">
                                    <input type="text" class="form-control" placeholder="City" value="{{!empty($itemdetails->to_city) ? $itemdetails->to_city : '' }}">
                                </div>

                                <div class="col-xs-12 col-sm-3 form-group">
                                    <input type="text" class="form-control" placeholder="Country" value="{{ !empty($itemdetails->to_country) ?  $countries[$itemdetails->to_country] : '' }} ">
                                </div>
                            </div>
                        </div>                        
                        <div class="col-xs-12">
                            <div class="ofrsec">
                                <h4>OFFER</h4>

                                <div class="form-group ofrcrpr">
                                    <div class="col-xs-6">
                                        <label class="col-xs-4">Currency:</label>
                                        <div class="col-xs-8">
                                            <input type="text" class="form-control" placeholder="$ Dollar" value="{{!empty($itemdetails->item_currency) ? $itemdetails->currency_type : '' }}">
                                        </div>

                                    </div>

                                    <div class="col-xs-6">
                                        <label class="col-xs-4">Price:</label>
                                        <div class="col-xs-8">
                                            <input type="text" class="form-control" placeholder="00.00" value="{{!empty($itemdetails->item_price) ? $itemdetails->item_price : '' }}" >
                                        </div>
                                    </div>
                                </div>

                                <?php
                                $checked_fix = '';
                                $checked_nego = '';
                                if (isset($itemdetails->offer_type) && ($itemdetails->offer_type == '1')) {
                                    $checked_fix = 'checked';
                                } else {
                                    $checked_nego = 'checked';
                                }
                                ?>

                                <div class="ofrckbx">
                                    <div class="col-xs-6 form-group agrytrms">
                                        <label class="custom-control custom-checkbox">
                                            <input value="1" name="checkbox" {{ $checked_fix }} class="custom-control-input" type="checkbox">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Fix</span>
                                        </label>
                                    </div>

                                    <div class="col-xs-6 form-group agrytrms">
                                        <label class="custom-control custom-checkbox">
                                            <input value="1" name="checkbox" {{ $checked_nego }} class="custom-control-input" type="checkbox">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Negotiate</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="crtpy wddslcr form-group">                            
                            <div class="col-xs-6 col-sm-6">                                
                                <label>Type of Car</label>
                                <input type="text" class="form-control" placeholder="Select car type" value="{{!empty($itemdetails->car_type) ? $itemdetails->cartype : '' }}">
                            </div>
                            <div class="col-xs-6 col-sm-6">                                
                                <label>Car Model</label>
                                <input type="text" class="form-control" placeholder="Select car type" value="{{!empty($itemdetails->car_type) ? $itemdetails->cartype : '' }}">
                            </div>                                               
                        </div><!--category-->
                        <div class="col-xs-12 form-group prcsec">
                            <div class="row">
                                @if(Auth::Check())                                
                                    @if($userinfo->id != $itemdetails->userid)
                                        @if($userinfo->role != '1')
                                        <div class="col-xs-12 col-sm-6 askbtn">                                    
                                            <a id='btn_accept' href='javascript:void(0)' name='btn_accept'>Accept</a>
                                        </div>                                    
                                        <?php if (!$checked_fix) { ?>
                                            <div class="col-xs-12 col-sm-6 accprbtn">
                                                <a id="mark_another_offer" href="javascript:void(0)">Make Another Offer</a>
                                            </div>
                                        <?php } ?>                                        
                                        @endif
                                    @endif                                    
                                @endif                                
                            </div>
                        </div>
<!--                        <div class="col-xs-12 adantrbtn">                            
                            <a href="javascript:void(0)">Contact : {{!empty($itemdetails->userid) ? DB::table('users')->find($itemdetails->userid)->firstname : '' }}</a>
                            @if(Auth::check() && $itemdetails->userid == Auth::User()->id)     
                            <a _method="delete" href="{{ route('item_destroy',  $itemdetails->id) }}">Delete Item</a>
                            @endif
                        </div>-->
                    </div><!-- End row -->
                </div><!-- End right panel -->
            </form>
        </div>
    </div>
    <!-- End content -->    
</section>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body">
                <div class="flash-message">
                    <div id="error_msg_div" class="">
                        <p id="error_msg"></p>
                    </div>
                </div>
                @if (session('flash_alert_notice') && !session('popup'))
                <div class="flash-message">
                    <div class="alert alert-{{session('flash_action')}}">
                        <p>{{session('flash_alert_notice')}}</p>
                    </div>
                </div>
                @endif
                @if (!empty($errors->all()) && !session('popup'))
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif                    
                <form id="frm_makeAnotherOffer" action="{{route('make_another_itemoffer')}}" method="POST">
                    <input type="hidden" id="driver_id" name="driver_id" value="{{@$userinfo['id']}}"/>
                    <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" id="itemid" name="itemid" value="{{@$itemdetails->id}}"/>
                    <input type="hidden" id="newprice_id" name="newprice_id" value="{{@$driverNewPrice->id}}"/>
                    <input type="hidden" id="trip_id" name="trip_id"/>
                    <div class="title-area">
                        <h2 class="title">Make Another Offer</h2>
                        <span class="line"></span>
                    </div>
                    <div class="frmtsec2">
                        <h3>Current Offer</h3>
                        <h4>{{!empty($itemdetails->currency_type) ? $currency[$itemdetails->currency_type] : '$' }} <span id="old_price">{{!empty($itemdetails->item_price) ? $itemdetails->item_price : '0' }}</span></h4>
                    </div>
                    <div class="frmtsec2 frmofmn">
                        <h3>Enter new firm offer</h3>
                        <div class="nwfrofr">
                            <select class="form-control">                                
                                <option value="">{{!empty($itemdetails->currency_type) ? $currency[$itemdetails->currency_type] : '$' }}</option>                                
                            </select>
                            <input id="new_price" name="new_price" type="text" placeholder="30.00" />
                        </div>
                    </div>
                    <div class="col-md-12 form-group agrytrms">
                        <label class="custom-control custom-checkbox">
                            <input value="1" id="checkbox_new_price" name="checkbox" class="custom-control-input" type="checkbox">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">I agree to the Terms &amp; Conditions</span>
                        </label>
                    </div>
                </form>    
            </div>
            <button type="button" id="btn_makeAnotherOffer" class="btn btn-default">Submit</button>
        </div>
    </div>
</div><!-- Modal End-->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body">
                <div class="flash-message">
                    <div id="error_msg_div" class="">
                        <p id="error_msg"></p>
                    </div>
                </div>
                @if (session('flash_alert_notice') && !session('popup'))
                <div class="flash-message">
                    <div class="alert alert-{{session('flash_action')}}">
                        <p>{{session('flash_alert_notice')}}</p>
                    </div>
                </div>
                @endif
                @if (!empty($errors->all()) && !session('popup'))
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif                    
                <form id="frm_makeAnotherOffer" action="{{route('make_another_itemoffer')}}" method="POST">
                    <input type="hidden" id="driver_id" name="driver_id" value="{{@$userinfo['id']}}"/>
                    <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" id="itemid" name="itemid" value="{{@$itemdetails->id}}"/>
                    <input type="hidden" id="newprice_id" name="newprice_id" value="{{@$driverNewPrice->id}}"/>
                    <div class="title-area">
                        <h2 class="title">Make Another Offer</h2>
                        <span class="line"></span>
                    </div>
                    <div class="frmtsec2">
                        <h3>Current Offer</h3>
                        <h4>{{!empty($itemdetails->currency_type) ? $currency[$itemdetails->currency_type] : '$' }} <span id="old_price">{{!empty($itemdetails->item_price) ? $itemdetails->item_price : '0' }}</span></h4>
                    </div>
                    <div class="frmtsec2 frmofmn">
                        <h3>Enter new firm offer</h3>
                        <div class="nwfrofr">
                            <select class="form-control">                                
                                <option value="">{{!empty($itemdetails->currency_type) ? $currency[$itemdetails->currency_type] : '$' }}</option>                                
                            </select>
                            <input id="new_price" name="new_price" type="text" placeholder="30.00" />
                        </div>
                    </div>
                    <div class="col-md-12 form-group agrytrms">
                        <label class="custom-control custom-checkbox">
                            <input value="1" id="checkbox_new_price" name="checkbox" class="custom-control-input" type="checkbox">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">I agree to the Terms &amp; Conditions</span>
                        </label>
                    </div>
                </form>    
            </div>
            <button type="button" id="btn_makeAnotherOffer" class="btn btn-default">Submit</button>
        </div>
    </div>
</div><!-- Modal End-->
<div id="myModalTrip" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body" style="display:inherit;">
                <input type="hidden" id="which_form" name="which_form" value=""/>
                <div class="table-responsive text-center bypftbl">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>                                    
                                <th>From</th>
                                <th>To</th>
                                <th>Date</th>
                                <th>Time</th>
                            </tr>
                        </thead>
                        <tbody>                            

                        </tbody>
                    </table>
                </div> 
            </div>
        </div>    
    </div><!-- Modal End-->
</div>
<!-- End content -->
@stop
@section('footer_scripts')
<script>
    $(document).ready(function() {
        var popup = "<?php echo session('popup') ?>";
        var error = "<?php echo count($errors->all()); ?>";
        if (error != 0 && popup == "") {
            $("#myModal").modal({backdrop: 'static', keyboard: false});
        }
        var error = "<?php echo session('flash_alert_notice'); ?>";
        if (error != "" && popup == "") {
            $("#myModal").modal({backdrop: 'static', keyboard: false});
        }

        $('#btn_accept').click(function() {
            $("#myModalTrip  #which_form").val('frm_driver_accept');
            triplist();
        });
        $("body").on("click", ".btn_trip_select", function() {
            var flag = true;
            $(".trip_id").each(function(key, val) {
                if ($(this).is(":checked")) {
                    flag = false;
                    $("#" + $("#myModalTrip  #which_form").val() + " > #trip_id").val($(this).val());
                    $("#myModalTrip").modal('hide');
                    $("#" + $("#myModalTrip  #which_form").val()).submit();
                }
            });
            if (flag) {
                alert("Please select Trip, It's mandatory");
            }
        });

        $(".emntfctn").mouseover(function() {
            $(".ntfdtinf").css("display", "block");
        });
        $(".emntfctn").mouseout(function() {
            $(".ntfdtinf").css("display", "none");
        });
        $("#mark_another_offer").click(function() {
            $("#myModal").modal({backdrop: 'static', keyboard: false});
        });
        $("#btn_makeAnotherOffer").click(function() {
            var new_price = $("#new_price").val();
            var old_price = $("#old_price").text();
            if ($("#checkbox_new_price").is(":checked")) {
                if (new_price != "") {
                    if (parseInt(new_price) <= parseInt(old_price)) {
                        $("#error_msg_div").addClass("alert");
                        $("#error_msg_div").addClass("alert-warning");
                        $("#error_msg").html("New price should be greater than old price!");
                    } else {
                        $("#myModal").modal('hide');
                        $("#myModalTrip  #which_form").val('frm_makeAnotherOffer');
                        triplist();
                    }
                } else {
                    $("#error_msg_div").addClass("alert");
                    $("#error_msg_div").addClass("alert-warning");
                    $("#error_msg").html("New price can't be blank!");
                }
            } else {
                $("#error_msg_div").addClass("alert");
                $("#error_msg_div").addClass("alert-warning");
                $("#error_msg").html("Without accept Term & Condition you can't proceed!");
            }
        });
    });
    function triplist() {
        /* Ajax for get trip list */
        var user_id = "<?php echo @$userinfo->id ?>";
        $.ajax({
            url: "{{route('user_trips')}}",
            dataType: 'JSON',
            type: 'POST',
            data: ({user_id: user_id, _token: "{{csrf_token()}}"}),
            success: function(res) {
                if (res['status'] == true) {
                    var rows = "";
                    $.each(res['data'], function(key, val) {
                        rows += "<tr>";
                        rows += "<td><input type='radio' value='" + val['id'] + "' name='trip_id' class='trip_id'/></td>";
                        rows += "<td>" + val['trip_from'] + "</td>";
                        rows += "<td>" + val['trip_to'] + "</td>";
                        rows += "<td>" + val['trip_date'] + "</td>";
                        rows += "<td>" + val['trip_time'] + "</td>";
                        rows += "</tr>";
                    });
                    rows += "<tr>";
                    rows += "<td colspan='5'><input class='btn btn-info form-control btn_trip_select' type='button' value='Submit'/></td>";
                    rows += "</tr>";
                    $('#myModalTrip tbody').html(rows);
                    $("#myModalTrip").modal({backdrop: 'static', keyboard: false});

                } else {
                    rows += "<tr>";
                    rows += "<td colspan='5'>No Item Found!</td>";
                    rows += "</tr>";
                    $('#myModalTrip tbody').html(rows);
                    $("#myModalTrip").modal();
                }

            },
            error: function(res) {
                alert("Network Error!");
            }
        });
    }
</script>

<script>
    $(document).ready(function() {
        $("input").prop('readonly', true);
        $("#new_price").prop('readonly', false);
        $("#checkbox_new_price").prop('readonly', false);
        var s = $("header");
        var pos = s.position();
        $(window).scroll(function() {
            var windowpos = $(window).scrollTop();
            if (windowpos >= pos.top & windowpos <= 10) {
                s.removeClass("stick");
            } else {
                s.addClass("stick");
            }
        });
    });

</script>
<!-- Write your script here -->
@stop