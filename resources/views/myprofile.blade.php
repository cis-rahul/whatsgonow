@extends('layouts/home')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
<style type='text/css'>
    .accprbtn button {background: #ffaf42;border-radius: 5px;color: #000;display: inline-block;font-family: "montserratregular";font-size: 18px;height: 49px;line-height: 49px;margin: 0;text-decoration: none;text-shadow: 1px 1px 1px #a0a0a0;width: 100%;text-align: center;}
    .has-error .form-control {border-color: #a94442 !important;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);}
</style>
@stop
{{-- Page content --}}
@section('content')
<!-- Start content -->
<div class="container">
    <div class="col-md-12 wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0s">
        <div class="title-area">
            <h2 class="title">{{ trans('message.MyProfile') }}</h2>
            <span class="line"></span>
        </div>
    </div>
    
    <div class="col-md-12 byfrmot">        
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if (session('flash_alert_notice'))
        <div class="flash-message">
            <div class="alert alert-{{session('flash_action')}}">
                <p>{{session('flash_alert_notice')}}</p>
            </div>
        </div>
        @endif
        <form action="{{route('user.profile')}}" method="POST" role="form" id="frm_profieupdate" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <div class="col-md-4 lftbar wow slideInLeft" data-wow-duration="0.8s" data-wow-delay="0.5s">
                <div class="col-xs-12 text-center">
                    <div class="uplsec">
                        @if(!empty($userinfo['userimage']))
                        <?php $path = route('home') . "/public/uploads/users/" . $userinfo['userimage']; ?>
                        <span><img width="135px" height="135px"  src="{{ $path}}" alt=""/></span>
                        @else
                        <span><img width="135px" height="135px" src="{{ asset('public/images/usrimg.png') }}" alt=""/></span>
                        @endif

                        <div class="uplbtnmn">
                            <div class="uplbtn">
                                <input type="file" value="" id="userimage" name="userimage"/>
                                <img src="{{ asset('public/images/uplbtn1.png') }}" alt=""/>
                            </div>
                        </div>
                    </div>	
                </div><!--upload image-->
                <div class="col-xs-12 form-group {{ $errors->first('username', 'has-error')}}">
                    <label>{{ trans('message.UserName') }}</label>
                    <input type="text" class="form-control" id='username' name='username' placeholder="{{ trans('message.EnterUserName') }}" value='{{(!empty($userinfo['username']))?$userinfo['username']:Input::old('username')}}'>
                </div>
                <p class="col-xs-12">{{ trans('message.permisssionContent') }}</p>
                <div class="col-xs-12 form-group {{ $errors->first('phone', 'has-error')}}">
                    <label>{{ trans('message.Phone') }}</label>
                    <input type="text" class="form-control" id='phone' name='phone' placeholder="{{ trans('message.EnterYourPhoneNumber') }}" value='{{(!empty($userinfo['phone']))?$userinfo['phone']:Input::old('phone')}}'>
                </div>
                <div class="col-xs-12 form-group {{ $errors->first('postalcode', 'has-error')}}">
                    <label>{{ trans('message.PostalCode') }}</label>
                    <input type="text" class="form-control" id='postalcode' name='postalcode' placeholder="{{ trans('message.PostalCode') }}" value='{{(!empty($userinfo['postalcode']))?$userinfo['postalcode']:Input::old('postalcode')}}'>
                </div>
                <div class="col-xs-12 form-group {{ $errors->first('city', 'has-error')}}">
                    <label>{{ trans('message.City') }}</label>
                    <input type="text" class="form-control" id='city' name='city'  placeholder="{{ trans('message.City') }}" value='{{(!empty($userinfo['city']))?$userinfo['city']:Input::old('city')}}'>
                </div>
                <div class="col-xs-12 form-group {{ $errors->first('country', 'has-error')}}">
                    <label>{{ trans('message.Country') }}</label>
                    <div class="ctyslct">
                        <select class="form-control" id='country' name='country'  >
                            @if(!empty($countries))
                            <option selected="" value=''>{{ trans('message.PleaseSelect') }}</option>
                            @foreach($countries as $key=>$objCountry)                           
                            @if(@Input::old('country') == @$key || @$userinfo['country'] == @$key)
                            <option value='{{$key}}' selected='selected'>{{$objCountry}}</option>
                            @else
                            <option value='{{$key}}'>{{$objCountry}}</option>
                            @endif
                            @endforeach
                            @endif                            
                        </select>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-8 padd-left form-group {{ $errors->first('street', 'has-error')}}">
                        <label>{{ trans('message.Street') }}</label>
                        <input type="text" name='street' id='street' class="form-control" placeholder="{{ trans('message.EnterStreet') }}" value='{{(!empty($userinfo['street']))?$userinfo['street']:Input::old('street')}}'>
                    </div>	
                    <div class="col-xs-4 padd form-group {{ $errors->first('housenumber', 'has-error')}}">
                        <label>{{ trans('message.No') }}</label>
                        <input type="text" class="form-control" id='housenumber' name='housenumber' placeholder="{{ trans('message.HouseNumber') }}" value='{{(!empty($userinfo['housenumber']))?$userinfo['housenumber']:Input::old('housenumber')}}'>
                    </div>
                </div>               
            </div><!-- End left panel -->
        </form>    
        <div class="col-md-8 col-sm-12 rgtbarnw wow slideInRight" data-wow-duration="0.8s" data-wow-delay="0.5s">
            <div class="row">
                <h5>{{ trans('message.ListOfItems') }}</h5>
                <div class="table-responsive text-center bypftbl">
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>{{ trans('message.LOCATION') }}</th>
                                <th>{{ trans('message.DESTINATION') }}</th>
                                <th>{{ trans('message.OFFER') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="tableinhdtp">
                                <td width="10%"></td>
                                <td width="35%">
                                    <table class="tableinhd">
                                        <thead>
                                            <tr>
                                                <th style='width:33%;text-align:center;'>{{ trans('message.City') }}</th>
                                                <th style='width:33%;text-align:center;'>{{ trans('message.PostalCode') }}</th>
                                                <th style='width:33%;text-align:center;'>{{ trans('message.Country') }}</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </td>
                                <td width="35%">
                                    <table class="tableinhd">
                                        <thead>
                                            <tr>
                                                <th style='width:33%;text-align:center;'>{{ trans('message.City') }}</th>
                                                <th style='width:33%;text-align:center;'>{{ trans('message.PostalCode') }}</th>
                                                <th style='width:33%;text-align:center;'>{{ trans('message.Country') }}</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </td>
                                <td width="10%"></td>
                                <td width="10%"></td>
                            </tr>                            
                            @if(!empty($useritems))
                            @foreach($useritems as $objUserItems)
                            <tr>
                                <td class="itmimgtd">
                                        <?php $profile_pic = isset($objUserItems['item_image1']) && !empty($objUserItems['item_image1']) ? $objUserItems['item_image1'] : 'itmimg.png'; ?>
                                        <a href="{{ route('user.item-view',$objUserItems['id']) }}"> <span class="itmimg"><img class="driv_prof" src="{{ route('home'). "/public/uploads/items/" . $profile_pic }}" alt = ""/></span></a>
                                    </td>
                                <td>
                                    <table class="tblindata">
                                        <tbody>
                                            <tr>
                                                <td style='width:33%;text-align:center;'>{{$objUserItems['from_city']}}</td>
                                                <td style='width:33%;text-align:center;'>{{$objUserItems['from_postal_code']}}</td>
                                                <td style='width:33%;text-align:center;'>{{$countries[$objUserItems['from_country']]}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <table class="tblindata">
                                        <tbody>
                                            <tr>
                                                <td style='width:33%;text-align:center;'>{{$objUserItems['to_city']}}</td>
                                                <td style='width:33%;text-align:center;'>{{$objUserItems['to_postal_code']}}</td>
                                                <td style='width:33%;text-align:center;'>{{$countries[$objUserItems['to_country']]}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>$ {{$objUserItems['item_price']}}</td>
                                <td class="acticns">
                                    <a href="{{route('item.edit', $objUserItems['id'])}}"><img src="{{ asset('public/images/edticn.png')}}" alt=""></a>
                                    <a href="{{route('item_destroy', $objUserItems['id'])}}"><img src="{{ asset('public/images/dlet.png')}}" alt=""></a>
                                </td>
                            </tr>
                            @endforeach
                            @else                            
                            <tr>
                                <td colspan="5">{{ trans('message.NoRecordAvailable') }} </td>
                            </tr>
                            @endif

                        </tbody>

                    </table>
                </div> 

                <div class="aditms">
                    <a class="additbtn" href="{{route('item.create')}}"><img src="{{ asset('public/images/plsicn.png') }}" alt=""/>{{ trans('message.AddNewItem') }} </a>
                    <div class="col-xs-12 form-group">
                        <label>{{ trans('message.NotDriver') }}</label>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 askbtn">
                                <a href="{{route('user.driver_registration')}}">{{ trans('message.BecomeaDriver') }}</a>
                            </div>
                            <div class="col-xs-12 col-sm-6 accprbtn">
                                <a href="javascript:void(0)" id="btn_profile_update">{{ trans('message.Update') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- End row -->
        </div><!-- End right panel -->
    </div>



</div>
<!-- End content -->
@stop

@section('footer_scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_profile_update").click(function() {
            $("#frm_profieupdate").submit();
        });
    });
    $('body').on('change', 'input[type="file"]', function(event) {
        var dd = $(this).parents('.uplsec').parent();
        var src = URL.createObjectURL(event.target.files[0]);
        dd.find('span').find('img').attr('src', src);
    });
</script>
<!-- Write your script here -->
@stop