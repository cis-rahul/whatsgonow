@extends('layouts/notification')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
@stop
{{-- Page content --}}

@section('content')   
<!-- End header -->
<!-- Start content -->
<div class="container">
    <div class="col-md-12 ntcbyr ntftdrvasc wow zoomIn" data-wow-duration="1.0s" data-wow-delay="0.0s">
        <h6>You have successfully delivered</h6>
        <h3>{{$items->product_name}}</h3>
        <p>Whatsgonow will release your funds minus 2%</p>
        <div class="ntfdrprc">
            <h3>Price:</h3>
            <h4>$ 29.40</h4>
            <p>Thank You</p>
            <div class="ntfdrbtns">
                <div class="col-xs-12 col-sm-5 col-md-3 askbtn">
                    <a href="{{route('user.driver_profile')}}">Place your next trip</a>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-3 accprbtn">
                    <a href="{{route('home')}}">Later</a>
                </div>
            </div>
        </div>
    </div><!-- End mapview -->
</div>
<!-- End content -->

<!-- Start Footer -->
<footer>
    <div class="container">
        <div class="footin">
            <p class="col-md-8 cpyryt">© Copyright 2016. All Rights reserved.     <a href="">Disclaimer</a>   |   <a href="">Terms & Conditions</a></p>

            <ul class="col-md-4 soclicons">
                <li><label>Follow Us on:</label></li>                
                <li><a href="javascript:void(0)"><img src="{{ asset('public/images/fb.png')}}" alt=""/></a></li>
                <li><a href="javascript:void(0)"><img src="{{ asset('public/images/twt.png')}}" alt=""/></a></li>
                <li><a href="javascript:void(0)"><img src="{{ asset('public/images/gplus.png')}}" alt=""/></a></li>
                <li><a href="javascript:void(0)"><img src="{{ asset('public/images/insta.png')}}" alt=""/></a></li>
            </ul>
        </div>
    </div>
</footer>
<!-- End Footer -->
@stop
@section('footer_scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#make_payment").click(function() {
            $(".loader_div").show();
        });
    });
</script>    
@stop