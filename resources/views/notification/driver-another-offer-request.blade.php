@extends('layouts/notification')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
@stop
{{-- Page content --}}
@section('content')
<!-- Start content -->

<!-- Modal -->

<body style="background:#fff; margin:0px; padding:0px; border:0px; font-family: 'Montserrat', sans-serif;">
    <!-- begin wrapper table -->
    <table width="570" cellpadding="0" cellspacing="0" style="font-size:14px; color:#506194;margin: 0 auto; padding:50px 20px;">
        <tr>
            <td align="center">
                <table align="center" border="0" cellspacing="0" cellpadding="0">
                    <tr>   
                        <td>
                            <a href="#"><img src="{{ asset('public/images/logo.png')}}" alt=""/></a>
                        </td>
                    </tr>
                </table>
                <!-- end logo -->
                <table align="center" border="0" cellspacing="0" cellpadding="0" style="margin: 40px 0px 0px;">
                    <tr>   
                        <td style="border: 4px solid #fff;  border-radius: 50%;  height: 116px;  overflow: hidden; width: 116px;">
                            @if(!empty($userdetails->userimage))
                            <img width="144px" height="144px" src="{{ route('home')}}/public/uploads/users/{{$userdetails->userimage}}" alt=""/>
                            @else
                            <img width="100%" src="{{ asset('public/images/drvimg.png')}}" alt=""/>
                            @endif
                        </td>
                        <td style="display: inline-block; margin: 15px 30px;">
                            <h3 style="color: #312f2f;  font-size: 19px;  margin: 5px 0 3px; text-align: left; font-weight: lighter;text-align: center;">{{$userinfo->firstname . " " . $userinfo->lastname}}</h3>
                            <p style="  font-family: arial; color: #d37d06;  font-size: 14px; line-height: 20px;  margin: 0 0 10px; text-align: left; font-weight: lighter;text-align: center;">{{$userinfo->city}}</p>
                            <p style="  font-family: arial; color: #312f2f;  font-size: 16px; line-height: 20px;  margin: 0 0 10px; text-align: left; font-weight: lighter;text-align: center;">{{$userdetails->cartype_title}}</br>
                                ({{$userdetails->carmodel_title}})</p>
                        </td>
                        <td style="">
                            <img width="100%" src="{{ asset('public/images/diagrm.png')}}" alt=""/>
                        </td>
                    </tr>
                </table>
                <!-- end driver detail -->
                <table align="center" border="0" cellspacing="0" cellpadding="0" style="margin: 40px 0px 0px;">
                    <tr>   
                        <td style="display: inline-block; margin:20px 0 10px;">
                            <p style="font-family: arial; color: #312f2f;  font-size: 16px; margin: 0 0 15px; text-align: center; font-weight: lighter;">has send you a user bid 
                                <span style="color: #e18c15;font-family: arial; font-size: 20px;font-weight: lighter;">{{$currency[$items->currency_type]}} {{$another_offer->new_price}}</span>
                            </p>
                        </td>    
                    </tr>
                    <tr> 
                        <td style="display: inline-block; margin:25px 0;">
                            <label style="font-family: arial; color: #191919;  font-size: 14px; margin:0; text-align: center; font-weight: lighter;">
                                <input style="vertical-align: middle; display: inline-block; margin: -1px 7px 0px 0px;" type="checkbox" value="" id='input_agree'/> I agree to the Terms & Conditions</label>
                        </td>
                    </tr>
                </table>
                <!-- end driver cost -->
                <table align="center" border="0" cellspacing="0" cellpadding="0" style="margin:0;">
                    <tr>   
                        <td style="display: inline-block; margin:0;">
                            <a data-href="{{route('deny_driver_request', base64_encode($notification_info->request_id))}}" id='btn_deny' style="background: #131313; border-radius: 5px; display: inline-block; color: #fff; font-family: 'Montserrat', sans-serif; font-size: 18px; height: 49px; line-height: 49px;  margin: 0; text-align: center;  text-decoration: none;  text-shadow: 1px 1px 1px #a0a0a0; width: 150px;">Deny</a>
                        </td>
                        <td style="display: inline-block; margin:0 10px;">
                            <a href="{{route('home')}}" style="background: #ffaf42; border-radius: 5px; display: inline-block; color: #000000; font-family: 'Montserrat', sans-serif; font-size: 18px; height: 49px; line-height: 49px;  margin: 0; text-align: center;  text-decoration: none;  text-shadow: 1px 1px 1px #a0a0a0; width: 180px;">Contact Driver</a>
                        </td>
                        <td style="display: inline-block; margin:0;">
                            <a data-href="{{route('accept_driver_request', base64_encode($notification_info->request_id))}}" id='btn_accept' style="background: #131313; border-radius: 5px; display: inline-block; color: #fff; font-family: 'Montserrat', sans-serif; font-size: 18px; height: 49px; line-height: 49px;  margin: 0; text-align: center;  text-decoration: none;  text-shadow: 1px 1px 1px #a0a0a0; width: 150px;">Accept Bid</a>
                        </td>
                    </tr>
                </table>
                <!-- end driver buttons -->
                <table align="center" border="0" cellspacing="0" cellpadding="0" style="margin:45px 0 0;">
                    <tr style="text-align: center;">   
                        <td style="display: inline-block; margin:0 0 7px;">
                            <ul style="margin: 0px; padding: 0px; width: 210px;">
                                <li style="display:inline-block; float: left; margin: 4px 0 0;"><label style="color: #2d3235; font-size: 13px; font-weight: lighter; margin: 0;">Follow Us on:</label></li>
                                <li style="display:inline-block; float: left;"><a href="#"><img src="{{ asset('public/images/fb.png')}}" alt=""></a></li>
                                <li style="display:inline-block; float: left;"><a href="#"><img src="{{ asset('public/images/twt.png')}}" alt=""></a></li>
                                <li style="display:inline-block; float: left;"><a href="#"><img src="{{ asset('public/images/gplus.png')}}" alt=""></a></li>
                                <li style="display:inline-block; float: left;"><a href="#"><img src="{{ asset('public/images/insta.png')}}" alt=""></a></li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td style="display: inline-block; margin:0;">
                            <p style="font-family: arial; color: #85939d;  font-size: 13px; margin: 0; text-align: center; font-weight: lighter;">© Copyright 2016. All Rights reserved.
                                <a style="font-family: arial; color: #85939d;  font-size: 13px;text-decoration: none;font-weight: lighter;" href="#">Disclaimer</a>   |  <a style="font-family: arial; color: #85939d;  font-size: 13px; margin: 0;text-decoration: none; font-weight: lighter;" href="#"> Terms & Conditions</a></p>
                        </td>
                    </tr>
                </table>
                <!-- end driver buttons -->
            </td>
        </tr>
    </table>
    <!-- end wrapper table -->
</body>
<!-- End content -->
@stop
@section('footer_scripts')
<script type='text/javascript'>
    $(document).ready(function() {
        $("#btn_deny, #btn_accept").click(function() {
            if ($("#input_agree").is(":checked")) {
                window.location.href = $(this).attr('data-href');
            } else {
                alert("Please accept Term and Condition, It's mandatory");
            }
        });
    });
</script>
@stop