@extends('layouts/notification')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
@stop
{{-- Page content --}}
@section('content')
<!-- Start content -->
<!-- Modal -->

    <!-- BEGAIN PRELOADER -->
    <!-- <div id="preloader">
      <div id="status">&nbsp;</div>
    </div> -->
    <!-- END PRELOADER -->

    <!-- SCROLL TOP BUTTON -->
    <!-- <a class="scrollToTop" href="#"><i class="fa fa-long-arrow-up" aria-hidden="true"></i></a> -->
    <!-- END SCROLL TOP BUTTON -->

    <!-- Start header -->     
    <!-- End header -->
    
    <!-- Start content -->   
    <div class="container">
        <div class="col-md-12 ntcbyr wow zoomIn" data-wow-duration="1.0s" data-wow-delay="0.0s">
            <h3>{{$items->firstname . " " . $items->lastname}}</h3>  
            <h6>Deny Your Request of Item Shipping</h6>
            <h3>{{$items->product_name}}</h3>
            <h4>Thanks</h4>            
        </div><!-- End mapview -->
    </div>
        <!-- End content -->    
    <!-- Start Footer -->
    <footer>
        <div class="container">
            <div class="footin">
                <p class="col-md-8 cpyryt">© Copyright 2016. All Rights reserved.     <a href="">Disclaimer</a>   |   <a href="">Terms & Conditions</a></p>

                <ul class="col-md-4 soclicons">
                    <li><label>Follow Us on:</label></li>
                    <li><a href="javascript:void(0)"><img src="{{ asset('public/images/fb.png')}}" alt=""/></a></li>
                    <li><a href="javascript:void(0)"><img src="{{ asset('public/images/twt.png')}}" alt=""/></a></li>
                    <li><a href="javascript:void(0)"><img src="{{ asset('public/images/gplus.png')}}" alt=""/></a></li>
                    <li><a href="javascript:void(0)"><img src="{{ asset('public/images/insta.png')}}" alt=""/></a></li>
                </ul>
            </div>
        </div>
    </footer>
<!-- End content -->
@stop
@section('footer_scripts')
<!-- Write your script here -->
@stop