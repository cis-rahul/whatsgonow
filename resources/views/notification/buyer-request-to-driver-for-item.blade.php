@extends('layouts/notification')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
@stop
{{-- Page content --}}
@section('content')   
<!-- Start header --> 
<!-- End header -->

    <!-- Start content -->
    <div class="container">
        <div class="col-md-12 ntcbyr ntfdrv wow zoomIn" data-wow-duration="1.0s" data-wow-delay="0.0s">
            <h6>Please pickup</h6>
            <h3>Washine Machine</h3>
            <div class="frmtsec">
                <h3>FROM</h3>
                <h4>Andrew Goods</h4>
                <p class="lct"><img src="{{ asset('public/images/loca.png')}}" alt=""/> Alexander Platz, Berlin 10115, Germany</p>
            </div>
            <div class="frmtsec">
                <h3>Deliver to</h3>
                <h4>Jeams Hoopes</h4>
                <p class="lct"><img src="{{ asset('public/images/loca.png')}}" alt=""/> Alexander Platz, Berlin 10115, Germany</p>
            </div>
            <div class="frmtsec2">
                <p class="lct"><img src="{{ asset('public/images/calnd.png')}}" alt=""/> 30 October, 2016</p>
                <p class="lct"><img src="{{ asset('public/images/tmimg.png')}}" alt=""/> 9:00am</p>
                <h3>Price</h3>
                <h4>$ 30.00</h4>
                <a href="#">Ok</a>
            </div>
        </div><!-- End mapview -->
    </div>
    <!-- End content -->
<!-- Start Footer -->
<!-- End Footer -->
@stop
@section('footer_scripts')
<!-- Write your script here -->
@stop