@extends('layouts/notification')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
@stop
{{-- Page content --}}

@section('content')   
<!-- End header -->
<!-- Start content -->
<div class="container">    
    <div class="col-md-12 ntcbyr ntfdrv wow zoomIn" data-wow-duration="1.0s" data-wow-delay="0.0s">
        <h6 style="font-family: arial; color: #312f2f;  font-size: 16px; margin:0; text-align: center; font-weight: lighter;"><span style="font-family: 'Montserrat', sans-serif; color: #141414;  font-size: 17px; margin:0; text-align: center; font-weight: normal;">{{@$userdetails->firstname . " ". $userdetails->lastname}}</span> will pick up <span style="font-family: 'Montserrat', sans-serif; color: #141414;  font-size: 16px; margin:0; text-align: center; font-weight: normal;">{{$items->product_name}}</span></h6>
        <div class="frmtsec">
            <h3>FROM</h3>
            <h4>{{$items->seller_email}}</h4>
            <p class="lct"><img src="{{ asset('public/images/loca.png')}}" alt=""/> {{$items->from_street_number}} {{$items->from_street}}</p>
            <p class="lct" style="text-align: center;">{{$items->from_city}} {{$countries[$items->from_country]}}</p>
        </div>
        
        <div class="frmtsec">
            <h3>Deliver to</h3>
            <h4>{{$items->firstname}} {{$items->lastname}}</h4>
            <p class="lct"><img src="{{ asset('public/images/loca.png')}}" alt=""/> {{$items->to_street_number}} {{$items->to_street}}</p>
            <p class="lct" style="text-align: center;"> {{$items->to_city}} {{$countries[$items->to_country]}}</p>
        </div>        
        <div class="frmtsec2">
            <p class="lct"><img src="{{ asset('public/images/calnd.png')}}" alt=""/> {{date('d M, Y', strtotime($trips->trip_date))}}</p>
            <p class="lct"><img src="{{ asset('public/images/tmimg.png')}}" alt=""/> {{$trips->trip_time}}</p>
            <h4 style="color: #312f2f;  font-family: 'Montserrat', sans-serif;  font-size: 17px; margin:18px 0 9px; font-weight: normal;">Agreed Commission</h4>
            <p style="  font-family: arial; font-weight: lighter;color: #e18c15; font-size:40px; margin: 0;">{{$currency[$items->currency_type]}} {{!empty($another_offer->new_price)?$another_offer->new_price:$items->item_price}}</p>
            <a id="make_payment" href="{{route('make_payment', $request_id)}}">Make Payment</a>
        </div>        
    </div><!-- End mapview -->
</div>
<!-- End content -->

<!-- Start Footer -->
<footer>
    <div class="container">
        <div class="footin">
            <p class="col-md-8 cpyryt">© Copyright 2016. All Rights reserved.     <a href="">Disclaimer</a>   |   <a href="">Terms & Conditions</a></p>

            <ul class="col-md-4 soclicons">
                <li><label>Follow Us on:</label></li>                
                <li><a href="javascript:void(0)"><img src="{{ asset('public/images/fb.png')}}" alt=""/></a></li>
                <li><a href="javascript:void(0)"><img src="{{ asset('public/images/twt.png')}}" alt=""/></a></li>
                <li><a href="javascript:void(0)"><img src="{{ asset('public/images/gplus.png')}}" alt=""/></a></li>
                <li><a href="javascript:void(0)"><img src="{{ asset('public/images/insta.png')}}" alt=""/></a></li>
            </ul>
        </div>
    </div>
</footer>
<!-- End Footer -->
@stop
@section('footer_scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $("#make_payment").click(function(){
           $(".loader_div").show(); 
        });        
    });
</script>    
@stop