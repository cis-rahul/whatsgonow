@extends('layouts/notification')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
@stop
{{-- Page content --}}

@section('content')   
<!-- End header -->
<!-- Start content -->
<div class="container">
    <div class="col-md-12 ntcbyr wow zoomIn" data-wow-duration="1.0s" data-wow-delay="0.0s">
        <h6>Your Item</h6>
        <h3>{{$items->product_name}}</h3>
        <p>has been picked up now and is on the way to you</p>
        <h4>Estimated arrival time</h4>
        <p class="tim"><img src="{{ asset('public/images/tmimg.png')}}" alt=""/> 9:00</p>
    </div><!-- End mapview -->
</div>
<!-- End content -->

<!-- Start Footer -->
<footer>
    <div class="container">
        <div class="footin">
            <p class="col-md-8 cpyryt">© Copyright 2016. All Rights reserved.     <a href="">Disclaimer</a>   |   <a href="">Terms & Conditions</a></p>

            <ul class="col-md-4 soclicons">
                <li><label>Follow Us on:</label></li>                
                <li><a href="javascript:void(0)"><img src="{{ asset('public/images/fb.png')}}" alt=""/></a></li>
                <li><a href="javascript:void(0)"><img src="{{ asset('public/images/twt.png')}}" alt=""/></a></li>
                <li><a href="javascript:void(0)"><img src="{{ asset('public/images/gplus.png')}}" alt=""/></a></li>
                <li><a href="javascript:void(0)"><img src="{{ asset('public/images/insta.png')}}" alt=""/></a></li>
            </ul>
        </div>
    </div>
</footer>
<!-- End Footer -->
@stop
@section('footer_scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#make_payment").click(function() {
            $(".loader_div").show();
        });
    });
</script>    
@stop