@extends('layouts/master')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

@stop

{{-- Page content --}}
@section('content')
<style type="text/css">
    .heading{list-style-type:circle;text-align: left;}
    .content{list-style-type:square;text-align: left;}
</style> 
<section class="abotsection">
    <div class="col-lg-10 col-md-11 col-sm-12 abutin wow flipInX" data-wow-duration="1.5s" data-wow-delay="0.8s" style="padding: 3% 0 5% 7%;">
        <h3 class="heading">{{ trans('message.AboutHeading') }}</h3>
        <ul class="content">
            <li>{{ trans('message.PainOne') }}</li>
            <li>{{ trans('message.PainTwo') }}</li>
            
        </ul>

        <h4 class="heading">{{ trans('message.Gain') }}</h4>
        <ul class="content">
            <li>{{ trans('message.GainOne') }}</li>
            <li>{{ trans('message.GainTwo') }}</li>
            <li>{{ trans('message.GainThree') }}</li>
            <li>{{ trans('message.GainFour') }}</li>
            <li>{{ trans('message.GainFive') }}</li>
            <li>{{ trans('message.GainSix') }}</li>
            <li>{{ trans('message.GainSeven') }}</li>
        </ul>

        <h4 class="heading">{{ trans('message.Product') }}</h4>
        <ul class="content">
            <li>{{ trans('message.ProductOne') }}</li>
            <li>{{ trans('message.ProductTwo') }}</li>
            <li>{{ trans('message.ProductThree') }}</li>
            <li>{{ trans('message.ProductFour') }}</li>
            <li>{{ trans('message.ProductFive') }}</li>
            <li>{{ trans('message.ProductSix') }}</li>
        </ul>

    </div><!-- End abotsection -->



</section>
@stop
@section('footer_scripts')
<!-- Write your script here -->
@stop