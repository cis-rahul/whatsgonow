@extends('layouts/home')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
<style type='text/css'>
    .accprbtn button {background: #ffaf42;border-radius: 5px;color: #000;display: inline-block;font-family: "montserratregular";font-size: 18px;height: 49px;line-height: 49px;margin: 0;text-decoration: none;text-shadow: 1px 1px 1px #a0a0a0;width: 100%;text-align: center;}
    .has-error .form-control {border-color: #a94442 !important;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);}
</style>
@stop
{{-- Page content --}}
@section('content')
<!-- Start content -->
<div class="container">
    <div class="col-md-12 wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0s">
        <div class="title-area">
            <h2 class="title">Registration for Drivers</h2>
            <h4 class="inftitl">Information not Public</h4>
            <span class="line"></span>
        </div>
    </div>
    <div class="regfrm">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if (session('flash_alert_notice'))
        <div class="flash-message">
            <div class="alert alert-{{session('flash_action')}}">
                <p>{{session('flash_alert_notice')}}</p>
            </div>
        </div>
        @endif
        <form action="{{route('user.driver_registration')}}" method="POST" role="form" id="frm_dregistration" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" name="role" value="2" />
            <div class="col-md-12 col-lg-9 lftbar rgfrmin wow zoomIn" data-wow-duration="0.8s" data-wow-delay="0.5s">
                <div class="col-xs-12 text-center">
                    <div class="uplsec">
                        <span><img style="width:135px;height:135px;" src="{{ asset('public/images/usrimg.png') }}" alt=""/></span>
                        <div class="uplbtnmn">
                            <div class="uplbtn">
                                <input type="file" value="" id="userimage" name="userimage" class='validate[required]'/>
                                <img id="uploadimg" src="{{ asset('public/images/uplbtn1.png') }}" alt=""/>
                            </div>
                        </div>
                    </div>                                       
                </div><!--upload image-->
                <div class="col-md-6 form-group {{ $errors->first('firstname', 'has-error')}}">
                    <label>First Name</label>
                    <input type="text" class="form-control required"  id='firstname' name='firstname' placeholder="Enter First Name" value='{{(!empty($userinfo['firstname']))?$userinfo['firstname']:Input::old('firstname')}}'>
                </div>
                <div class="col-md-6">
                    <div class="col-xs-6 padd-left form-group {{ $errors->first('cartype', 'has-error')}}">
                        <label>Car Type</label>
                        <select class="form-control required" id='cartype' name='cartype'  >
                            @if(!empty($cartype))
                            <option selected="" value=''>Please Select</option>
                            @foreach($cartype as $objCarType)
                            @if($objCarType->id != "9")
                            @if(@Input::old('cartype') == @$objCarType->id || @$userinfo['cartype'] == @$objCarType->id)
                            <option value='{{$objCarType->id}}' selected='selected'>{{$objCarType->type}}</option>
                            @else
                            <option value='{{$objCarType->id}}'>{{$objCarType->type}}</option>
                            @endif
                            @endif    
                            @endforeach
                            @endif                            
                        </select>
                    </div>	
                    <div class="col-xs-6 padd form-group {{ $errors->first('carmodel', 'has-error')}}">
                        <label>Car Model</label>
                        <div class="ctyslct">
                            <select id="carmodel" name="carmodel" class="form-control required">
                                <option value="">Please Select</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 padd">
                    <div class="col-sm-12 form-group {{ $errors->first('lastname', 'has-error')}}">
                        <label>Sur Name</label>
                        <input type="text" class="form-control required" id='lastname' name='lastname' placeholder="Enter Last Name" value='{{(!empty($userinfo['lastname']))?$userinfo['lastname']:Input::old('lastname')}}'>
                    </div>
                    <div class="col-md-12 form-group {{ $errors->first('email', 'has-error')}}">
                        <label>Email</label>
                        <span style='line-height: 35px;' type="text" class="form-control required" id='email' name='email'>{{(!empty($userinfo['email']))?$userinfo['email']:Input::old('email')}}</sapn>
                    </div>    
                </div>
                <div class="col-md-6 form-group {{ $errors->first('cardesc', 'has-error')}}">
                    <label>Car Description</label>
                    <textarea name="cardesc" id="cardesc" class="form-control" placeholder="Enter Car Description">{{(!empty($userinfo['cardesc']))?$userinfo['cardesc']:Input::old('cardesc')}}</textarea>
                </div>
                <p class="col-md-6">Following information will not be made public without your permission</p>
                <p class="col-md-6">Following information will not be made public without your permission</p>
                <div class="col-md-6">
                    <div class="col-xs-8 padd-left form-group {{ $errors->first('street', 'has-error')}}">
                        <label>Street</label>
                        <input type="text" name='street' id='street' class="form-control required" placeholder="Enter Street" value='{{(!empty($userinfo['street']))?$userinfo['street']:Input::old('street')}}'>
                    </div>	
                    <div class="col-xs-4 padd form-group {{ $errors->first('housenumber', 'has-error')}}">
                        <label>No</label>
                        <input type="text" class="form-control required" id='housenumber' name='housenumber' placeholder="Enter Your House Number" value='{{(!empty($userinfo['housenumber']))?$userinfo['housenumber']:Input::old('housenumber')}}'>
                    </div>
                </div>
                <div class="col-md-6 form-group {{ $errors->first('licenceno', 'has-error')}}">
                    <label>Driving Licence No</label>
                    <input type="text" name='licenceno' id='licenceno' class="form-control required" placeholder="Enter Licence Number" value='{{(!empty($userinfo['licenceno']))?$userinfo['licenceno']:Input::old('licenceno')}}'>
                </div>
                <div class="col-md-6 form-group {{ $errors->first('dob', 'has-error')}}">
                    <label>Date of Birth</label>                    
                    <input id="dob" name="dob" type="text" class="form-control jquery-datepicker-dob required" placeholder="yyyy-mm-dd" value='{{(!empty($userinfo['dob']))?$userinfo['dob']:Input::old('dob')}}'>
                    <a class="calicn" href="javascript:void(0)"><i class="fa fa-calendar" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-6 form-group {{ $errors->first('licenceissueplace', 'has-error')}}">
                    <label>Licence Issue Place</label>
                    <input type="text" class="form-control required" id='licenceissueplace' name='licenceissueplace'  placeholder="Licence Issue Place" value='{{(!empty($userinfo['licenceissueplace']))?$userinfo['licenceissueplace']:Input::old('licenceissueplace')}}'>
                </div>
                <div class="col-md-6 form-group {{ $errors->first('postalcode', 'has-error')}}">
                    <label>Postal Code</label>
                    <input type="text" class="form-control required numeric" id='postalcode' name='postalcode' placeholder="Enter Postal Code" value='{{(!empty($userinfo['postalcode']))?$userinfo['postalcode']:Input::old('postalcode')}}'>
                </div>
                <div class="col-md-6 form-group {{ $errors->first('licenceissuedt', 'has-error')}}">
                    <label>Licence Issue Date</label>
                    <input id="licenceissuedt" name="licenceissuedt" type="text" class="form-control jquery-datepicker-dob required" placeholder="Select Date" value='{{(!empty($userinfo['licenceissuedt']))?$userinfo['licenceissuedt']:Input::old('licenceissuedt')}}'>                    
                    <a class="calicn" href="#"><i class="fa fa-calendar" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-6 form-group {{ $errors->first('city', 'has-error')}}">
                    <label>City</label>
                    <input type="text" class="form-control required" id='city' name='city'  placeholder="Enter City" value='{{(!empty($userinfo['city']))?$userinfo['city']:Input::old('city')}}'>
                </div>
                <div class="col-md-6 form-group {{ $errors->first('identificationmethod', 'has-error')}}">
                    <label>Identification Method</label>
                    <div class="ctyslct">
                        <select class="form-control required" id="identificationmethod" name="identificationmethod">
                            <option value=''>Please Select</option>
                            <option value="passport" <?php echo (Input::old('identificationmethod') == "passport") ? "selected='selected'" : ""; ?>>Passport</option>
                            <option value="id-card" <?php echo (Input::old('identificationmethod') == "id-card") ? "selected='selected'" : ""; ?>>ID-Card</option>
                            <option value="dl" <?php echo (Input::old('identificationmethod') == "dl") ? "selected='selected'" : ""; ?>>Driving License</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 form-group {{ $errors->first('country', 'has-error')}}">
                    <label>Country</label>
                    <div class="ctyslct">
                        <select class="form-control required" id='country' name='country'  >
                            @if(!empty($countries))
                            <option selected="" value=''>Select Country</option>
                            @foreach($countries as $key => $objCountry)                           
                            @if(@Input::old('country') == @$key || @$userinfo['country'] == @$key)
                            <option value='{{$key}}' selected='selected'>{{$objCountry}}</option>
                            @else
                            <option value='{{$key}}'>{{$objCountry}}</option>
                            @endif
                            @endforeach
                            @endif                            
                        </select>
                    </div>
                </div>
                <div class="col-md-6 form-group {{ $errors->first('paypal_email_id', 'has-error')}}">
                    <label>Get Paid</label>
                    <div class="paygtw row">
                        <div class="col-md-4">
                            <a class="padd" href="javascript:void(0)"><img class="img-responsive" src="{{ asset('public/images/payplicn.jpg')}}" alt=""/></a>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control required email" id='paypal_email_id' name='paypal_email_id'  placeholder="Paypal Email ID" value='{{(!empty($userinfo['paypal_email_id']))?$userinfo['paypal_email_id']:Input::old('paypal_email_id')}}'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 form-group agrytrms">
                    <label class="custom-control custom-checkbox">
                        <?php $termandcare = ""; ?>
                        @if(Input::old('checkbox'))
                        <?php $termandcare = "checked='checked'"; ?>                            
                        @endif
                        <input value="1" name="checkbox" id="checkbox" class="custom-control-input validate[required]" type="checkbox" <?php echo $termandcare ?>>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">I agree to the Terms & Conditions</span>
                    </label>
                </div>
                <div class="col-md-6 accprbtn">
                    <a href="#" data-toggle="modal" id="btn_driver_registration">Save</a>
                </div>
            </div><!-- End left panel -->
        </form>
    </div>
</div>
<!-- End content -->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="edit" data-size="16" data-loop="true" data-c="#fff" data-hc="white" id="livicon-47" style="width: 16px; height: 16px;"></i>
                            Warning
                        </h3>                        
                    </div>
                    <div class="panel-body">
                        <p style="text-align:left;">{{ trans('message.WarningImage') }}</p>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-12 col-md-6" style="height: 54px;">
                            <button style="width:172px;" type="submit" id="btn_accept" name="btn_add_trip" class="btn btn-default">Accept</button>
                        </div>
                        <div class="col-xs-6 col-sm-12 col-md-6" style="height: 54px;">
                            <button style="width:172px;" type="button" class="btn btn-default cancel_confirmation">Cancel</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>    
    </div><!-- Modal End-->
</div>
<!-- End content -->
@stop
@section('footer_scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_accept").click(function(){
            $("#myModal").modal('toggle');
            $("#uploadimg").hide();
        });
        $(".cancel_confirmation").click(function(){           
            window.location.href = "{{route('user.profile')}}";
        });
        $("#btn_driver_registration").click(function() {
            if ($("#checkbox").is(":checked")) {
                $("#frm_dregistration").submit();
            } else {
                alert("Please accept Term & Condition, It's Mandatory");
            }
        });
        $("#cartype").change(function() {
            $(".loader_div").show();
            var url = "{{route('carmodel')}}";
            $.ajax({
                url: url,
                data: ({cartypeid: $(this).val(), _token: '{{csrf_token()}}'}),
                type: 'POST',
                dataType: 'JSON',
                success: function(res) {
                    var html = "<option value=''>Please Select</option>";
                    ;
                    if (res['status']) {
                        console.log(res['data']);
                        $.each(res['data'], function(key, val) {
                            html += "<option value='" + val['id'] + "'>" + val['model'] + "</option>";
                        });
                    }
                    $("#carmodel").html(html);
                    setTimeout(function(){
                        $(".loader_div").hide();
                    },1000);
                },
                error: function(res) {
                    alert('System Error');
                }
            });
        });
        //$("#frm_dregistration").validationEngine({maxErrorsPerField: 1});
    });
    $('body').on('change', 'input[type="file"]', function(event) {
        var dd = $(this).parents('.uplsec').parent();
        var src = URL.createObjectURL(event.target.files[0]);
        dd.find('span').find('img').attr('src', src);
        $("#myModal").modal({backdrop: 'static', keyboard: false});
    });
</script>
<!-- Write your script here -->
@stop