@extends('layouts/home')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
<link href="{{ asset('public/css/datepicker.css') }}" rel="stylesheet">
<link href="{{ asset('public/css/bootstrap-timepicker.min.css') }}" rel="stylesheet">
<style type='text/css'>
    .driv_prof {  border-radius: 20%;  height:50px;}
</style>
@stop
{{-- Page content --}}
@section('content')
<!-- Start content -->
<!-- begin wrapper table -->
<table width="570" cellpadding="0" cellspacing="0" style="font-size:14px; color:#506194;margin: 0 auto; padding:50px 20px;">
    <tr>
        <td align="center">            
            <table align="center" border="0" cellspacing="0" cellpadding="0">
                <tr>   
                    <td>
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        @if (session('flash_alert_notice'))
                        <div class="flash-message">
                            <div class="alert alert-{{session('flash_action')}}">
                                <p>{{session('flash_alert_notice')}}</p>
                            </div>
                        </div>
                        @endif
                    </td>
                </tr>
            </table>
            <table align="center" border="0" cellspacing="0" cellpadding="0">
                <tr>   
                    <td>
                        <a href="javascript:void(0)"><img src="{{ asset('public/images/logo.png') }}" alt=""/></a>
                    </td>
                </tr>
            </table>
            <!-- end logo -->
            <table align="center" border="0" cellspacing="0" cellpadding="0" style="margin: 40px 0px 0px;">
                <tr>   
                    <td style="border: 4px solid #fff;  border-radius: 50%;  height: 116px;  overflow: hidden; width: 155px;">
                        <?php
                        $profile_pic = '';
                        if (isset($driverDetails->userimage) && !empty($driverDetails->userimage)) {
                            $profile_pic = 'public/uploads/users/' . $driverDetails->userimage;
                        } else {
                            $profile_pic = 'public/uploads/users/itmimg.png';
                        }
                        ?>
                        <img width="100%" height="128%" src="{{ asset($profile_pic) }}" alt=""/>
                    </td>
                    <td style="display: inline-block; margin: 15px 30px;">
                        <h3 style="color: #312f2f;  font-size: 19px;  margin: 5px 0 3px; text-align: center; font-weight: lighter;">{{ isset($driverDetails->country) ? ucfirst($driverDetails->firstname) : '' }}</h3>
                        <p style="  font-family: arial; color: #d37d06;  font-size: 14px; line-height: 20px;  margin: 0 0 10px; text-align: center; font-weight: lighter;">{{isset($driverDetails->country) ? $countries[$driverDetails->country] : '' }}</p>
                        <p style="  font-family: arial; color: #312f2f;  font-size: 16px; line-height: 20px;  margin: 0 0 10px; text-align: center; font-weight: lighter;">{{isset($driverDetails->cartype) ? $cartypes[$driverDetails->cartype] : '' }}</br>
                            {{ isset($driverDetails->city) ? ucfirst($driverDetails->city) : '' }}
                        </p>
                    </td>
                    <td style="">
                        <img width="100%" src="{{ asset('public/images/diagrm.png') }}" alt=""/>
                    </td>
                </tr>
            </table>
            <!-- end driver detail -->
            <table align="center" border="0" cellspacing="0" cellpadding="0" style="margin: 80px 0px 50px;">
                <tr>
                    <td style="display: inline-block; margin:0 10px;">
                        @if(Auth::check())
                        <a href="javascript:void(0)" id='my_modal' style="background: #ffaf42; border-radius: 5px; display: inline-block; color: #000000; font-family: 'Montserrat', sans-serif; font-size: 18px; height: 49px; line-height: 49px;  margin: 0; text-align: center;  text-decoration: none;  text-shadow: 1px 1px 1px #a0a0a0; width: 180px;">Contact Driver</a>
                        @else
                        <a href="{{route('user.signin')}}" style="background: #ffaf42; border-radius: 5px; display: inline-block; color: #000000; font-family: 'Montserrat', sans-serif; font-size: 18px; height: 49px; line-height: 49px;  margin: 0; text-align: center;  text-decoration: none;  text-shadow: 1px 1px 1px #a0a0a0; width: 180px;">Contact Driver</a>
                        @endif
                    </td>
                </tr>
            </table>
            <!-- end driver buttons -->
            <!-- end driver buttons -->
        </td>
    </tr>
</table>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div style="margin: 15px,15px,15px,15px;">                
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="edit" data-size="16" data-loop="true" data-c="#fff" data-hc="white" id="livicon-47" style="width: 16px; height: 16px;"></i>
                            Item List
                        </h3>                        
                    </div>
                    <div class="panel-body">
                        <form action="{{route('buyer_request_driver')}}" method="POST" id="frm_buyer_request_driver">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            <input type="hidden" name="driver_id" value="{{$driverDetails->id}}"/>
                            <input type="hidden" name="trip_id" value="1"/>
                            <div class="table-responsive text-center bypftbl">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>                                    
                                            <th>Item Name</th>
                                            <th>Posted Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>                            

                                    </tbody>
                                </table>
                            </div>
                        </form>  
                    </div>
                </div>                  
            </div>
        </div>    
    </div><!-- Modal End-->
</div>
<!-- End content -->
<!-- end wrapper table -->
<!-- End content -->
@stop
@section('footer_scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var user_id = "<?php echo $driverDetails->id ?>";
        $("body").on("click", ".btn_submit_buyer_request_driver", function() {
            var flag = false;
            $(".itemClass").each(function(key, val) {
                if ($(this).is(":checked")) {
                    flag = true
                }
            });
            if (flag) {
                $("#frm_buyer_request_driver").submit();
            } else {
                alert("Please select any Item, It's Mandatory");
            }
        });
        $('#my_modal').click(function() {
            $.ajax({
                url: "{{route('user_items')}}",
                dataType: 'JSON',
                type: 'POST',
                data: ({user_id: user_id, _token: "{{csrf_token()}}"}),
                success: function(res) {
                    if (res['status'] == true) {
                        var rows = "";
                        $.each(res['data'], function(key, val) {
                            rows += "<tr>";
                            rows += "<td><input type='radio' value='" + val['id'] + "' name='item_id' class='itemClass'/></td>";
                            rows += "<td>" + val['product_name'] + "</td>";
                            rows += "<td>" + val['posted_date'] + "</td>";
                            rows += "</tr>";
                        });
                        rows += "<tr>";
                        rows += "<td colspan='3'><input class='btn btn-info form-control btn_submit_buyer_request_driver' type='button' value='Submit'/></td>";
                        rows += "</tr>";
                        $('#myModal tbody').html(rows);
                        $("#myModal").modal({backdrop: 'static', keyboard: false});
                    } else {
                        rows += "<tr>";
                        rows += "<td colspan='3'>No Item Found!</td>";
                        rows += "</tr>";
                        $('#myModal tbody').html(rows);
                        $("#myModal").modal();
                    }

                },
                error: function(res) {
                    alert("Network Error!");
                }
            });
        });
        $(".emntfctn").mouseover(function() {
            $(".ntfdtinf").css("display", "block");
        });
        $(".emntfctn").mouseout(function() {
            $(".ntfdtinf").css("display", "none");
        });
        var s = $("header");
        var pos = s.position();
        $(window).scroll(function() {
            var windowpos = $(window).scrollTop();
            if (windowpos >= pos.top & windowpos <= 10) {
                s.removeClass("stick");
            } else {
                s.addClass("stick");
            }
        });
//        var popup = "<?php echo session('popup') ?>";
//        var error = "<?php echo count($errors->all()); ?>";
//        if (error != 0 && popup == true) {
//            $('#my_modal').click();
//        }
//        var error = "<?php echo session('flash_alert_notice'); ?>";
//        if (error != "" && popup == true) {
//            $('#my_modal').click();
//        }
    });
</script>
<!-- Write your script here -->
@stop