@extends('layouts/master')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

@stop

{{-- Page content --}}
@section('content')
<style type="text/css">
    .abutin{text-align: left !important; padding: 4%; font-size: 14px;}
</style>
    <section class="abotsection">
    <div class="col-lg-10 col-md-11 col-sm-12 abutin wow flipInX" data-wow-duration="1.5s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 1.5s; animation-delay: 0.8s; animation-name: flipInX;">
                <div class="row">
                    <div id="c13" class="frame default">
                        <div class="row">
                            <div class="col-md-12">
                            </div>
                            <div class="col-md-12">
                                  
                                <p><strong>Legal Information</strong>
                                </p>
                                <p>BaSSCo Bavarian Software Solution Company UG&nbsp;Haftungsbeschränkt
                                </p>
                                <p>Trade licence Number HRB 226781
                                </p>
                                <p>Address
                                </p>
                                <p>Emil-Riedel-Strasse 11
                                </p>
                                <p>80538 Munich, Germany
                                </p>
                                <p>Fon.&nbsp;0049 (0) 160 89 62 401&nbsp;
                                </p>
                                <p>Email: pirate(at)pirate-app.com
                                </p>
                                <p><br> Legal Status: Private held company<br> Registered in&nbsp;Munich - Germany
                                </p>
                                <p>&nbsp;</p>
                                <p><a href="http://www.pirate-app.com/" target="_blank"><strong>Disclaimer</strong></a><strong></strong>
                                </p>
                                <p><strong><em>1. Content</em></strong><em>&nbsp;</em><em><br> </em>The author reserves the right not to be responsible for the topicality, correctness, completeness or quality of the information provided. Liability claims regarding damage caused by the use of any information provided, including any kind of information, which is incomplete or incorrect, will therefore be rejected.&nbsp;<br> All offers are not binding and without obligation. Parts of the pages or the complete publication including all offers and information might be extended, changed or partly or completely deleted by the author without separate announcement.
                                </p>
                                <p><strong><em>2. Referrals and links</em></strong><em>&nbsp;</em><em><br> </em>The author is not responsible for any contents linked or referred to from his pages - unless he has full knowledge of illegal contents and would be able to prevent the visitors of his site from viewing those pages. If any damage occurs by the use of information presented there, only the author of the respective pages might be liable, not the one who has linked to these pages. Furthermore the author is not liable for any postings or messages published by users of discussion boards, guest books or mailing lists provided on his page.
                                </p>
                                <p><strong><em>3. Copyright</em></strong><em>&nbsp;</em><em><br> </em>The author intended not to use any copyrighted material for the publication or, if not possible, to indicate the copyright of the respective object.&nbsp;<br> The copyright for any material created by the author is reserved. Any duplication or use of objects such as images, diagrams, sounds or texts in other electronic or printed publications is not permitted without the author's agreement.
                                </p>
                                <p><strong><em>4. Privacy policy</em></strong><em>&nbsp;</em><em><br> </em>If the opportunity for the input of personal or business data (email addresses, name, addresses) is given, the input of these data takes place voluntarily. The use and payment of all offered services are permitted - if and so far technically possible and reasonable - without specification of any personal data or under specification of anonymized data or an alias. The use of published postal addresses, telephone or fax numbers and email addresses for marketing purposes is prohibited, offenders sending unwanted spam messages will be punished.
                                </p>
                                <p><strong><em>5. Legal validity of this disclaimer</em></strong><em>&nbsp;</em><em><br> </em>This disclaimer is to be regarded as part of the Internet publication which you were referred from. If sections or individual terms of this statement are not legal or correct, the content or validity of the other parts remain uninfluenced by this fact.</p>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
    
    
     
</section>


@stop
@section('footer_scripts')
 
@stop
