@extends('layouts/login')

{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
@stop

{{-- Page content --}}
@section('content')

<div class="col-lg-5 col-md-6 col-sm-8 col-xs-12 sgin wow zoomIn" data-wow-duration="1.0s" data-wow-delay="1.0s">
    <div class="row">        
        <form action="{{route('user.signin')}}" method="POST" role="form" id="frm_login">                        
            <h5>Login details</h5>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session('flash_alert_notice'))
            <div class="flash-message">
                <div class="alert alert-{{session('flash_action')}}">
                    <p>{{session('flash_alert_notice')}}</p>
                </div>
            </div>
            @endif
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <div class="col-xs-12 form-group {{ $errors->first('username', 'has-error')}}">
                <span><img src="{{ asset('public/images/user.png') }}" alt=""/></span>
                <input class="form-control" placeholder="User Name & Email" id="username" name="username" type="text">                
            </div>
            <div class="col-xs-12 form-group {{ $errors->first('password', 'has-error')}}">
                <span><img src="{{ asset('public/images/paswrd.png') }}" alt=""/></span>
                <input class="form-control" placeholder="Password" type="password" id="password" name="password">                
            </div>
            <div class="col-xs-12 form-group">
                <button type='submit' class="btn btn-primary btn-lg btn-block sgupbtn" id="btn_login" name="btn_login">Login</a>
            </div>
            <a class="frgtpswd" href="{{route('user.forgotpassword')}}">Forgot Password</a>
            <p class="col-xs-12 lgnsup">New Member? <a href="{{route('user.signup')}}">Sign Up Now</a></p>
        </form>
    </div>
</div><!-- End Services -->
@stop

@section('footer_scripts')
<!-- Write your script here -->
<script type="text/javascript">
    $(document).ready(function() {
//        $("#btn_login").click(function() {
//            $("#frm_login").submit();
//        });
    });
</script>
@stop