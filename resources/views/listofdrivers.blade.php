@extends('layouts/home')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
<link href="{{ asset('public/css/datepicker.css') }}" rel="stylesheet">
<link href="{{ asset('public/css/bootstrap-timepicker.min.css') }}" rel="stylesheet">
<style type='text/css'>
    .driv_prof {  border-radius: 20%;  height:50px;}
    .pager li > a, .pager li > span {width: 100px;height: 41px;border-radius:5px;text-align:center;margin-top:-8px;padding-top:9px;}
    .pager {text-align: right; margin-right: 15px;}
</style>
@stop
{{-- Page content --}}
@section('content')
<!-- Start content -->
<section id="">
    <!-- Start content -->
    <div class="container">
        <div class="col-md-12 wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0s">
            <div class="title-area">
                <h2 class="title">{{ trans('message.DriverListHeading') }}</h2>
                <span class="line"></span>
            </div>
        </div>
        <div class="col-md-12 lstfltxt lstdrvtb">
            <div class="col-sm-12 rgtbarnw wow zoomIn" data-wow-duration="0.8s" data-wow-delay="0s">
                <div class="row">
                    <div class="table-responsive text-center bypftbl">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>{{ trans('message.PICTURE') }}</th>
                                    <th>{{ trans('message.FirstName') }}</th>
                                    <th>{{ trans('message.LastName') }}</th>
                                    <th>{{ trans('message.TYPEOFCAR') }}</th>
                                    <th>{{ trans('message.COMPLETEDTRIPS') }}</th>
                                </tr>
                            </thead>

                            <tbody>
                                @if(isset($drivers) && !empty($drivers))
                                @foreach($drivers as $row)
                                <tr>
                                    <td class="itmimgtd">
                                        <?php $profile_pic = isset($row->userimage) && !empty($row->userimage) ? $row->userimage : 'itmimg.png'; ?>                                                                  
                                        <a href="{{ route('user.driver-detail', $row->id) }}"><span class="itmimg"><img class="driv_prof" src="public/uploads/users/{{$profile_pic}}" alt="{{ $row->firstname }}"/></span></a>                                        
                                    </td>
                                    <td><a href="javascript:void(0)">{{ ucfirst($row->firstname) }}</a></td>
                                    <td><a href="javascript:void(0)">{{ ucfirst($row->lastname) }}</a></td>
                                    <td>{{ ucfirst($row->type) }}</td>
                                    <td>{{ ucfirst($row->tripCount) }}</td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="5">{{ trans('message.NoRecordAvailable') }}</td>
                                </tr>
                                @endif
                            </tbody>

                        </table>
                    </div>
                    <div class = "fltbtns">
                        <div class = "row">
                            <div class = "col-xs-12 col-sm-3 accprbtn">
                                &nbsp;
                            </div>                            
                            <div class = "col-sm-7 col-xs-12 nxbcbtn">                                
                                <?php echo $drivers->appends(Input::all())->render() ?>
                            </div>
                        </div>
                    </div>
                </div><!-- End row -->
            </div><!-- End right panel -->
        </div>
    </div>
    <!-- End content -->

</section>   
<!-- End content -->
@stop
@section('footer_scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("li a[rel='prev']").html("Back").css('background-color', '#000').css('color', '#ffffff').css('font-weight', 'bold');
        $("li a[rel='next']").html("Next").css('background-color', '#ffaf42').css('color', '#ffffff').css('font-weight', 'bold');
        $("li.disabled span").hide();
    });
</script>
<!-- Write your script here -->
@stop