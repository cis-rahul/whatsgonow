<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Whatsgonow</title>
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/icon" href="images/logo.png') }}"/>
        <!-- Font Awesome -->
        <link href="{{ asset('public/css/font-awesome.css') }}" rel="stylesheet" type="text/css" >
        <!-- Bootstrap -->
        <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" >  
        <link href="{{ asset('public/css/bootstrap.css') }}" rel="stylesheet" type="text/css" >    
        <!-- Slick slider -->
        <link href="{{ asset('public/css/slick.css') }}" rel="stylesheet" type="text/css" /> 
        <!-- Fancybox slider -->
        <link href="{{ asset('public/css/jquery.fancybox.css') }}" type="text/css" media="screen" /> 
        <!-- Animate css -->
        <link href="{{ asset('public/css/animate.css') }}" rel="stylesheet" type="text/css" /> 
        <!-- Progress bar  -->
        <link href="{{ asset('public/css/bootstrap-progressbar-3.3.4.css') }}" rel="stylesheet" type="text/css" /> 
        <!-- Theme color -->
        <link href="{{ asset('public/css/default-theme.css') }}" rel="stylesheet" type="text/css" >
        <!-- Main Style -->
        <link href="{{ asset('public/css/style.css') }}" rel="stylesheet" type="text/css" >        
        <style type="text/css">
            a,button { cursor: pointer; cursor: hand; }
        </style>
        @yield('header_styles')
    </head>
    <body id="login">        
        <div class="container">
            <div class="col-xs-12 text-center header wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.0s">
                <!-- LOGO --> 
                <a class="logo3" href="{{route('home')}}"><img src="{{ asset('public/images/logo.png') }}" alt="logo"></a>             
                <!-- LOGO  End-->
            </div>
        </div> 
        <!-- END container --> 
        <!-- End header -->
        <!-- Start content -->
        <div class="container">
            @yield('content')
        </div>
        <!-- End content -->
        <!-- Start Footer -->
        <footer>
            <div class="container">
                <div class="footin">
                    <p class="col-md-8 cpyryt">{{ trans('message.COPYRITE') }}   <a href="">{{ trans('message.DISCLAIMER') }} </a>   |   <a href="">{{ trans('message.TERMS') }} </a></p>
                    <ul class="col-md-4 soclicons">
                        <li><label>{{ trans('message.FOLLOW') }} </label></li>
                        <li><a href="javascript:void(0)"><img src="{{ asset('public/images/fb.png')}}" alt=""/></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ asset('public/images/twt.png')}}" alt=""/></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ asset('public/images/gplus.png')}}" alt=""/></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ asset('public/images/insta.png')}}" alt=""/></a></li>
                    </ul>
                </div>
            </div>
        </footer>
        <!-- End Footer -->
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>    
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <!-- Bootstrap -->
        <script src="{{ asset('public/js/bootstrap.js') }}"></script>
        <!-- Slick Slider -->
        <script type="text/javascript" src="{{ asset('public/js/slick.js') }}"></script>    
        <!-- mixit slider -->
        <script type="text/javascript" src="{{ asset('public/js/jquery.mixitup.js') }}"></script>
        <!-- Add fancyBox -->        
        <script type="text/javascript" src="{{ asset('public/js/jquery.fancybox.pack.js') }}"></script>
        <!-- counter -->
        <script src="{{ asset('public/js/waypoints.js') }}"></script>
        <script src="{{ asset('public/js/jquery.counterup.js') }}"></script>
        <!-- Wow animation -->
        <script type="text/javascript" src="{{ asset('public/js/wow.js') }}"></script> 
        <!-- progress bar   -->
        <script type="text/javascript" src="{{ asset('public/js/bootstrap-progressbar.js') }}"></script>
        <!-- Custom js -->
        <script type="text/javascript" src="{{ asset('public/js/custom.js') }}"></script>
        <script type='text/javascript'>
            $(document).ready(function(){
                $(".counrties").change(function(){
                    alert("Hi");
                });
            });           
        </script>
        @yield('footer_scripts')
        
    </body>
</html>