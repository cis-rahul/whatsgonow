<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Whatsgonow</title>
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/icon" href="images/logo.png') }}"/>
        <!-- Font Awesome -->
        <link href="{{ asset('public/css/font-awesome.css') }}" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet"> 
        <link href="{{ asset('public/css/bootstrap.css') }}" rel="stylesheet">    
        <!-- Slick slider -->
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/slick.css') }}"/> 
        <!-- Fancybox slider -->
        <link rel="stylesheet" href="{{ asset('public/css/jquery.fancybox.css') }}" type="text/css" media="screen" /> 
        <!-- Animate css -->
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/animate.css') }}"/> 
        <!-- Progress bar  -->
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/bootstrap-progressbar-3.3.4.css') }}"/> 
        <!-- Theme color -->
        <link id="switcher" href="{{ asset('public/css/default-theme.css') }}" rel="stylesheet">

        <!-- Main Style -->
        <link href="{{ asset('public/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('public/css/jquery-ui.css') }}" rel="stylesheet">
        <link href="{{ asset('public/css/validationEngine.jquery.css') }}" rel="stylesheet">
        <script src="{{ asset('public/js/jquery.min.js') }}"></script>    
        @yield('header_styles')        
        <style type='text/css'>
            header{z-index: 1000;}
            .aftlgnimg {display:inline-flex !important}
            a,button { cursor: pointer; cursor: hand; }
            .small-uplsec > span {border-radius: 50%;display: inline-table;height: 42px;overflow: hidden;width: 134px;width: 42px;}
            .loader_div{display:none;position: fixed;width: 100%;height: 100%;left: 0;top: 0;background: rgba(51,51,51,0.7);z-index: 10000;}
            .loader_div img{widht:42px;height:42px;top: 50%;left:50%;position:fixed;}
            .languageFile{min-width:100px !important;margin: -27px 0 0 0 !important;display: block;padding: 5px; }
            .mydiv{border:2px solid #565656;font-weight: 700;text-decoration: none; border-radius:12%; font-size:16px; padding: 5px;}
            .mydiv:hover{background-color: #565656;color:white;text-decoration: none;}            
        </style>
        <script src="{{ asset('public/js/jquery.min.js') }}"></script>    
    </head>
    <body>
        <?php
        $userinformation = Auth::User();
        ?>
        <!-- Start header -->
        <header class="wow fadeInDown newhdr" data-wow-duration="0.5s" data-wow-delay="0.0s">
            <div class="container">
                <div class="col-xs-12 header hdr3">
                    <!-- LOGO --> 
                    <div class="col-md-3 col-sm-4 col-xs-6 padd">
                        <a class="logo2" href="{{route('home')}}"><img src="{{ asset('public/images/logo2.png') }}" alt="logo"></a>
                    </div>		
                    <!-- LOGO  End-->
                    <!-- desktop Nav  Start-->
                    <div class="col-sm-9 padd hidden-sm hidden-xs nav-rght">
                        <div class="col-xs-12 padd navtop">
                            <ul class="hidden-sm hidden-xs tpnav">
                                <li><a href="{{route('user.disclaimer')}}">{{ trans('message.DISCLAIMER') }}</a></li>
                                <li><a href="{{route('user.terms-and-conditions')}}">{{ trans('message.TERMS') }}</a></li>
                                <li><a href="{{ route('user.contact') }}">{{ trans('message.CONTACT') }}</a></li>                                
                                @if(Auth::check())                                                              
                                <li><a href="{{route('user.logout')}}">{{ trans('message.LOGOUT') }}</a></li>
                                @endif
                            </ul>
                            <div class="langsec">
                                <form id="lacal_form" method="post" action="{{ route('user.mylang') }}">
                                    <?php
                                    $lang = Session::get('localei');
                                    $myLang = array('en' => 'ENGLISH', 'de' => 'GERMAN', 'fr' => 'FRENCH', 'es' => 'SPAIN');
                                    ?>
                                    <label>{{ trans('message.LANGUAGE') }}</label>
                                    <div class="dropdown">
                                        <div class="dropdown">
                                            <select name="localei" id="locale" class="dropdown-menu languageFile" aria-labelledby="dropdownMenu1">
                                                <?php
                                                $selected = '';
                                                foreach ($myLang as $key => $value) {
                                                    echo $key . '==' . $lang;
                                                    if ($key == $lang) {
                                                        $selected = 'selected';
                                                    } else {
                                                        $selected = '';
                                                    }
                                                    ?>
                                                    <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                <?php } ?>
                                            </select>
                                            <input type="hidden" value="{{Route::getCurrentRoute()->getName()}}" name="currURl"/>

                                            {{ csrf_field() }}
                                        </div>
                                </form>
                            </div>
                        </div><!-- Top Nav  End-->
                        <div class="col-12 hidden-sm hidden-xs text-right padd">
                            <nav class="navbar">
                                <div class="collapse navbar-toggleable-md" id="navbarResponsive">
                                    <ul class="nav navbar-nav" style="margin: 18px 0 0 0;">
                                        <li class="active"><a href="{{route('home')}}">{{ trans('message.HOME') }}</a></li>
                                        <li><a href="{{route('user.about')}}">{{ trans('message.ABOUT') }}</a></li>
                                        @if(Auth::check())                                 
                                        @if(Auth::User()->role == '2')
                                        <li><a href="{{route('user.driver_profile')}}">{{ trans('message.MyProfile') }}</a></li>
                                        @else
                                        <li><a href="{{route('user.profile')}}">{{ trans('message.MyProfile') }}</a></li>
                                        @endif                                        
                                        @endif
                                        <li><a href="{{route('user.item-list')}}">{{ trans('message.ListOfItems') }}</a></li>
                                        <li><a href="{{route('user.driver-list')}}">{{ trans('message.DriverList') }}</a></li>
                                        @if(Auth::check())                                    
                                        <li class="dropdown msgddr"><a href="#">Message<span class="msgnb2">{{!empty($user_notification)?count($user_notification):0}}</span></a>
                                            <ul class="dropdown-menu logpfdtl" role="menu">
                                                <li class="nwmsg"><i class="fa fa-envelope-o" aria-hidden="true"></i> You have {{!empty($user_notification)?count($user_notification):0}} messages</li>   
                                                @if(!empty($user_notification))
                                                @foreach($user_notification as $objNotification)
                                                <?php
                                                $notifi_message = $notifi_type[$objNotification->notification_type];
                                                $sender_name = $objNotification->senderfirstname . " " . $objNotification->senderlastname;
                                                $notifi_message = str_replace("{{user-name}}", $sender_name, $notifi_message);
                                                $notifi_message = str_replace("{{item-name}}", $objNotification->product_name, $notifi_message);
                                                ?>
                                                <li><a href="{{route('view_notification', base64_encode($objNotification->id))}}"><?php echo $notifi_message ?></a></li>
                                                @endforeach                                                
                                                @endif
                                                <li class="msgvwall"><a href="#">View All</a></li> 
                                            </ul>
                                        </li>
                                        @endif
                                        <li class="dropdown hidden-xs aftlgnimg">
                                            @if(Auth::check())
                                            <a href="javascript:void(0)" class="dropdown-toggle small-uplsec" data-toggle="dropdown">
                                                <span>

                                                    <?php
                                                    $profile_pic = '';
                                                    $imageUser = !empty($userinfo->userimage) ? $userinfo->userimage : "";
                                                    if (isset($imageUser) && !empty($imageUser)) {
                                                        $profile_pic = route('home') . '/public/uploads/users/' . $imageUser;
                                                    } else {
                                                        $profile_pic = route('home') . '/public/images/usrimg2.png';
                                                    }
                                                    ?>
                                                    <img style="width:42px;height:42px;" src="{{$profile_pic}}" alt=""/>

                                                </span>
                                                <label class="hidden-sm">{{@$userinfo->firstname . " ". @$userinfo->lastname}}</label>
                                            </a>
                                            @else
                                            <label class="lgnbtns">
                                                <a class="" href="{{ route('user.signin') }}">{{ trans('message.LOGIN') }}</a>
                                                <a class="" href="{{ route('user.signup') }}">{{ trans('message.REGISTER') }}</a>
                                            </label>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div><!-- bottom Nav  End-->
                    </div><!-- desktop Nav  End-->
                    <!-- Mobile top nav -->
                    <div class="hidden-lg hidden-md">
                        <div class="lnglgnsec">
                            <div class="langsec">                            
                                <form id="lacal_form" method="post" action="{{ route('user.mylang') }}">
                                    <?php
                                    $lang = Session::get('localei');
                                    $myLang = array('en' => 'ENGLISH', 'de' => 'GERMAN', 'fr' => 'FRENCH', 'es' => 'SPAIN');
                                    ?>
                                    <label>{{ trans('message.LANGUAGE') }}</label>
                                    <div class="dropdown">
                                        <select name="localei" id="locale" class="dropdown-menu languageFile" aria-labelledby="dropdownMenu1">
                                            <?php
                                            $selected = '';
                                            foreach ($myLang as $key => $value) {
                                                echo $key . '==' . $lang;
                                                if ($key == $lang) {
                                                    $selected = 'selected';
                                                } else {
                                                    $selected = '';
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        </select>
                                        <input type="hidden" value="{{Route::getCurrentRoute()->getName()}}" name="currURl"/>
                                        {{ csrf_field() }}
                                    </div>
                                </form>
                            </div>
                        </div><!-- END Language Section--> 

                        <div class="col-sm-5 col-xs-6 pull-right text-right padd navmbsec">
                            <div class="col-xs-12 lgnbtns tplgbtn">
                                @if(!Auth::check())                        
                                <a href="{{ route('user.signin') }}">{{ trans('message.LOGIN') }}</a>
                                <a href="{{ route('user.signup') }}">{{ trans('message.REGISTER') }}</a>                        
                                @else
                                <a href="{{ route('user.logout') }}">{{ trans('message.LOGOUT') }}</a>                            
                                @endif
                                <!--                            <a href="{{route('user.signin')}}">Login</a>
                                                            <a href="{{route('user.signup')}}">Register</a>-->
                            </div> 
                            <nav class="navbar respnav">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar" aria-controls="exCollapsingNavbar" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars" aria-hidden="true"></i></button>
                                <div class="collapse navbar-toggleable-md" id="navbarResponsive2">
                                    <ul class="nav navbar-nav">
                                        <li class="active"><a href="{{route('home')}}">{{ trans('message.HOME') }}</a></li>
                                        <li><a href="{{route('user.about')}}">{{ trans('message.ABOUT') }}</a></li>
                                        <li><a href="{{route('user.item-list')}}">{{ trans('message.ListOfItems') }}</a></li>
                                        <li><a href="{{route('user.driver-list')}}">{{ trans('message.DriverList') }}</a></li>
                                        @if(Auth::check())                                        
                                        @if(Auth::User()->role == "2")
                                        <li><a href="{{route('user.driver_profile')}}">{{ trans('message.MyProfile') }}</a></li>
                                        @else
                                        <li><a href="{{route('user.profile')}}">{{ trans('message.MyProfile') }}</a></li>
                                        @endif
                                        @endif
                                        <li><a href="{{route('user.disclaimer')}}">{{ trans('message.DISCLAIMER') }}</a></li>
                                        <li><a href="{{route('user.terms-and-conditions')}}">{{ trans('message.TERMS') }}</a></li>
                                        <li><a href="{{ route('user.contact') }}">{{ trans('message.CONTACT') }}</a></li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                    <!-- Mobile top nav  End-->
                </div>
            </div>
            <!-- END container -->
        </header>
        <!-- End header -->
        <section id="headrbotm">          
            @yield('content')
        </section>
        <!-- Start Footer -->
        <footer>
            <div class="container">
                <div class="footin">
                    <p class="col-md-8 cpyryt">{{ trans('message.COPYRITE') }}   <a href="">{{ trans('message.DISCLAIMER') }} </a>   |   <a href="">{{ trans('message.TERMS') }} </a></p>
                    <ul class="col-md-4 soclicons">
                        <li><label>{{ trans('message.FOLLOW') }} </label></li>
                        <li><a href="javascript:void(0)"><img src="{{ asset('public/images/fb.png') }}" alt=""/></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ asset('public/images/twt.png') }}" alt=""/></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ asset('public/images/gplus.png') }}" alt=""/></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ asset('public/images/insta.png') }}" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </footer>
        <div class='loader_div'>
            <img src="{{route('home')}}/public/images/loader.gif"/>
        </div>
        <!-- End Footer -->
        <!-- jQuery library -->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <!-- Bootstrap -->
        <script src="{{ asset('public/js/bootstrap.js')}}"></script>
        <!-- Slick Slider -->
        <script type="text/javascript" src="{{ asset('public/js/slick.js')}}"></script>    
        <!-- mixit slider -->
        <script type="text/javascript" src="{{ asset('public/js/jquery.mixitup.js')}}"></script>
        <!-- Add fancyBox -->        
        <script type="text/javascript" src="{{ asset('public/js/jquery.fancybox.pack.js')}}"></script>
        <!-- counter -->
        <script src="{{ asset('public/js/waypoints.js')}}"></script>
        <script src="{{ asset('public/js/jquery.counterup.js')}}"></script>
        <!-- Wow animation -->
        <script type="text/javascript" src="{{ asset('public/js/wow.js')}}"></script> 
        <!-- progress bar   -->
        <script type="text/javascript" src="{{ asset('public/js/bootstrap-progressbar.js')}}"></script>  
        <!-- Custom js -->
        <script type="text/javascript" src="{{ asset('public/js/custom.js')}}"></script>        
        <script type="text/javascript" src="{{ asset('public/js/jquery-ui.js') }}"></script>
        
        <script type="text/javascript" src="{{ asset('public/js/jquery.validationEngine.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/js/jquery.validationEngine-en.js') }}"></script>
        @yield('footer_scripts')
        <script type="text/javascript">
$(document).ready(function() {
    $(".navbar-toggler").click(function() {
        $(".collapse").toggle("slow");
    });
    //to change the language file
    $('#locale').on('change', function() {
        $('#lacal_form').submit();
    });
    var s = $("header");
    var pos = s.position();
    $(window).scroll(function() {
        var windowpos = $(window).scrollTop();
        if (windowpos >= pos.top & windowpos <= 10) {
            s.removeClass("stick");
        } else {
            s.addClass("stick");
        }
    });
    $('body').on('focus', ".jquery-datepicker", function() {
        $(this).datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            yearRange: "1950:2050",
            minDate: new Date(),
            change: function() {
                debugger;
            }
        });
    });
    $('body').on('focus', ".jquery-datepicker-dob", function() {
        $(this).datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            yearRange: "1950:2050",
            maxDate: new Date()
        });
    });
    $(".counrties").change(function() {
        var country_id = $(this).val();
        var url = "{{url('diling-code')}}";
        url = url + "/" + country_id;
        $.ajax({
            url: url,
            dataType: 'JSON',
            type: 'GET',
            success: function(res) {
                if (res['status']) {
                    console.log(res['data']);
                    $(".country_code").html(res['data']['country_dailing_code']);
                    if (res['data']['country_type'] == "europ" && res['data']['name'] != "United Kingdom") {
                        $("#currency").val('2');
                    } else if (res['data']['country_type'] == "europ" && res['data']['name'] == "United Kingdom") {
                        $("#currency").val('3');
                    } else if (res['data']['country_type'] == "" && res['data']['name'] == "Australia") {
                        $("#currency").val('4');
                    } else {
                        $("#currency").val('1');
                    }
                }
            },
            error: function(res) {

            }

        });
    });
    $('form').submit(function() {
        if ($(this).attr('id') != "lacal_form") {
            var status = true;
            $("input.required, select.required").each(function() {
                if ($(this).val() == "") {
                    status = false;
                    $(this).parents('.form-group').first().addClass('has-error');
                } else {
                    $(this).parents('.form-group').first().removeClass('has-error');
                }

<<<<<<< Updated upstream
                    });
                    $("input.numeric, select.numeric").each(function() {
                        var inputtext = $(this).val();
                        if (inputtext == "" || $.isNumeric(inputtext)) {
                            $(this).parents('.form-group').first().removeClass('has-error');
                        } else {
                            status = false;
                            $(this).parents('.form-group').first().addClass('has-error');
                        }
                    });
                    $("input.email").each(function() {
                        var inputtext = $(this).val();
                        if (isValidEmailAddress(inputtext)) {
                            $(this).parents('.form-group').first().removeClass('has-error');
                        } else {
                            status = false;
                            $(this).parents('.form-group').first().addClass('has-error');
                        }
                    });
                    if (status == true) {
                        return true;
                    } else {
                        return false;
                    }
                }    
=======
>>>>>>> Stashed changes
            });
            $("input.numeric, select.numeric").each(function() {
                var inputtext = $(this).val();
                if ($.isNumeric(inputtext)) {
                    $(this).parents('.form-group').first().removeClass('has-error');
                } else {
                    status = false;
                    $(this).parents('.form-group').first().addClass('has-error');
                }
            });
            $("input.email").each(function() {
                var inputtext = $(this).val();
                if (isValidEmailAddress(inputtext)) {
                    $(this).parents('.form-group').first().removeClass('has-error');
                } else {
                    status = false;
                    $(this).parents('.form-group').first().addClass('has-error');
                }
            });
            if (status == true) {
                return true;
            } else {
                return false;
            }
        }
    });
});
function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}
        </script>
    </body>
</html>