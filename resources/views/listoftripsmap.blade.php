@extends('layouts/home')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
<link href="{{ asset('public/css/datepicker.css') }}" rel="stylesheet">
<link href="{{ asset('public/css/bootstrap-timepicker.min.css') }}" rel="stylesheet">
<style type='text/css'>
    .driv_prof {  border-radius: 20%;  height:50px;}
</style>
@stop
{{-- Page content --}}
@section('content')
<!-- Start content -->
<section id="">
    <!-- Start content -->
    <div class="container">
        <div class="col-md-12 wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0s">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session('flash_alert_notice'))
            <div class="flash-message">
                <div class="alert alert-{{session('flash_action')}}">
                    <p>{{session('flash_alert_notice')}}</p>
                </div>
            </div>
            @endif
            <div class="title-area">
                <h2 class="title">Map view of trips</h2>
                <span class="line"></span>
            </div>

        </div>
        <div class="col-md-12 lstfltxt">

            <div class="col-sm-12 rgtbarnw wow zoomIn" data-wow-duration="0.8s" data-wow-delay="0s">
                <div class="row">

                    <div class="table-responsive text-center bypftbl">
                        <div id="map_canvas" style="width:100%;height:400px;"></div>
                    </div>
                    <br><br>
                    <div class = "fltbtns">
                        <div class = "row">
                            <div class = "col-xs-12 col-sm-3 accprbtn">
                                <?php
                                $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                                $url = str_replace('trip-list-map?', 'trip-list?', $url);
                                ?>
                                <a href = "<?php echo $url; ?>">List View</a>
                            </div>
                            <?php
                            ?>
                            <div class = "col-sm-7 col-xs-12 nxbcbtn">
                                <!--<div class = "col-xs-12 col-sm-6 askbtn ftnbbtn">
                                    <a href = "<?php // echo $url;      ?>">Back</a>
                                </div>
                                <div class = "col-xs-12 col-sm-6 accprbtn ftnbbtn">
                                    <a href = "javascript:void(0)">Next</a>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div><!--End row -->
            </div><!--End right panel -->
        </div>
    </div>
    <!--End content -->

    <?php
    $dataArr = array();
    ?>

    <?php $idsArr = array(); ?>
    @if(isset($trips) && !empty($trips))
    @foreach($trips as $objtem)
    <?php
    if (in_array($objtem->id, $idsArr))
        continue;
    $dataArr[] = $objtem;
    $idsArr[] = $objtem->id;
    ?>
    @endforeach
    @endif
    <?php
    $dataArr = json_encode($dataArr);
    ?>
</section>
<!--End content -->
@stop
@section('footer_scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDL5Ae9Mv4lqPyQ1wD3NUhHkpmuX85DFo4&libraries=places"></script>

<script>
var dataArr = JSON.parse('<?php echo $dataArr; ?>');

var map, bounds, infowindow;

function initialize() {
	infowindow = new google.maps.InfoWindow();
    bounds = new google.maps.LatLngBounds();
    var latlng = new google.maps.LatLng(-34.397, 150.644);
    var mapOptions = {
        zoom: 8,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

    showAllRecords();
}

function showAllRecords() {
    if (dataArr.length > 0) {

        for (i in dataArr) {
            var id = dataArr[i].id
            var from_latitude = dataArr[i].from_latitude;
            var from_longitude = dataArr[i].from_longitude;
            var to_latitude = dataArr[i].to_latitude;
            var to_longitude = dataArr[i].to_longitude;

            var firstname = dataArr[i].firstname;
            var trip_from = dataArr[i].trip_from;
            var trip_to = dataArr[i].trip_to;
            var trip_time = dataArr[i].trip_time;
            var userid = dataArr[i].userid;
            var baseUrl = "{{route('home')}}";
            var baseToUrl = baseUrl + "/driver-detail/" + userid;


            var myLatLngFrom = new google.maps.LatLng(from_latitude, from_longitude);
            bounds.extend(myLatLngFrom);
            var markerFrom = new google.maps.Marker({
                map: map,
                position: myLatLngFrom,
                icon: './public/images/black.png',
                url: baseToUrl,
                firstname:firstname,
                trip_from:trip_from,
                trip_to:trip_to,
               trip_time:trip_time
            });

            var myLatLngTo = new google.maps.LatLng(to_latitude, to_longitude);
            bounds.extend(myLatLngTo);
            var markerTo = new google.maps.Marker({
                map: map,
                position: myLatLngTo,
                icon: './public/images/black.png',
                url: baseToUrl,
                firstname:firstname,
                trip_from:trip_from,
                trip_to:trip_to,
                trip_time:trip_time 
            });
			//events for from marker
            google.maps.event.addListener(markerFrom, 'click', function() {
                window.location.href = this.url;
            });
            google.maps.event.addListener(markerFrom, 'mouseover', function() {
                var html = '<div id="content">' +
                    '<div id="siteNotice">' +
                    '</div>' +
                    '<div id="bodyContent" style="width:auto; height:auto;">' +
                    '<p><b>Drivers Details</b></p>' +
                    '<p>Name : ' + this.firstname + ' </p>' +
                    '<p>Trip From : ' + this.trip_from + ' </p>' +
                    '<p>Trip to : ' + this.trip_to + ' </p>' +
                    '<p>Trip Time: ' + this.trip_time + ' </p>' +
                    '</div>';
                infowindow.setContent(html);
                infowindow.open(map, this);
            });
            google.maps.event.addListener(markerFrom, 'mouseout', function() { 
                infowindow.setMap(null);
            });

			//events for to marker
            google.maps.event.addListener(markerTo, 'mouseover', function() {
                var html = '<div id="content">' +
                    '<div id="siteNotice">' +
                    '</div>' +
                    '<div id="bodyContent" style="width:auto; height:auto;">' +
                    '<p><b>Drivers Details</b></p>' +
                    '<p>Name : ' + this.firstname + ' </p>' +
                    '<p>Trip From : ' + this.trip_from + ' </p>' +
                    '<p>Trip to : ' + this.trip_to + ' </p>' +
                    '<p>Trip Time: ' + this.trip_time + ' </p>' +
                    '</div>';
                infowindow.setContent(html);
                infowindow.open(map, this);
            });
			google.maps.event.addListener(markerTo, 'mouseout', function() { 
                infowindow.setMap(null);
            });
            google.maps.event.addListener(markerTo, 'click', function() {
                window.location.href = this.url;
            });
        }
        map.fitBounds(bounds);
    }
}

$(document).ready(function() {
    initialize();

    var s = $("header");
    var pos = s.position();
    $(window).scroll(function() {
        var windowpos = $(window).scrollTop();
        if (windowpos >= pos.top & windowpos <= 10) {
            s.removeClass("stick");
        } else {
            s.addClass("stick");
        }
    });
});

</script>

<script>
    $(document).ready(function() {
        $(".fltrtxt").click(function() {
            $(".fltdpdwn").toggle();
        });
    });
</script>
<!-- Write your script here -->
@stop
