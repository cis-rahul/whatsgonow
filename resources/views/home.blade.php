@extends('layouts/master')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

@stop

{{-- Page content --}}
@section('content')
<div class="col-lg-10 col-md-11 col-sm-12 serdtl wow zoomIn" data-wow-duration="0.8s" data-wow-delay="0.8s">
    <div class="col-md-4 col-sm-4 col-xs-12 serin">
        <div class="col-xs-12 srvcsbg">
            <ul>                
                <li>{{ trans('message.Boxone1') }}</li>
                <li>{{ trans('message.Boxone2') }}</li>
                <li>{{ trans('message.Boxone3') }}</li>
                <li>{{ trans('message.Boxone4') }}</li>
            </ul>
        </div>
    </div>

    <div class="col-md-4 col-sm-4 col-xs-12 serin">
        <div class="col-xs-12 srvcsbg">
            <ul><?php if (!empty(trans('message.Boxtwo1'))) { ?>
                <li>{{ trans('message.Boxtwo1') }}</li>
                <?php } ?>
                <li>{{ trans('message.Boxtwo2') }}</li>
                <li>{{ trans('message.Boxtwo3') }}</li>
                <li>{{ trans('message.Boxtwo4') }}</li>
            </ul>
        </div>
    </div>

    <div class="col-md-4 col-sm-4 col-xs-12 serin">
        <div class="col-xs-12 srvcsbg">
            <ul>
                <li>{{ trans('message.Boxthree1') }}</li>
                <li>{{ trans('message.Boxthree2') }}</li>
            </ul>
        </div>
    </div>
</div><!-- End Services -->

<section class="formsection">
    <div class="col-lg-8 col-md-9 col-sm-12 srcform">        
        @if (session('flash_alert_notice'))
        <div class="flash-message">
            <div class="alert alert-{{session('flash_action')}}">
                <p>{{session('flash_alert_notice')}}</p>
            </div>
        </div>
        @endif
        <form action="{{ route('user.item-list') }}" method="get"  id="homePageSearch" >
            <div class="form-group wow slideInLeft" data-wow-duration="0.8s" data-wow-delay="0s">
                <label class="col-sm-1 col-form-label">{{ trans('message.From') }}:</label>
                <div class="col-sm-3 {{ $errors->first('fromPostalCode', 'has-error')}}">
                    <input type="text" name="fromPostalCode" class="form-control from" placeholder="{{ trans('message.PostalCode') }}"/>
                </div>
                <div class="col-sm-3 {{ $errors->first('fromCity', 'has-error')}}">
                    <input type="text" name="fromCity" class="form-control from" placeholder="{{ trans('message.City') }}"/>
                </div>                
                <div class="col-sm-3 {{ $errors->first('fromCountry', 'has-error')}}">                    
                    <select name="fromCountry" class="from">
                        <option value="">{{ trans('message.PleaseSelect') }}</option>
                        @if(!empty($countries))
                        @foreach($countries as $key=>$val)
                        <option value="{{$key}}">{{$val}}</option>
                        @endforeach                        
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group wow slideInRight" data-wow-duration="0.8s" data-wow-delay="0s">
                <label class="col-sm-1 col-form-label">{{ trans('message.To') }}:</label>
                <div class="col-sm-3 {{ $errors->first('toPostalCode', 'has-error')}}">
                    <input type="text" name="toPostalCode" class="form-control to" placeholder="{{ trans('message.PostalCode') }}"/>
                </div>
                <div class="col-sm-3 {{ $errors->first('toCity', 'has-error')}}">
                    <input type="text" name="toCity" class="form-control to" placeholder="{{ trans('message.City') }}"/>
                </div>
                <div class="col-sm-3 {{ $errors->first('toCountry', 'has-error')}}">
                    <select name="toCountry" class="to">
                        <option value="">{{ trans('message.PleaseSelect') }}</option>
                        @if(!empty($countries))
                        @foreach($countries as $key=>$val)
                        <option value="{{$key}}">{{$val}}</option>
                        @endforeach                        
                        @endif
                    </select>
                </div>
            </div>

            <div class="frmdrbtns wow zoomIn" data-wow-duration="0.8s" data-wow-delay="0s">
                <button type="submit" name="lookforitem" style="display:none;" id="lookforitem">lookforitem</button>
                <button type="submit" name="lookfordriver" style="display:none;" id="lookfordriver">lookfordriver</button>
                <a href="javascript:void(0)" onclick="submitForm('lookforitem')" class="drvbtn" >{{ trans('message.LookForItems') }}</a>
                <a href="javascript:void(0)" onclick="submitForm('lookfordriver')" class="secdrvbtn" >{{ trans('message.LookForDrivers') }}</a>
            </div>

        </form>

    </div><!-- End Services -->    
</section>
<section class="abotsection">
    <div class="col-lg-10 col-md-11 col-sm-12 abutin wow flipInX" data-wow-duration="1.5s" data-wow-delay="0.8s">
        <p> 
            {{ trans('message.HomeParaOne') }}</br>
            {{ trans('message.HomeParaOne1') }}
        </p>
    </div>
    <div class="col-lg-10 col-md-11 col-sm-12 dtprvcyot wow bounce" data-wow-duration="1.5s" data-wow-delay="0.8s">
        <div class="dtprvcy">
            <div class="title-area">
                <h2 class="title">{{ trans('message.DataPrivacy') }}</h2>
                <span class="line"></span>
            </div>
            <div class="col-sm-3 col-xs-12 cldlogoimg">
                <img class="img-responsive" src="{{ asset('public/images/cldlogoimg.png')}}" alt=""/>
            </div>
            <div class="col-sm-9 col-xs-12 cldlst">
                <ul>
                    <li>{{ trans('message.HomeParaThirdF') }}</li>
                    <li>{{ trans('message.HomeParaThirdS') }}</li>
                    <li>{{ trans('message.HomeParaThirdT') }}</li>
                </ul>
            </div>            
        </div>
    </div><!-- End abotsection -->
    <div class="addsection">
        
        <?php $url = asset('public/uploads/advertise/').'/'.$link_data[0]->ads_images; ?>
        <a target="_blank" href="<?php echo $link_data[0]->ads_link; ?>"><img class="img-responsive" src="<?php echo $url; ?>" alt=""/></a>
    </div>
</section>
<!-- Modal -->
<div id="myModalItemReceived" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="edit" data-size="16" data-loop="true" data-c="#fff" data-hc="white" id="livicon-47" style="width: 16px; height: 16px;"></i>
                            Item Received
                        </h3>                        
                    </div>
                    <div class="panel-body">
                        <div class="flag-error"></div>
                        <form action="{{route('confirmation_received')}}" method="POST" role="form" id="frm_addtrip" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="request_id" value="{{session('request_id')}}" />
                            <p>If you received user item from seller, than click 'Ok' button, Otherwise click on "Cancel" button</p>
                            <div class="row">
                                <div class="col-xs-6 col-sm-12 col-md-6" style="height: 54px;">
                                    <button style="width:172px;" type="submit" id="btn_add_trip" name="btn_add_trip" class="btn btn-default">Ok</button>
                                </div>
                                <div class="col-xs-6 col-sm-12 col-md-6" style="height: 54px;">
                                    <button style="width:172px;" type="button" class="btn btn-default cancel_confirmation">Cancel</button>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>                
            </div>
        </div>    
    </div><!-- Modal End-->
</div>
<!-- End content -->
<div id="myModalItemDelivered" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="edit" data-size="16" data-loop="true" data-c="#fff" data-hc="white" id="livicon-47" style="width: 16px; height: 16px;"></i>
                            Item Received
                        </h3>                        
                    </div>
                    <div class="panel-body">
                        <div class="flag-error"></div>
                        <form action="{{route('confirmation_delivered')}}" method="POST" role="form" id="frm_addtrip" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="request_id" value="{{session('request_id')}}" />
                            <p>If you received user item from driver, than click 'Ok' button, Otherwise click on "Cancel" button</p>
                            <div class="row">
                                <div class="col-xs-6 col-sm-12 col-md-6" style="height: 54px;">
                                    <button style="width:172px;" type="submit" id="btn_add_trip" name="btn_add_trip" class="btn btn-default">Ok</button>
                                </div>
                                <div class="col-xs-6 col-sm-12 col-md-6" style="height: 54px;">
                                    <button style="width:172px;" type="button" class="btn btn-default cancel_confirmation">Cancel</button>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>                
            </div>
        </div>    
    </div><!-- Modal End-->
</div>
<div id="myModalItemError" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="edit" data-size="16" data-loop="true" data-c="#fff" data-hc="white" id="livicon-47" style="width: 16px; height: 16px;"></i>
                            Error Message
                        </h3>                        
                    </div>
                    <div class="panel-body">
                        <div class="flag-error"></div>
                        <p>Sorry! This item not valid</p>
                    </div>
                </div>                
            </div>
        </div>    
    </div><!-- Modal End-->
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDL5Ae9Mv4lqPyQ1wD3NUhHkpmuX85DFo4&libraries=places&libraries=geometry"></script>
@stop
@section('footer_scripts')
<script type='text/javascript'>
                    $(document).ready(function() {
                        var popupmodal = "{{session('popup')}}";
                        if (popupmodal == "item_received") {
                            $("#myModalItemReceived").modal();
                        }
                        if (popupmodal == "item_delivered") {
                            $("#myModalItemDelivered").modal();
                        }
                        if (popupmodal == "error-msg") {
                            $("#myModalItemError").modal();
                        }
                        $(".cancel_confirmation").click(function() {
                            $(this).parents(".modal").modal('hide');
                        });
                    });
                    function submitForm(elementTrigger) {
                        if (elementTrigger == 'lookfordriver') {
                            var getFormAction = $('form#homePageSearch').attr('action');
                            getFormAction = getFormAction.replace('item-list', 'trip-list');
                            $('form#homePageSearch').attr('action', getFormAction);
                        }
                        $('#' + elementTrigger).trigger('click');
                    }
</script>



@stop
