<table width="570" cellpadding="0" cellspacing="0" style="font-size:14px; color:#506194;margin: 0 auto; padding:50px 20px;">
    <tr>
        <td align="center">
            <table align="center" border="0" cellspacing="0" cellpadding="0">
                <tr>   
                    <td>
                        <a href="#"><img src="{{ asset('public/images/logo.png')}}" alt=""/></a>
                    </td>
                </tr>
            </table>
            <!-- end logo -->
            <table align="center" border="0" cellspacing="0" cellpadding="0" style="margin: 40px 0px 0px;">
                <tr>   
                    <td style="border: 4px solid #fff;  border-radius: 50%;  height: 116px;  overflow: hidden; width: 116px;">                        
                        <img width="144px" height="144px" src="{{$userimage}}" alt=""/>
                    </td>
                    <td style="display: inline-block; margin: 15px 30px;">
                        <h3 style="color: #312f2f;  font-size: 19px;  margin: 5px 0 3px; text-align: left; font-weight: lighter;text-align: center;">{{$driver_name}}</h3>
                        <p style="  font-family: arial; color: #d37d06;  font-size: 14px; line-height: 20px;  margin: 0 0 10px; text-align: left; font-weight: lighter;text-align: center;">{{$driver_city}}</p>
                        <p style="  font-family: arial; color: #312f2f;  font-size: 16px; line-height: 20px;  margin: 0 0 10px; text-align: left; font-weight: lighter;text-align: center;">{{$vehicle_type}}</br>
                            ({{$vehicle_model}})</p>
                    </td>
                    <td style="">
                        <img width="100%" src="{{ asset('public/images/diagrm.png')}}" alt=""/>
                    </td>
                </tr>
            </table>
            <!-- end driver detail -->
            <table align="center" border="0" cellspacing="0" cellpadding="0" style="margin: 40px 0px 0px;">
                <tr>   
                    <td style="display: inline-block; margin:20px 0 10px;">
                        <p style="font-family: arial; color: #312f2f;  font-size: 16px; margin: 0 0 15px; text-align: center; font-weight: lighter;">
                            {{$mail_message}}
                        </p>
                    </td>    
                </tr>                
            </table>
            <!-- end driver cost -->            
            <!-- end driver buttons -->
            <table align="center" border="0" cellspacing="0" cellpadding="0" style="margin:45px 0 0;">
                <tr style="text-align: center;">   
                    <td style="display: inline-block; margin:0 0 7px;">
                        <ul style="margin: 0px; padding: 0px; width: 178px;">
                            <li style="display:inline-block; float: left; margin: 4px 0 0;"><label style="color: #2d3235; font-size: 13px; font-weight: lighter; margin: 0;">Follow Us on:</label></li>
                            <li style=" display:inline-block; float: left;"><a href="javascript:void(0)"><img src="{{ asset('public/images/fb.png')}}" alt=""></a></li>
                            <li style=" display:inline-block; float: left;"><a href="javascript:void(0)"><img src="{{ asset('public/images/twt.png')}}" alt=""></a></li>
                            <li style=" display:inline-block; float: left;"><a href="javascript:void(0)"><img src="{{ asset('public/images/gplus.png')}}" alt=""></a></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td style="display: inline-block; margin:0;">
                        <p style="font-family: arial; color: #85939d;  font-size: 13px; margin: 0; text-align: center; font-weight: lighter;">© Copyright 2016. All Rights reserved.
                            <a style="font-family: arial; color: #85939d;  font-size: 13px;text-decoration: none;font-weight: lighter;" href="javascript:void(0)">Disclaimer</a>   |  <a style="font-family: arial; color: #85939d;  font-size: 13px; margin: 0;text-decoration: none; font-weight: lighter;" href="javascript:void(0)"> Terms & Conditions</a></p>
                    </td>
                </tr>
            </table>
            <!-- end driver buttons -->
        </td>
    </tr>
</table>