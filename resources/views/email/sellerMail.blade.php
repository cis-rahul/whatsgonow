<table width="570" cellpadding="0" cellspacing="0" style="font-size:14px; color:#506194;margin: 0 auto; padding:50px 20px;">
    <tr>
        <td align="center">
            <table align="center" border="0" cellspacing="0" cellpadding="0">
                <tr>   
                    <td>
                        <a href="#"><img src="{{route('home')}}/public/images/logo.png" alt=""/></a>
                    </td>
                </tr>
            </table>
            <!-- end logo -->
            <table align="center" border="0" cellspacing="0" cellpadding="0" style="margin: 40px 0px 0px;">
                <tr>   
                    <td style="display: inline-block; margin:20px 0 10px;">
                        <h6 style="font-family: arial; color: #312f2f;  font-size: 16px; margin:0; text-align: center; font-weight: lighter;">Following driver will pickup</h6>
                        <h6 style="font-family: arial; color: #312f2f;  font-size: 16px; margin:0; text-align: center; font-weight: lighter;"><span style="font-family: 'Montserrat', sans-serif; color: #141414;  font-size: 16px; margin:0; text-align: center; font-weight: normal;">{{$item_name}}</span></h6>
                    </td>
                </tr>
            </table>
            <!-- end driver cost -->
            <table align="center" border="0" cellspacing="0" cellpadding="0" style="background: #f7fafc; margin:22px 0 40px; padding: 0 0 21px; text-align: center;  width: 100%;">
                <tr>
                    <td style="display: inline-block; margin:-8px 0 10px;">                                                
                        <img width="180px;" height="180px;" style="float: left; margin: 0px 10px 0px 0px;"src="{{$driver_image}}" alt="">
                    </td>
                    <td style="display: inline-block; margin:-8px 0 10px;">                        
                        <h4 style="color: #312f2f;  font-family: 'Montserrat', sans-serif;  text-align: left; font-size: 17px; margin:18px 0 15px; font-weight: normal;">Driver Name : <span style="font-family: 'Montserrat', sans-serif; color: #F2BA6D;  font-size: 16px; margin:0; text-align: left; font-weight: normal;">{{$driver_name}}</span></h4>
                        <h6 style="font-family: arial; color: #312f2f;  font-size: 16px; margin:0; text-align: left; font-weight: lighter;">Car Type : <span style="font-family: 'Montserrat', sans-serif; color: #F2BA6D;  font-size: 16px; margin:0; text-align: center; font-weight: normal;">{{$car_type}}</span></h6>
                        <h6 style="font-family: arial; color: #312f2f;  font-size: 16px; margin:0; text-align: left; font-weight: lighter;">Contact No. : <span style="font-family: 'Montserrat', sans-serif; color: #F2BA6D;  font-size: 16px; margin:0; text-align: center; font-weight: normal;">{{$driver_contact_no}}</span></h6>
                        <h6 style="font-family: arial; color: #312f2f;  font-size: 12px; margin:0; text-align: left; font-weight: lighter;"><span style="font-family: 'Montserrat', sans-serif; color: #F2BA6D;  font-size: 14px; margin:0; text-align: center; font-weight: normal;">{{$driver_address}}</span></h6>
                        <h6 style="font-family: arial; color: #312f2f;  font-size: 16px; margin:0; text-align: left; font-weight: lighter;"><img style="float: left; margin: 0px 10px 0px 0px;"src="{{route('home')}}/public/images/calnd.png" alt="">{{$trip_date}}</h6>
                    </td>
                </tr>
                
            </table>
            <!-- end driver from -->
            <table align="center" border="0" cellspacing="0" cellpadding="0" style="background: #f7fafc; margin:22px 0 10px; padding: 0 0 21px; text-align: center;  width: 100%;">
                <tr>
                    <td style="display: inline-block; margin:-8px 0 10px;">
                        <h6 style="background: #313131;color: #fff; display: inline; float: none;font-family: 'Montserrat', sans-serif; font-size: 13px;  margin: 0; padding: 1px 6px;
                            text-transform: uppercase; width: auto;">Note</h6>                        
                        <h4 style="color: #312f2f;  font-family: 'Montserrat', sans-serif;  font-size: 17px; margin:18px 0 15px; font-weight: normal;">&nbsp;</h4>
                        <p style="  font-family: arial; font-weight: lighter;color: #312f2f; font-size: 14px; margin: 0;">Please find the attachment and Download QR-CODE, It's for Driver! </p>
                    </td>
                </tr>                
            </table>
            <!-- end driver to -->
            <table align="center" border="0" cellspacing="0" cellpadding="0" style="margin:45px 0 0;">
                <tr style="text-align: center;">   
                    <td style="display: inline-block; margin:0 0 7px;">
                        <ul style="margin: 0px; padding: 0px; width: 178px;">
                            <li style="display:inline-block; float: left; margin: 4px 0 0;"><label style="color: #2d3235; font-size: 13px; font-weight: lighter; margin: 0;">Follow Us on:</label></li>
                            <li style=" display:inline-block; float: left;"><a href="#"><img src="{{route('home')}}/public/images/fb.png" alt=""></a></li>
                            <li style=" display:inline-block; float: left;"><a href="#"><img src="{{route('home')}}/public/images/twt.png" alt=""></a></li>
                            <li style=" display:inline-block; float: left;"><a href="#"><img src="{{route('home')}}/public/images/gplus.png" alt=""></a></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td style="display: inline-block; margin:0;">
                        <p style="font-family: arial; color: #85939d;  font-size: 13px; margin: 0; text-align: center; font-weight: lighter;">© Copyright 2016. All Rights reserved.
                            <a style="font-family: arial; color: #85939d;  font-size: 13px;text-decoration: none;font-weight: lighter;" href="#">Disclaimer</a>   |  <a style="font-family: arial; color: #85939d;  font-size: 13px; margin: 0;text-decoration: none; font-weight: lighter;" href="#"> Terms & Conditions</a></p>
                    </td>
                </tr>
            </table>
            <!-- end driver buttons -->
        </td>
    </tr>
</table>