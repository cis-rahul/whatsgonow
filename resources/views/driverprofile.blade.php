@extends('layouts/home')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
<link href="{{ asset('public/css/bootstrap-timepicker.min.css') }}" rel="stylesheet">
<style type='text/css'>     
    .modal-content button.btn {width:100%}
    .modal-body {display: inline-block;padding: 50px 42px 58px 41px;position: relative;}
    .bypftbl{max-height: 400px; overflow:hidden;}
    .bypftbl:hover { overflow-y:scroll; }
    .itmimg img {width: 90%;border-radius: 6px;height: 78%;}
    .accprbtn button {background: #ffaf42;border-radius: 5px;color: #000;display: inline-block;font-family: "montserratregular";font-size: 18px;height: 49px;line-height: 49px;margin: 0;text-decoration: none;text-shadow: 1px 1px 1px #a0a0a0;width: 100%;text-align: center;}
    .has-error .form-control {border-color: #a94442 !important;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);}
    .datepicker{z-index:1151 !important;}
    .bootstrap-timepicker-widget{z-index:1151 !important;}
    #myModal .modal-content{text-align: left;}
    .tblinfld span {border: 1px solid #d6dce0;border-radius: 6px;box-shadow: none;color: #949494;font-size: 13px;height: 43px;padding: 6px 15px;line-height: 32px;text-align: left;}

    @media (max-width: 360px){
        .diaggrm2{top: -204px;left: 124px;}                
    }
    @media (min-width: 361px){
        .diaggrm2{top:14px;}                
    }
</style>
@stop
{{-- Page content --}}
@section('content')
<!-- Start content -->
<div class="container">
    <div class="col-md-12 drvprfhd wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0s">
        <div class="title-area">
            <h2 class="title">{{ trans('message.DriverProfile') }}</h2>
            <span class="line"></span>
        </div>
    </div>
    <!--    <div class="col-xs-12 text-center wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0s; animation-name: fadeInDown;">
            <ul class="bystep">
                <li><a href="{{ route('user.item-list') }}">Look for Items</a></li>
                <li><a href="{{ route('item.create') }}">Upload an Item</a></li>
            </ul>
        </div>-->
    <div class="col-md-12 byfrmot">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if (session('flash_alert_notice'))
        <div class="flash-message">
            <div class="alert alert-{{session('flash_action')}}">
                <p>{{session('flash_alert_notice')}}</p>
            </div>
        </div>
        @endif
        <form action="{{route('user.driver_profile')}}" method="POST" role="form" id="frm_profieupdate" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" name="role" value="2" />
            <input type="hidden" name="userid" value="{{$userinfo['id']}}" />
            <div class="col-md-4 lftbar wow slideInLeft" data-wow-duration="0.8s" data-wow-delay="0.5s">
                <div class="col-xs-12 text-center">
                    <div class="uplsec">
                        @if(!empty($userinfo['userimage']))
                        <?php $path = route('home') . "/public/uploads/users/" . $userinfo['userimage']; ?>
                        <span><img width="135px" height="135px"  src="{{ $path}}" alt=""/></span>
                        @else
                        <span><img width="135px" height="135px" src="{{ asset('public/images/usrimg.png') }}" alt=""/></span>
                        @endif
                        @if(empty($userinfo['userimage']))
                        <div class="uplbtnmn">
                            <div class="uplbtn">
                                <input type="file" value="" id="userimage" name="userimage"/>
                                <img src="{{ asset('public/images/uplbtn1.png') }}" alt=""/>
                            </div>
                        </div>
                        @endif
                    </div>	
                </div><!--upload image-->
                <div class="col-xs-12 form-group {{ $errors->first('firstname', 'has-error')}}">
                    <label>{{ trans('message.FirstName') }}</label>
                    @if(!empty($userinfo['firstname']))
                    <span class="form-control" style='line-height: 35px;'>{{@$userinfo['firstname']}}</span>
                    @else
                    <input type="text" class="form-control"  id='firstname' name='firstname' placeholder="{{ trans('message.FirstName') }}" value='{{(!empty($userinfo['firstname']))?$userinfo['firstname']:Input::old('firstname')}}'>
                    @endif                   
                </div>
                <div class="col-xs-12 form-group {{ $errors->first('lastname', 'has-error')}}">
                    <label>{{ trans('message.LastName') }}</label>
                    @if(!empty($userinfo['lastname']))
                    <span class="form-control" style='line-height: 35px;'>{{@$userinfo['lastname']}}</span>
                    @else
                    <input type="text" class="form-control" id='lastname' name='lastname' placeholder="{{ trans('message.LastName') }}" value='{{(!empty($userinfo['lastname']))?$userinfo['lastname']:Input::old('lastname')}}'>
                    @endif                    
                </div>
                <div class="col-md-12">
                    <div class="col-xs-8 padd-left form-group {{ $errors->first('street', 'has-error')}}">
                        <label>{{ trans('message.Street') }}</label>
                        @if(!empty($userdetail->street))
                        <span class="form-control" style='line-height: 35px;'>{{@$userdetail->street}}</span>
                        @else
                        <input type="text" name='street' id='street' class="form-control" placeholder="{{ trans('message.Street') }}" value='{{(!empty($userinfo['street']))?$userinfo['street']:Input::old('street')}}'>
                        @endif                        
                    </div>	
                    <div class="col-xs-4 padd form-group {{ $errors->first('housenumber', 'has-error')}}">
                        <label>{{ trans('message.No') }}</label>
                        @if(!empty($userdetail->housenumber))
                        <span class="form-control" style='line-height: 35px;'>{{@$userdetail->housenumber}}</span>
                        @else
                        <input type="text" class="form-control" id='housenumber' name='housenumber' placeholder="{{ trans('message.No') }}" value='{{(!empty($userinfo['housenumber']))?$userinfo['housenumber']:Input::old('housenumber')}}'>
                        @endif                        
                    </div>
                </div>
                <div class="col-xs-12 form-group {{ $errors->first('phone', 'has-error')}}">
                    <label>{{ trans('message.Phone') }}</label>
                    @if(!empty($userinfo['phone']))
                    <span class="form-control" style='line-height: 35px;'>{{@$userinfo['phone']}}</span>
                    @else
                    <input type="text" class="form-control" id='phone' name='phone' placeholder="{{ trans('message.Phone') }}" value='{{(!empty($userinfo['phone']))?$userinfo['phone']:Input::old('phone')}}'>
                    @endif                    
                </div>
                <div class="col-xs-12 form-group {{ $errors->first('postalcode', 'has-error')}}">
                    <label>{{ trans('message.PostalCode') }}</label>
                    @if(!empty($userinfo['postalcode']))
                    <span class="form-control" style='line-height: 35px;'>{{@$userinfo['postalcode']}}</span>
                    @else
                    <input type="text" class="form-control" id='postalcode' name='postalcode' placeholder="{{ trans('message.PostalCode') }}" value='{{(!empty($userinfo['postalcode']))?$userinfo['postalcode']:Input::old('postalcode')}}'>
                    @endif                    
                </div>                
                <div class="col-xs-12 form-group {{ $errors->first('city', 'has-error')}}">
                    <label>{{ trans('message.City') }}</label>
                    @if(!empty($userinfo['city']))
                    <span class="form-control" style='line-height: 35px;'>{{@$userinfo['city']}}</span>
                    @else
                    <input type="text" class="form-control" id='city' name='city'  placeholder="{{ trans('message.City') }}" value='{{(!empty($userinfo['city']))?$userinfo['city']:Input::old('city')}}'>
                    @endif                    
                </div>
                <div class="col-xs-12 form-group {{ $errors->first('country', 'has-error')}}">
                    <label>{{ trans('message.Country') }}</label>
                    <div class="ctyslct">
                        @if(!empty($userinfo['country']))
                        <?php
                        $countryinfo = App\Countries::find($userinfo['country']);
                        ?>
                        <span class="form-control" style='line-height: 35px;'>{{@$countryinfo['name']}}</span>
                        @else
                        <select class="form-control" id='country' name='country'  >
                            @if(!empty($countries))
                            <option selected="" value=''>Select Country</option>
                            @foreach($countries as $objCountry)                           
                            @if(@Input::old('country') == @$objCountry['id'] || @$userinfo['country'] == @$objCountry['id'])
                            <option value='{{$objCountry['id']}}' selected='selected'>{{$objCountry['name']}}</option>
                            @else
                            <option value='{{$objCountry['id']}}'>{{$objCountry['name']}}</option>
                            @endif
                            @endforeach
                            @endif                            
                        </select>
                        @endif                        
                    </div>
                </div>                
                <div class="col-md-12 form-group">
                    <label>{{ trans('message.DateofBirth') }}</label>
                    @if(!empty($userinfo['dob']))
                    <span class="form-control" style='line-height: 35px;'>{{(!empty($userinfo['dob']))?date('d M, Y', strtotime($userinfo['dob'])):Input::old('dob')}}</span>
                    @else
                    <input id="dob" name="dob" type="text" class="form-control jquery-datepicker" placeholder="{{ trans('message.DateFormat') }}" value='{{(!empty($userinfo['dob']))?$userinfo['dob']:Input::old('dob')}}'>
                    @endif                    
                    <a class="calicn" href="javascript:void(0)"><i class="fa fa-calendar" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-12 form-group">
                    <label>{{ trans('message.MemberSince') }}</label>
                    @if(!empty($userinfo['membersince']))
                    <span class="form-control" style='line-height: 35px;'>{{date('d M, Y', strtotime($userinfo['membersince']))}}</span>
                    @else
                    <input id="membersince" name="membersince" type="text" class="form-control jquery-datepicker" placeholder="{{ trans('message.DateFormat') }}" value='{{(!empty($userinfo['membersince']))?$userinfo['membersince']:Input::old('membersince')}}'>                    
                    @endif                    
                    <a class="calicn" href="javascript:void(0)"><i class="fa fa-calendar" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-12 form-group">
                    <label>{{ trans('message.DriverSince') }}</label>
                    @if(!empty($userinfo['driversince']))
                    <span class="form-control" style='line-height: 35px;'>{{(!empty($userinfo['driversince']))?date('d M, Y', strtotime($userinfo['driversince'])):Input::old('driversince')}}</span>
                    @else
                    <input id="driversince" name="driversince" type="text" class="form-control jquery-datepicker" placeholder="{{ trans('message.DateFormat') }}" value='{{(!empty($userinfo['driversince']))?$userinfo['driversince']:Input::old('driversince')}}'>                    
                    @endif                    
                    <a class="calicn" href="javascript:void(0)"><i class="fa fa-calendar" aria-hidden="true"></i></a>
                </div>
            </div><!-- End left panel -->           
            <div class="col-md-8 col-sm-12 rgtbarnw drvprf wow slideInRight" data-wow-duration="0.8s" data-wow-delay="0.5s">
                <div class="diaggrm2 wow zoomIn" data-wow-duration="0.8s" data-wow-delay="0s">
                    <img src="{{ asset('public/images/diaggrm2.png') }}" alt=""/>
                </div>
                <div class="row">                    
                    <h5 style="margin-top:146px;">{{ trans('message.CarPictures') }}</h5>
                    <div class="uplimgs">
                        <div class="row">
                            <div class="col-sm-3 col-xs-3">
                                <div class="imgs1">
                                    <span>
                                        @if(!empty($userdetail->image1))
                                        <?php $path = route('home') . "/public/uploads/users/" . $userdetail->image1; ?>
                                        <img style="width:200px;height: 160px;" src="{{ $path }}" alt=""/>
                                        @else
                                        <img style="width:200px;height: 160px;"  class="img-responsive" src="{{ asset('public/images/imgs1.jpg') }}" alt=""/>
                                        @endif                                        
                                    </span>
                                    <div class="imupbtn">
                                        <img src="{{ asset('public/images/imupbtn.jpg') }}" alt=""/>
<!--                                        <input name="car_picture[]" type="file" value=""/>-->
                                        <input name="image1" class="upload_car_pic" type="file" value=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-3">
                                <div class="imgs1">
                                    <span>
                                        @if(!empty($userdetail->image2))
                                        <?php $path = route('home') . "/public/uploads/users/" . $userdetail->image2; ?>
                                        <img style="width:200px;height: 160px;" src="{{ $path }}" alt=""/>
                                        @else
                                        <img style="width:200px;height: 160px;"  class="img-responsive" src="{{ asset('public/images/imgs1.jpg') }}" alt=""/>
                                        @endif 
                                    </span>
                                    <div class="imupbtn">
                                        <img src="{{ asset('public/images/imupbtn.jpg') }}" alt=""/>
<!--                                        <input name="car_picture[]" type="file" value=""/>-->
                                        <input name="image2" class="upload_car_pic" type="file" value=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-3">
                                <div class="imgs1">
                                    <span>
                                        @if(!empty($userdetail->image3))
                                        <?php $path = route('home') . "/public/uploads/users/" . $userdetail->image3; ?>
                                        <img style="width:200px;height: 160px;" src="{{ $path }}" alt=""/>
                                        @else
                                        <img style="width:200px;height: 160px;"  class="img-responsive" src="{{ asset('public/images/imgs1.jpg') }}" alt=""/>
                                        @endif
                                    </span>
                                    <div class="imupbtn">
                                        <img src="{{ asset('public/images/imupbtn.jpg') }}" alt=""/>
<!--                                        <input name="car_picture[]" type="file" value=""/>-->
                                        <input name="image3" class="upload_car_pic" type="file" value=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-3">
                                <div class="imgs1">
                                    <span>
                                        @if(!empty($userdetail->image4))
                                        <?php $path = route('home') . "/public/uploads/users/" . $userdetail->image4; ?>
                                        <img style="width:200px;height: 160px;" src="{{ $path }}" alt=""/>
                                        @else
                                        <img style="width:200px;height: 160px;"  class="img-responsive" src="{{ asset('public/images/imgs1.jpg') }}" alt=""/>
                                        @endif 
                                    </span>
                                    <div class="imupbtn">
                                        <img src="{{ asset('public/images/imupbtn.jpg') }}" alt=""/>
                                        <input name="image4" class="upload_car_pic" type="file" value=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- End upload images -->
                    <h5>{{ trans('message.Items') }}</h5>
                    <div class="table-responsive text-center bypftbl items-class" >
                        <table class="table">
                            <thead>
                                <tr>
                                    <th style="width:10%"></th>
                                    <th style="width:36%">{{ trans('message.LOCATION') }}</th>
                                    <th style="width:36%">{{ trans('message.DESTINATION') }}</th>
                                    <th style="width:10%">{{ trans('message.OFFER') }}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="tableinhdtp">
                                    <td></td>
                                    <td>
                                        <table class="tableinhd">
                                            <thead>
                                                <tr>
                                                    <th>{{ trans('message.City') }}</th>
                                                    <th>{{ trans('message.PostalCode') }}</th>
                                                    <th>{{ trans('message.Country') }}</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </td>
                                    <td>
                                        <table class="tableinhd">
                                            <thead>
                                                <tr>
                                                     <th>{{ trans('message.City') }}</th>
                                                    <th>{{ trans('message.PostalCode') }}</th>
                                                    <th>{{ trans('message.Country') }}</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                @if(isset($items) && !empty($items))
                                @foreach($items as $objtem)                                
                                <tr>                                    
                                    <td class="itmimgtd">
                                        <?php
                                        $profile_pic = isset($objtem->item_image1) && !empty($objtem->item_image1) ? $objtem->item_image1 : 'itmimg.png';
                                        if (!file_exists("public/uploads/items/" . $profile_pic)) {
                                            $profile_pic = 'itmimg.png';
                                        }
                                        ?>
                                        <a href="{{ route('user.item-view',$objtem->id) }}"> <span class="itmimg"><img class="driv_prof" src="{{ route('home'). "/public/uploads/items/" . $profile_pic }}" alt = ""/></span></a>
                                    </td>                                    
                                    <td>
                                        <table class = "tblindata">
                                            <tbody style = "text-align: center;">
                                                <tr>
                                                    <td style='width:33%;text-align:center;'>{{ $objtem->from_city }}</td>
                                                    <td style='width:33%;text-align:center;'>{{ $objtem->from_postal_code }}</td>
                                                    <td style='width:33%;text-align:center;'>{{ $countries[$objtem->from_country] }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>

                                    <td>
                                        <table class = "tblindata">
                                            <tbody>
                                                <tr>
                                                    <td style='width:33%;text-align:center;'>{{ $objtem->to_city }}</td>
                                                    <td style='width:33%;text-align:center;'>{{ $objtem->to_postal_code }}</td>
                                                    <td style='width:33%;text-align:center;'>{{ $countries[$objtem->to_country] }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>

                                    <td>{{ $currency[$objtem->currency_type].' '.$objtem->item_price }}</td>                                    
                                    <td class="acticns">
                                        <a href="{{route('item.edit', $objtem->id)}}"><img src="{{ asset('public/images/edticn.png')}}" alt=""></a>
                                        <a data-href="{{route('item_destroy', $objtem->id)}}" class="del_item"><img src="{{ asset('public/images/dlet.png')}}" alt=""></a>
                                    </td>
                                </tr>                                
                                @endforeach
                                @else
                                <tr id="no_item_available">
                                    <td colspan="5">{{ 'No item available.' }}</td>
                                </tr>
                                @endif                                
                            </tbody>
                        </table>                        
                    </div>
                    <h5>{{ trans('message.Trips') }}</h5>                    
                    <div class="table-responsive text-center bypftbl trips-class">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="20%">{{ trans('message.From') }}</th>
                                    <th width="20%">{{ trans('message.To') }}</th>
                                    <th width="26%">{{ trans('message.DATE') }} +/-</th>
                                    <th width="26%">{{ trans('message.STARTTIME') }} +/-</th>
                                    <th width="8%"></th>                                   
                                </tr>
                            </thead>
                            <tbody>                                
                                @if(!empty($trips))
                                @foreach($trips as $objTrips)                                
                                <?php
                                $current_date = strtotime(date('Y-m-d'));
                                $trip_date = strtotime($objTrips['trip_date']);
                                $current_time = date("His");
                                $trip_time = str_replace(':', '', $objTrips['trip_time']);
                                ?>
                                <tr id="row_{{$objTrips['id']}}">
                                    <td>                                            
                                        <span class='tblinfld'>                                            
                                            <span class='form-control' placeholder='{{ trans('message.From') }}'>{{$objTrips['trip_from']}}</span>                                            
                                        </span>
                                    </td>
                                    <td>
                                        <span class='tblinfld'>                                            
                                            <span class='form-control' placeholder='{{ trans('message.To') }}'>{{$objTrips['trip_to']}}</span>                                                                                      
                                        </span>
                                    </td>
                                    <td>
                                        <span class='tblinfld'>                                            
                                            <span class='form-control' placeholder='Barlin'>{{$objTrips['trip_date']}}</span>                                            
                                            <a class='calicn' href='javascript:void(0)'><i class='fa fa-calendar' aria-hidden='true'></i></a>
                                        </span>
                                    </td>
                                    <td>
                                        <span class='tblinfld'>                                            
                                            <span class='form-control' placeholder='Barlin'>{{$objTrips['trip_time']}}</span>                                            
                                            <a class='calicn' href='javascript:void(0)'><i class='fa fa-clock-o' aria-hidden='true'></i></a>
                                        </span>
                                    </td>
                                    <td class="acticns">
                                        <a href="javascript:void(0)" data-tripid="{{$objTrips['id']}}" class="edit_trip"><img src="{{ asset('public/images/edticn.png')}}" alt=""></a>
                                        <a href="javascript:void(0)" data-tripid="{{$objTrips['id']}}" class="del_trip"><img src="{{ asset('public/images/dlet.png')}}" alt=""></a>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr id="no_trip_available">
                                    <td colspan="5">{{ 'No trip available.' }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>                    
                    <div class="col-xs-12 padd form-group">
                        <div class="row">                            
                            <div class="col-xs-6 col-sm-5 askbtn">
                                <a href="{{ route('item.create') }}" id="btn_upload_item" name="btn_upload_item" href="javascript:void(0)">{{ trans('message.UploadanItem') }}</a>                                
                            </div>
                            <div class="col-xs-6 col-sm-5 pull-right accprbtn">
                                <a href="javascript:void(0)" id='my_modal'>{{ trans('message.AddTrip') }}</a>                                
                            </div>
                        </div>                        
                    </div>                    
                </div><!-- End row -->
            </div><!-- End right panel -->
        </form>
    </div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="edit" data-size="16" data-loop="true" data-c="#fff" data-hc="white" id="livicon-47" style="width: 16px; height: 16px;"></i>
                            {{ trans('message.AddTrip') }}
                        </h3>                        
                    </div>
                    <div class="panel-body">
                        <div class="flag-error"></div>
                        <form action="{{route('trip.store')}}" method="POST" role="form" id="frm_addtrip" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id='trip_from' name='trip_from' placeholder="{{ trans('message.From') }}" value='{{(!empty($userinfo['trip_from']))?$userinfo['trip_from']:Input::old('trip_from')}}'>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id='trip_to' name='trip_to' placeholder="{{ trans('message.To') }}" value='{{(!empty($userinfo['trip_to']))?$userinfo['trip_to']:Input::old('trip_to')}}'>
                                    </div>
                                </div>
                            </div>                            
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <select class='form-control' id='country_from' name='country_from'>
                                            <option value=''>{{ trans('message.PleaseSelect') }}</option>
                                            @if(!empty($countries))
                                            @foreach($countries as $objKey => $val)
                                            <option value='{{$objKey}}'>{{$val}}</option>
                                            @endforeach
                                            @endif
                                        </select>                                         
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <select class='form-control' id='country_to' name='country_to'>
                                            <option value=''>{{ trans('message.PleaseSelect') }}</option>
                                            @if(!empty($countries))
                                            @foreach($countries as $objKey => $val)
                                            <option value='{{$objKey}}'>{{$val}}</option>
                                            @endforeach
                                            @endif
                                        </select>                                                                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id='postalcode_from' name='postalcode_from' placeholder="{{ trans('message.PostalCode') }}" value='{{(!empty($userinfo['postalcode_from']))?$userinfo['postalcode_from']:Input::old('postalcode_from')}}'>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id='postalcode_to' name='postalcode_to' placeholder="{{ trans('message.PostalCode') }}" value='{{(!empty($userinfo['postalcode_to']))?$userinfo['postalcode_to']:Input::old('postalcode_to')}}'>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input id="trip_date" name="trip_date" type="text" class="form-control jquery-datepicker" placeholder="{{ trans('message.DateFormat') }}" value='{{(!empty($userinfo['trip_date']))?$userinfo['trip_date']:Input::old('trip_date')}}'>                                        
                                    </div>

                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input id="trip_time" name="trip_time" type="text" class="form-control timepickerbootstrap" placeholder="{{ trans('message.TimeFormat') }}" value='{{(!empty($userinfo['trip_time']))?$userinfo['trip_time']:Input::old('trip_time')}}'>                                        
                                    </div>                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12" style="height: 54px;">
                                    <button type="button" id="btn_add_trip" name="btn_add_trip" class="btn btn-default">{{ trans('message.Submit') }}</button>
                                </div>                                
                            </div>                            
                        </form>
                    </div>
                </div>                
            </div>
        </div>    
    </div><!-- Modal End-->
</div>
<!-- End content -->
<!-- Modal -->
<div id="myModalEdit" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="edit" data-size="16" data-loop="true" data-c="#fff" data-hc="white" id="livicon-47" style="width: 16px; height: 16px;"></i>
                            Edit Trip
                        </h3>                        
                    </div>
                    <div class="panel-body">
                        <div class="flag-error"></div>
                        <form action="{{route('trip.update')}}" method="POST" role="form" id="frm_edittrip" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="trip_id" id="trip_id" value="" />
                            <input type="hidden" name="_method" value="PUT" />
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id='edit_trip_from' name='trip_from' placeholder="Trip From" value='{{(!empty($userinfo['trip_from']))?$userinfo['trip_from']:Input::old('trip_from')}}'>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id='edit_trip_to' name='trip_to' placeholder="Trip To" value='{{(!empty($userinfo['trip_to']))?$userinfo['trip_to']:Input::old('trip_to')}}'>
                                    </div>
                                </div>
                            </div>                            
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <select class='form-control' id='edit_country_from' name='country_from'>
                                            <option value=''>Please Select</option>
                                            @if(!empty($countries))
                                            @foreach($countries as $objKey => $val)
                                            <option value='{{$objKey}}'>{{$val}}</option>
                                            @endforeach
                                            @endif
                                        </select>                                         
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <select class='form-control' id='edit_country_to' name='country_to'>
                                            <option value=''>Please Select</option>
                                            @if(!empty($countries))
                                            @foreach($countries as $objKey => $val)
                                            <option value='{{$objKey}}'>{{$val}}</option>
                                            @endforeach
                                            @endif
                                        </select>                                                                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id='edit_postalcode_from' name='postalcode_from' placeholder="Postalcode From" value='{{(!empty($userinfo['postalcode_from']))?$userinfo['postalcode_from']:Input::old('postalcode_from')}}'>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id='edit_postalcode_to' name='postalcode_to' placeholder="Postalcode To" value='{{(!empty($userinfo['postalcode_to']))?$userinfo['postalcode_to']:Input::old('postalcode_to')}}'>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input id="edit_trip_date" name="trip_date" type="text" class="form-control jquery-datepicker" placeholder="dd-mm-yyyy" value='{{(!empty($userinfo['trip_date']))?$userinfo['trip_date']:Input::old('trip_date')}}'>                                        
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input id="edit_trip_time" name="trip_time" type="text" class="form-control timepickerbootstrap" placeholder="HH:MM" value='{{(!empty($userinfo['trip_time']))?$userinfo['trip_time']:Input::old('trip_time')}}'>                                        
                                    </div>                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12" style="height: 54px;">
                                    <button type="button" id="btn_edit_trip" name="btn_edit_trip" class="btn btn-default">Update</button>
                                </div>                                
                            </div>                            
                        </form>
                    </div>
                </div>                
            </div>
        </div>    
    </div><!-- Modal End-->
</div>
<!-- End content -->
@stop
@section('footer_scripts') 
<script type="text/javascript" src="{{ asset('public/js/bootstrap-timepicker.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {    
    $(".del_item").click(function(){
        if(confirm("Are you sure, Do you realy want to delete this item?")){
            window.location.href = $(this).attr('data-href');
        }
    });
    $("body").on("click", ".edit_trip", function(){
        var url = "{{route('tripinfo')}}";
        var trip_id = $(this).attr('data-tripid');
        $.ajax({
            url:url,
            type:'POST',
            dataType:'JSON',
            data:({trip_id:trip_id,_token:"{{csrf_token()}}"}),
            success:function(res){                
                if(res['status']){                    
                    $("#frm_edittrip").find('#trip_id').val(res['data']['id']);
                    $("#frm_edittrip").find('#edit_trip_from').val(res['data']['trip_from']);
                    $("#frm_edittrip").find('#edit_trip_to').val(res['data']['trip_to']);
                    $("#frm_edittrip").find('#edit_country_from').val(res['data']['from_country']);
                    $("#frm_edittrip").find('#edit_country_to').val(res['data']['to_country']);
                    $("#frm_edittrip").find('#edit_postalcode_from').val(res['data']['from_postalcode']);
                    $("#frm_edittrip").find('#edit_postalcode_to').val(res['data']['to_postalcode']);
                    $("#frm_edittrip").find('#edit_trip_date').val(res['data']['trip_date']);
                    $("#frm_edittrip").find('#edit_trip_time').val(res['data']['trip_time']);
                }
            },
            error:function(res){
                alert("System Error!");
            }
        });
        $("#myModalEdit").modal();
    });
    $("body").on("click", ".del_trip", function(){
        if(confirm("Are you sure, Do you realy want to delete this trip?")){
            var url = "{{route('trip_destroy')}}";
            var trip_id = $(this).attr('data-tripid');
            $.ajax({
                url:url,
                type:'POST',
                dataType:'JSON',
                data:({trip_id:trip_id,_token:"{{csrf_token()}}"}),
                success:function(res){ 
                    if(res){                        
                        $(".trips-class tbody #row_" + trip_id).remove();
                    }
                },
                error:function(res){
                    alert("System Error!");
                }
            });
        }
    });    
    $('body').on('change', 'input[type="file"]', function(event) {
        var dd = $(this).parents('.imgs1').parent();
        var src = URL.createObjectURL(event.target.files[0]);
        dd.find('span').find('img').attr('src', src);
        $(".warning-msg").fadeIn();
    });
    $('#my_modal').click(function() {
        $("#myModal").modal();
    });
    $('body').on('focus', ".timepickerbootstrap", function() {
        $(this).timepicker();
    });
    $("#btn_update_driver_profile").click(function() {
        $("#frm_profieupdate").submit();
    });
    $("#btn_add_trip").click(function() {
        $("#frm_addtrip").find('.has-error').removeClass('has-error');        
        $.ajax({
            url: "{{route('trip.store')}}",
            type: "POST",
            dataType: "JSON",
            data: $("#frm_addtrip").serialize(),
            success: function(res) {
                var html = "";
                if (res['status'] == 1) {
                    html += "<tr id='row_"+ res['data']['id'] +"'>";
                    html += "<td>";
                    html += "<input name='trip_id[]' class='form-control' placeholder='Barlin' type='hidden' value='" + res['data']['id'] + "'>";
                    html += "<span class='tblinfld'>";
                    html += "<input name='trip_from[]' class='form-control' placeholder='Barlin' type='text' value='" + res['data']['trip_from'] + "'>";
                    html += "</span>";
                    html += "</td>";
                    html += "<td>";
                    html += "<span class='tblinfld'>";
                    html += "<input name='trip_to[]' class='form-control' placeholder='Munich' type='text' value='" + res['data']['trip_to'] + "'>";
                    html += "</span>";
                    html += "</td>";
                    html += "<td>";
                    html += "<span class='tblinfld'>";
                    html += "<input name='trip_date[]' class='form-control jquery-datepicker' placeholder='mm-dd-yyyy' type='text' value='" + res['data']['trip_date'] + "'>";
                    html += "<a class='calicn' href='javascript:void(0)'><i class='fa fa-calendar' aria-hidden='true'></i></a>";
                    html += "</span>";
                    html += "</td>";
                    html += "<td>";
                    html += "<span class='tblinfld'>";
                    html += "<input name='trip_time[]' class='form-control timepickerbootstrap' placeholder='10:00am' type='text' value='" + res['data']['trip_time'] + "'>";
                    html += "<a class='calicn' href='javascript:void(0)'><i class='fa fa-clock-o' aria-hidden='true'></i></a>";
                    html += "</span>";
                    html += "</td>";
                    html += "<td class='acticns'>";
                    html += "<a href='javascript:void(0)' data-tripid='"+ res['data']['id'] +"' class='edit_trip'><img src='{{ asset('public/images/edticn.png')}}' alt=''></a>";
                    html += "<a href='javascript:void(0)' data-tripid='"+ res['data']['id'] +"' class='del_trip'><img src='{{ asset('public/images/dlet.png')}}' alt=''></a>";
                    html += "</td>";
                    html += "</tr>";
                    $("#frm_addtrip input[type='text']").val("");
                    $("#frm_addtrip select").val("");
                    $(".trips-class tbody #no_trip_available").hide();
                    $(".trips-class tbody").prepend(html);
                    $("#myModal .close").click();
                } else if (res['status'] == 0) {
                    $.each(res['data'], function(key, val) {
                        $('#' + key).parent().addClass('has-error');
                    });
                } else {
                    html += "<div class='flash-message'>";
                    html += "<div class='alert alert-danger'>";
                    html += "<p>" + res['data'] + "</p>";
                    html += "</div>";
                    html += "</div>";
                    $("#frm_addtrip .flag-error").html(html);
                }
            },
            error: function(res) {
                var html = "";
                html += "<div class='flash-message'>";
                html += "<div class='alert alert-danger'>";
                html += "<p>Network Error, Please try after some time!</p>";
                html += "</div>";
                html += "</div>";
                $("#frm_addtrip .flag-error").html(html);
            }
        });
    });
    $("#btn_edit_trip").click(function() {
        $("#frm_edittrip").find('.has-error').removeClass('has-error');        
        $.ajax({
            url: "{{route('trip.update')}}",
            type: "POST",
            dataType: "JSON",
            data: $("#frm_edittrip").serialize(),
            success: function(res) {
                var html = "";
                if (res['status'] == 1) {                    
                    html += "<td>";
                    html += "<input name='trip_id[]' class='form-control' placeholder='Barlin' type='hidden' value='" + res['data']['id'] + "'>";
                    html += "<span class='tblinfld'>";
                    html += "<input name='trip_from[]' class='form-control' placeholder='Barlin' type='text' value='" + res['data']['trip_from'] + "'>";
                    html += "</span>";
                    html += "</td>";
                    html += "<td>";
                    html += "<span class='tblinfld'>";
                    html += "<input name='trip_to[]' class='form-control' placeholder='Munich' type='text' value='" + res['data']['trip_to'] + "'>";
                    html += "</span>";
                    html += "</td>";
                    html += "<td>";
                    html += "<span class='tblinfld'>";
                    html += "<input name='trip_date[]' class='form-control jquery-datepicker' placeholder='mm-dd-yyyy' type='text' value='" + res['data']['trip_date'] + "'>";
                    html += "<a class='calicn' href='javascript:void(0)'><i class='fa fa-calendar' aria-hidden='true'></i></a>";
                    html += "</span>";
                    html += "</td>";
                    html += "<td>";
                    html += "<span class='tblinfld'>";
                    html += "<input name='trip_time[]' class='form-control timepickerbootstrap' placeholder='10:00am' type='text' value='" + res['data']['trip_time'] + "'>";
                    html += "<a class='calicn' href='javascript:void(0)'><i class='fa fa-clock-o' aria-hidden='true'></i></a>";
                    html += "</span>";
                    html += "</td>";
                    html += "<td class='acticns'>";
                    html += "<a href='javascript:void(0)' data-tripid='"+ res['data']['id'] +"' class='edit_trip'><img src='{{ asset('public/images/edticn.png')}}' alt=''></a>";
                    html += "<a href='javascript:void(0)' data-tripid='"+ res['data']['id'] +"' class='del_trip'><img src='{{ asset('public/images/dlet.png')}}' alt=''></a>";
                    html += "</td>";
                    alert("Trip updated successfully");
                    $("#frm_edittrip input[type='text']").val("");
                    $("#frm_edittrip select").val("");                    
                    $(".trips-class tbody #row_" + res['data']['id']).html(html);
                    $("#myModalEdit .close").click();                    
                } else if (res['status'] == 0) {
                    $.each(res['data'], function(key, val) {
                        $('#frm_edittrip #' + key).parent().addClass('has-error');
                    });
                } else {
                    html += "<div class='flash-message'>";
                    html += "<div class='alert alert-danger'>";
                    html += "<p>" + res['data'] + "</p>";
                    html += "</div>";
                    html += "</div>";
                    $("#frm_edittrip .flag-error").html(html);
                }
            },
            error: function(res) {
                var html = "";
                html += "<div class='flash-message'>";
                html += "<div class='alert alert-danger'>";
                html += "<p>Network Error, Please try after some time!</p>";
                html += "</div>";
                html += "</div>";
                $("#frm_addtrip .flag-error").html(html);
            }
        });
    });
    $('body').on('change', 'input[type="file"]', function(event) {    
        var dd = $(this).parents('.uplsec').parent();
        var fd=new FormData();
        var src = URL.createObjectURL(event.target.files[0]);
        fd.append('file',event.target.files[0]);
        fd.append('_token',"{{csrf_token()}}");
        fd.append('userid',"{{$userinfo->id}}");
        fd.append('column_name',$(this).attr('name'));
        dd.find('span').find('img').attr('src', src);
        $.ajax({
            url:"{{route('car_img_upload')}}",
            type:'POST',
            data:fd,
            contentType:false,
            processData:false,
            dataType:'JSON',
            success:function(res){
                if(res['status']){
                    alert("Image uploaded successfully!");
                }
            },
            error:function(){
                
            }
        });
    });
});
</script>
<!-- Write your script here -->
@stop