@extends('admin/layouts/master')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
@stop

{{-- Page content --}}
@section('content')
<style type="text/css">
    .my_profile{ width:25%; height: 140px; margin: 0 0 3% 37%; border: 2px solid burlywood;  border-radius: 50%;}
</style>

<section class="content-header">
    <h1>
        Users Details
        <small>Version 1.0</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>    
<!-- Main content --> <div class="row">  
    <div class="col-xs-12 sgin wow zoomIn" data-wow-duration="1.0s" data-wow-delay="1.0s">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><?php echo ucfirst($users->username); ?></h4>
                </div>
                <div class="modal-body">
                    <div class="image">
                        <?php
                        if (isset($users->userimage) && !empty($users->userimage)) {
                            $path = route('home') . "/public/uploads/users/" . $users->userimage;
                            ?>
                            <img class="my_profile" src="{{ $path }}"><?php
                        } else {
                            $usersProfile = 'usrimg.png';
                            ?>
                            <img class="my_profile" src="{{ asset('public/images/usrimg.png') }}">
<?php } ?>

                    </div>
                    <table class="table table-bordered table-hover">
                        <tbody>
                            <tr>
                                <th>User ID </th>
                                <td><?php echo $users->id; ?></td>
                            </tr>
                            <tr>
                                <th>Name</th> 
                                <td><?php echo isset($users->username) ? $users->username : '-'; ?></td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td><?php echo isset($users->email) ? $users->email : '-'; ?></td>
                            </tr>
                            <tr>
                                <th>Mobile No</th>
                                <td><?php echo isset($users->phone) && !empty($users->phone) ? $users->phone : '-'; ?></td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td><?php echo isset($users->street) && !empty($users->street) ? $users->street : '-'; ?></td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td><?php echo isset($users->status) ? $users->status : '-'; ?></td>
                            </tr>
                            <tr>
                                <th>Created Date</th>
                                <td><?php echo isset($users->created_at) ? $users->created_at : '-'; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <!--<button type="submit" class="btn blue">Save changes</button>-->
                    <a href="#" type="button" class="btn default" onclick="goBack();" data-dismiss="modal">Back</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div><!-- End Services -->
@stop
@section('footer_scripts')
<script type="text/javascript">
    //$(document).ready(function() {
    function goBack() {
        window.history.back();
    }
    //F};
</script>
@stop