@extends('admin/layouts/master')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>
        Language
        <small>Version 1.0</small>
    </h1>
</section>    
<br/>
@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@if (session('flash_alert_notice'))
<div class="flash-message">
    <div class="alert alert-{{session('flash_action')}}">
        <p>{{session('flash_alert_notice')}}</p>
    </div>
</div>
@endif

<!-- Main content -->
<div class="col-xs-12 sgin wow zoomIn" data-wow-duration="1.0s" data-wow-delay="1.0s">
    <div class="row">        
        <style type="text/css">
            hr{
                border-top: 1px solid #090707 !important;
            }
            #userPopup th{
                width:40%;
            }
            #userPopup div.image{
                width:150px;
                height:150px;
                text-align: center;
                margin: 10px auto;
            }
            #userPopup img{
                max-width: 100%;
                max-height: 100%;
                width: 100%;
                height: 100%;
            }
            #userPopup .modal-body{
                padding: 0px 15px !important;
            }

        </style>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!--                    <div class="box-header">
                                            <h3 class="box-title">Customers Record</h3>
                                        </div>-->
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="user_detail" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>English</th>
                                    <th>Germany</th>
                                    <th>French</th>
                                    <th>Spain</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>        
        <div class="modal fade in" id="userPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form name="editFrm" id="editFrm" action="http://megapixels.rt.cisinlive.com/megapixels/admin/admin/savePrice" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Customer Details</h4>
                        </div>
                        <div class="modal-body">

                        </div>
                        <div class="modal-footer">
                            <!--<button type="submit" class="btn blue">Save changes</button>-->
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="container">
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Language Update</h4>
                        </div>
                        <div class="modal-body">
                            <div class="box box-warning">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <form role="form" method="POST" name="lang_form" id="lang_form" action="{{ route('admin.final_cahnge_lang') }}">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>English</label>
                                            <input type="text" class="form-control" name="english" id="english"  placeholder="Enter Language">
                                            <span style="color:red;" id="ens"></span>
                                        </div>
                                        <div class="form-group">
                                            <label>German</label>
                                            <input type="text" class="form-control" name="german" id="german" placeholder="Enter Language">
                                            <span style="color:red;" id="des"></span>
                                        </div>
                                        <div class="form-group">
                                            <label>French</label>
                                            <input type="text" class="form-control" name="french" id="french" placeholder="Enter Language">
                                            <span style="color:red;" id="frs"></span>
                                        </div>
                                        <div class="form-group">
                                            <label>Spain</label>
                                            <input type="text" class="form-control" name="spain" id="spain" placeholder="Enter Language">
                                            <span style="color:red;" id="ess"></span>
                                        </div>
                                        <input type="hidden" id="lng_id" name="lng_id" value="" />
                                        <div class="form-group">
                                            <input type="submit" onclick="return submitForm();" class="form-control" />
                                        </div>
                                    </form>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- End Services -->
@stop
@section('footer_scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var column = ['id', 'en', 'de', 'es', 'fr'];
        var tablename = 'wgn_language';
        //var where="status='1'"
        $('#user_detail').DataTable({
            "bProcessing": true,
            "serverSide": true,
            //"lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
            "order": [[0, "desc"]],
            "ajax": {
                url: "{{ route('admin.users-data') }}", // json datasource
                type: "post", // type of method  ,GET/POST/DELETE
                //data: {col: column, tablename: tablename, where: where}
                data: {col: column, tablename: tablename}
            },
            "columnDefs": [
                {
                    "render": function(data, type, full, meta) {
                        return full[0];
                    },
                    "targets": 0
                },
                {
                    "render": function(data, type, full, meta) {
                        return full[1];
                    },
                    "targets": 1
                },
                {
                    "render": function(data, type, full, meta) {
                        return full[2];
                    },
                    "targets": 2
                },
                {
                    "render": function(data, type, full, meta) {
                        return full[3];
                    },
                    "targets": 3
                },
                {
                    "render": function(data, type, full, meta) {
                        return full[4];
                    },
                    "targets": 4
                },
                {
                    "render": function(data, type, full, meta) {
                        var links = '';
                        links += '<button type="button" onclick="changeData(' + full[0] + ')" class="btn btn-primary btn-xs btn-info" data-toggle="modal" data-target="#myModal" ><i class="fa fa-edit"> Edit</button>';
                        return links;
                    },
                    "targets": 5,
                    "orderable": false
                },
            ]
        });
    });


    function changeData(id) {
        $.ajax({
            url: "{{route('admin.language_update_data')}}",
            dataType: 'JSON',
            type: 'POST',
            data: ({key_id: id, _token: "{{csrf_token()}}"}),
            success: function(res) {
                var en = res.en;
                var de = res.de;
                var es = res.es;
                var fr = res.fr;
                $('#english').val(en);
                $('#german').val(de);
                $('#french').val(es);
                $('#spain').val(fr);
                $('#lng_id').val(id);
            },
            error: function(res) {
                alert("Network Error!");
            }
        });
    }

    function submitForm() {
        var checked = checkValidation();
        if (parseInt(checked) === 1) {
            return false;
        } else {
            return true;
        }
    }



    function checkValidation() {
        var flag = 0;
        var ens = $('#english').val();
        var des = $('#german').val();
        var ess = $('#spain').val();
        var frs = $('#french').val();

        if (ens == '') {
            $('#ens').text('Please enter text!')
            flag = 1;
        } else {
            $('#ens').text('');
        }
        if (des == '') {
            $('#des').text('Please enter text!')
            flag = 1;
        } else {
            $('#des').text('');
        }
        if (ess == '') {
            $('#ess').text('Please enter text!')
            flag = 1;
        } else {
            $('#ess').text('');
        }
        if (frs == '') {
            $('#frs').text('Please enter text!')
            flag = 1;
        } else {
            $('#frs').text('');
        }
        return flag;
    }


</script>
@stop