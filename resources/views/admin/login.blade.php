@extends('admin/layouts/login')

{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
@stop

{{-- Page content --}}
@section('content')

<!-- /.login-logo -->
<div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    @if (session('flash_alert_notice'))
    <div class="flash-message">
        <div class="alert alert-{{session('flash_action')}}">
            {{session('flash_alert_notice')}}
        </div>
    </div>
    @endif
    <form action="{{route('admin.login')}}" class="omb_loginForm"  autocomplete="off" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group has-feedback {{ $errors->first('username', 'has-error') }}">            
            <input type="text" class="form-control" name="username" placeholder="Email/Username" value="{!! Input::old('username') !!}">
        </div>
        <span class="help-block">{{ $errors->first('username', ':message') }}</span>
        <div class="form-group has-feedback {{ $errors->first('password', 'has-error') }}">            
            <input type="password" class="form-control" name="password" placeholder="Password">
        </div>
        <span class="help-block">{{ $errors->first('password', ':message') }}</span>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>        
    </form>
    <!-- /.social-auth-links -->
     <a href="{{route('admin.forgot_pass')}}">I forgot my password</a>
    <!--                <a href="register.html" class="text-center">Register a new membership</a>-->
</div>
<!-- /.login-box-body -->
@stop
@section('footer_scripts')
<!-- Write your script here -->
@stop