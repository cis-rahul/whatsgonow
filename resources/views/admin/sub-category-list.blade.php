@extends('admin/layouts/master')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>
        Sub-Category
        <small>Version 1.0</small>
    </h1>   
</section>    
<!-- Main content -->
<div class="col-xs-12 sgin wow zoomIn" data-wow-duration="1.0s" data-wow-delay="1.0s">
    <div class="row">        
        <style type="text/css">
            hr{
                border-top: 1px solid #090707 !important;
            }
            #userPopup th{
                width:40%;
            }
            #userPopup div.image{
                width:150px;
                height:150px;
                text-align: center;
                margin: 10px auto;
            }
            #userPopup img{
                max-width: 100%;
                max-height: 100%;
                width: 100%;
                height: 100%;
            }
            #userPopup .modal-body{
                padding: 0px 15px !important;
            }

        </style>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="user_detail" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Category Name</th>
                                    <th>Sub Category Name</th>
<!--                                    <th>Created Date</th>-->
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>        
        <div class="modal fade in" id="userPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form name="editFrm" id="editFrm" action="http://megapixels.rt.cisinlive.com/megapixels/admin/admin/savePrice" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Customer Details</h4>
                        </div>
                        <div class="modal-body">

                        </div>
                        <div class="modal-footer">
                            <!--<button type="submit" class="btn blue">Save changes</button>-->
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <input type="hidden" id="cat_id" name="cat_id" value="<?php echo $category_id; ?>">
    </div>
</div><!-- End Services -->
@stop
@section('footer_scripts')
<script type="text/javascript">
    $(document).ready(function () {
        //debugger;
        var catID = $('#cat_id').val();
        if (catID) {
            var column = ['wgn_item_subcategory.id', 'category_name', 'subcat_name', 'wgn_item_subcategory.created_at'];
            var tablename = 'wgn_item_subcategory';
            var where = 'wgn_item_subcategory.category_id = ' + catID;
            var joins = [['INNER JOIN', 'wgn_item_category', 'wgn_item_subcategory.category_id=wgn_item_category.id']];
        } else {
            var column = ['wgn_item_subcategory.id', 'category_name', 'subcat_name', 'wgn_item_subcategory.created_at'];
            var tablename = 'wgn_item_subcategory';
            var joins = [['INNER JOIN', 'wgn_item_category', 'wgn_item_subcategory.category_id=wgn_item_category.id']];
        }
//        var column = ['wgn_item_subcategory.id', 'category_name', 'subcat_name', 'wgn_item_subcategory.created_at'];
//        var tablename = 'wgn_item_subcategory';
//        //var where="status='1'"
//        var joins = [['INNER JOIN', 'wgn_item_category', 'wgn_item_subcategory.category_id=wgn_item_category.id']];
        $('#user_detail').DataTable({
            "bProcessing": true,
            "serverSide": true,
            //"lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
            "order": [[0, "desc"]],
            "ajax": {
                url: "{{ route('admin.users-data') }}", // json datasource
                type: "post", // type of method  ,GET/POST/DELETE
                //data: {col: column, tablename: tablename, where: where}
                data: {col: column, tablename: tablename, join: joins, where: where}
            },
            "columnDefs": [
                {
                    "render": function (data, type, full, meta) {
                        return full[0];
                    },
                    "targets": 0
                },
                {
                    "render": function (data, type, full, meta) {
                        return full[1];
                    },
                    "targets": 1
                },
                {
                    "render": function (data, type, full, meta) {
                        return full[2];
                    },
                    "targets": 2
                },
//                {
//                    "render": function (data, type, full, meta) {
//                        return full[3];
//                    },
//                    "targets": 3
//                },
                /*{
                 "render": function(data, type, full, meta) {
                 return full[4];
                 },
                 "targets": 4
                 },*/
                {
                    "render": function (data, type, full, meta) {
                        var links = '';

                        var updateUrl = '';
                        var edit_url = "{{url('admin/edit-categories')}}";
                        updateUrl = edit_url + "/" + full[0] + "_catwo";


                        var url = "{{url('admin/second-sub-category-list')}}";
                        url = url + "/" + full[0];
                        links += '<a href="' + updateUrl + '" class="btn btn-primary btn-xs btn-info" title="Update Category"><i class="fa fa-edit"></i>Edit</a>&nbsp;';
                        links += '<a href="' + url + '" class="btn btn-primary btn-xs btn-info" title="Order Details"><i class="fa fa-search"></i> Show Details</a>&nbsp;';
                        return links;
                    },
                    "targets": 3,
                    "orderable": false
                },
            ]
        });
    });
</script>
@stop