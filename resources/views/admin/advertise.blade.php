@extends('admin/layouts/master')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
@stop

{{-- Page content --}}
@section('content')
<style type="text/css">
    .my_profile{ width:25%; height: 140px; margin: 0 0 3% 37%; border: 2px solid burlywood;  border-radius: 50%;}
</style>

<section class="content-header">
    <h1>
        Change Image & Link
        <small>Version 1.0</small>
    </h1>
</section>    
<!-- Main content --> <div class="row">  
    @if (session('flash_alert_notice'))
        <div class="flash-message">
            <div class="alert alert-{{session('flash_action')}}">
                <p>{{session('flash_alert_notice')}}</p>
            </div>
        </div>
        @endif
    <div class="col-xs-12 sgin wow zoomIn" data-wow-duration="1.0s" data-wow-delay="1.0s">
        <div class="modal-dialog">
            <form  id="change_img_link" name="change_img_link"  action="{{ route('admin.change-img-link') }}"  method="POST" enctype="multipart/form-data" > 
                <div class="modal-content">
                    <div class="modal-body">
                        <p style="color: red;"><b>Note :</b>Please choose image size 872x158 </p>
                        <b><p>Change Image :</p></b>
                        <input type="file" name="advertise_img" id="advertise_img" />
                        <span style="color:red;" id="img"></span>
                    </div>
                    <div class="modal-body">
                        <b><p>Change Link :</p></b>
                        <input type="text" name="advertise_link" id="advertise_link"  style="width:50%;" placeholder="www.google.com"/><br>
                        <span style="color:red;" id="link"></span>
                    </div>
                    <div class="modal-body">
                        <button type="submit" name="submit" id="submit" onclick="return submitForm();">UPDATE</button>
                    </div>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div><!-- End Services -->
@stop
@section('footer_scripts')
<script type="text/javascript">
    function submitForm() {
        var checked = checkValidation();
        if(parseInt(checked) === 1){
            return false;
        }else{
            return true;
        }
    }

    function checkValidation() {
        var flag = 0;
        var img = $('#advertise_img').val();
        var link = $('#advertise_link').val();
        if (img == '') {
            $('#img').text('Please select the image first!')
            flag = 1;
        }else{
            $('#img').text('');
        }
        if (link == '') {
            $('#link').text('Please enter link!')
            flag = 1;
        }else{
            $('#link').text('');
        }
        return flag;
    }

</script>
@stop