@extends('admin/layouts/master')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
@stop

{{-- Page content --}}
@section('content')
<style type="text/css">
    .my_profile{ width:25%; height: 140px; margin: 0 0 3% 37%; border: 2px solid burlywood;  border-radius: 50%;}
    .bug{color:red;}
</style>

<section class="content-header">
    <h1>
        Add Categories
        <small>Version 1.0</small>
    </h1>
</section>    
<!-- Main content --> <div class="row">  
    <div class="col-xs-12 sgin wow zoomIn" data-wow-duration="1.0s" data-wow-delay="1.0s">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <form action="{{route('admin.add_category')}}" method="post">
                <div class="modal-body">
                    <table class="table table-bordered table-hover">
                        <tbody>
                            <tr>
                                <th>Category</th>
                                <td>
                                    <input style="width: 100%" type="text" id="first_cat" name="first_cat" placeholder="Please enter category" />
                                    <span id="fcats" class="bug"></span>
                                </td>
                            </tr>
                            <tr>
                                <th>Sub-Category</th> 
                                <td>
                                    <input type="text" style="width: 100%" id="second_cat" name="second_cat" placeholder="Please enter sub category" />
                                    <span id="scats" class="bug"></span>
                                </td>
                            </tr>
                            <tr>
                                <th>Sub-Category-Two</th> 
                                <td>
                                    <input style="width: 100%" type="text" id="third_cat" name="third_cat" placeholder="Please enter second sub category" />
                                    <span id="tcats" class="bug"></span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <input style="margin: 0 0 0 44%;" onclick="return submitForm();"  type="submit" value="ADD">
                </div>
                </form>
                <div class="modal-footer">                   
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div><!-- End Services -->
@stop
@section('footer_scripts')
<script type="text/javascript">
    function submitForm() {
        var checked = checkValidation();
        if(parseInt(checked) === 1){
            return false;
        }else{
            return true;
        }
    }

    function checkValidation() {
        var flag = 0;
        var fcat = $('#first_cat').val();
        var scat = $('#second_cat').val();
        var tcat = $('#third_cat').val();
        if (fcat == '') {
            $('#fcats').text('Please enter category!')
            flag = 1;
        }else{
            $('#fcats').text('');
        }
        if (scat == '') {
            $('#scats').text('Please enter category!')
            flag = 1;
        }else{
            $('#scats').text('');
        }
        if (tcat == '') {
            $('#tcats').text('Please enter category!')
            flag = 1;
        }else{
            $('#tcats').text('');
        }
        return flag;
    }

</script>
@stop