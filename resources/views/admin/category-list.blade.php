@extends('admin/layouts/master')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>
        Category
        <small>Version 1.0</small>
    </h1>
</section>    
    <!-- Main content -->
    <div class="col-xs-12 sgin wow zoomIn" data-wow-duration="1.0s" data-wow-delay="1.0s">
        <div class="row">        
            
             @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if (session('flash_alert_notice'))
                <div class="flash-message">
                    <div class="alert alert-{{session('flash_action')}}">
                        <p>{{session('flash_alert_notice')}}</p>
                    </div>
                </div>
                @endif
            
            
            <style type="text/css">
                hr{
                    border-top: 1px solid #090707 !important;
                }
                #userPopup th{
                    width:40%;
                }
                #userPopup div.image{
                    width:150px;
                    height:150px;
                    text-align: center;
                    margin: 10px auto;
                }
                #userPopup img{
                    max-width: 100%;
                    max-height: 100%;
                    width: 100%;
                    height: 100%;
                }
                #userPopup .modal-body{
                    padding: 0px 15px !important;
                }

            </style>
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!--                    <div class="box-header">
                                                <h3 class="box-title">Customers Record</h3>
                                            </div>-->
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="user_detail" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Category Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>        
            <div class="modal fade in" id="userPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form name="editFrm" id="editFrm" action="http://megapixels.rt.cisinlive.com/megapixels/admin/admin/savePrice" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Customer Details</h4>
                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <!--<button type="submit" class="btn blue">Save changes</button>-->
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!--
                public function show_datatable() {
                    $this->isLoggedIn();
                    $dt = new DataTable();
                    $data = $dt->fetch_data();
                    echo json_encode($data);
                }-->
        </div>
    </div><!-- End Services -->
    @stop
    @section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            var column = ['id', 'category_name'];
            var tablename = 'wgn_item_category';
            //var where="status='1'"
            $('#user_detail').DataTable({
                "bProcessing": true,
                "serverSide": true,
                //"lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
                "order": [[0, "desc"]],
                "ajax": {
                    url: "{{ route('admin.users-data') }}", // json datasource
                    type: "post", // type of method  ,GET/POST/DELETE
                    //data: {col: column, tablename: tablename, where: where}
                    data: {col: column, tablename: tablename}
                },
                "columnDefs": [
                    {
                        "render": function (data, type, full, meta) {
                            return full[0];
                        },
                        "targets": 0
                    },
                    {
                        "render": function (data, type, full, meta) {
                            return full[1];
                        },
                        "targets": 1
                    },
                    {
                        "render": function (data, type, full, meta) {
                            var links = '';
                            var updateUrl = '';
                            var edit_url = "{{url('admin/edit-categories')}}";
                            updateUrl = edit_url + "/" + full[0] + "_catone";
                            
                            var url = "{{url('admin/sub-category-list')}}";
                            url = url + "/" + full[0];
                            links += '<a href="' + updateUrl + '" class="btn btn-primary btn-xs btn-info" title="Update Category"><i class="fa fa-edit"></i>Edit</a>&nbsp;';
                            links += '<a href="' + url + '" class="btn btn-primary btn-xs btn-info" title="Category"><i class="fa fa-search"></i> Show Details</a>&nbsp;';
                            return links;
                        },
                        "targets": 2,
                        "orderable": false
                    },
                ]
            });
        });
    </script>
    @stop