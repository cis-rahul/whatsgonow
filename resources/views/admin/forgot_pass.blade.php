@extends('admin/layouts/login')

{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
@stop

{{-- Page content --}}
@section('content')

<!-- /.login-logo -->
<div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    @if (session('flash_alert_notice'))
    <div class="flash-message">
        <div class="alert alert-{{session('flash_action')}}">
            {{session('flash_alert_notice')}}
        </div>
    </div>
    @endif
    <form action="{{route('admin.forgot_pass')}}" class="omb_loginForm"  autocomplete="off" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group has-feedback {{ $errors->first('email', 'has-error') }}">            
            <input type="text" class="form-control" name="email" placeholder="Enter Register Email ID" value="{!! Input::old('email') !!}">
        </div>
        <span class="help-block">{{ $errors->first('email', ':message') }}</span>        
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
        </div>        
    </form>
    <!-- /.social-auth-links -->
    <a href="{{route('admin.login')}}">Login</a>    
</div>
<!-- /.login-box-body -->
@stop
@section('footer_scripts')
<!-- Write your script here -->
@stop