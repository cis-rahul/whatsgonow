@extends('layouts/home')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
<style type='text/css'>
    .accprbtn button {
        background: #ffaf42;
        border-radius: 5px;
        color: #000;
        display: inline-block;
        font-family: "montserratregular";
        font-size: 18px;
        height: 49px;
        line-height: 49px;
        margin: 0;
        text-decoration: none;
        text-shadow: 1px 1px 1px #a0a0a0;
        width: 100%;
        text-align: center;
    }
    .has-error .form-control {
        border-color: #a94442 !important;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    }
</style>
@stop
{{-- Page content --}}
@section('content')
<!-- Start content -->
<div class="container">

    <div class="col-md-12 wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0s">
        <div class="title-area">
            <h2 class="title">{{ trans('message.ContactUsHeading') }}</h2>
            <span class="line"></span>
        </div>
    </div>
    <div class="regfrm">
         @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('flash_alert_notice'))
        <div class="flash-message">
            <div class="alert alert-{{session('flash_action')}}">
                <p>{{session('flash_alert_notice')}}</p>
            </div>
        </div>
        @endif

        <form action="{{route('user.contact')}}" method="POST" role="form" id="frm_contact">
            <div class="col-sm-12 col-md-7 col-lg-6 rgfrmin wow zoomIn" data-wow-duration="0.8s" data-wow-delay="0.5s">
                <div class="col-md-12 form-group">
                    <label>{{ trans('message.Name') }}</label>
                    <input type="text" class="form-control" placeholder="{{ trans('message.EnterYourName') }}" name="cname">
                </div>
                <div class="col-md-12 form-group">
                    <label>{{ trans('message.Email') }}</label>
                    <input type="text" class="form-control" placeholder="{{ trans('message.EnterYourEmailAddress') }}" name="cemail">
                </div>

                <div class="col-md-12 form-group">
                    <label>{{ trans('message.PhoneNo') }}</label>
                    <input type="text" class="form-control" placeholder="{{ trans('message.EnterYourPhoneNumber') }}" name="cphone">
                </div>

                <div class="col-md-12 form-group">
                    <label>{{ trans('message.MESSAGE') }}</label>
                    <textarea class="form-control" name="comment" placeholder="{{ trans('message.EnterYourMessage') }}"></textarea>
                </div>

                <div class="col-md-12 accprbtn">
                    <button>Submit</button>
                </div>
            </div><!-- End left panel -->
        </form>
    </div>
</div>
 
</div>
<!-- End content -->
@stop

@section('footer_scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_profile_update").click(function() {
            $("#frm_profieupdate").submit();
        });
    });
    $('body').on('change', 'input[type="file"]', function(event) {
        var dd = $(this).parents('.uplsec').parent();
        var src = URL.createObjectURL(event.target.files[0]);
        dd.find('span').find('img').attr('src', src);
    });
</script>
<!-- Write your script here -->
@stop