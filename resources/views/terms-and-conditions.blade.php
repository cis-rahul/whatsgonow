@extends('layouts/master')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

@stop

{{-- Page content --}}
@section('content')
<style type="text/css">
    .abutin{text-align: left !important; padding: 4%; font-size: 14px;}
</style>
<section class="abotsection">
    <div class="col-lg-10 col-md-11 col-sm-12 abutin wow flipInX" data-wow-duration="1.5s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 1.5s; animation-delay: 0.8s; animation-name: flipInX;">
        
               <div class="col-xs-12 serin abutin">
                        <ul>
                            <li>{{ trans('message.term1') }}</li>
                            <li>{{ trans('message.term2') }}</li>
                            <li>{{ trans('message.term3') }}</li>
                            <li>{{ trans('message.term4') }}</li>
                            <li>{{ trans('message.term5') }}</li>
                            <li>{{ trans('message.term6') }}</li>
                            <li>{{ trans('message.term6a') }}</li>
                            <li>{{ trans('message.term6b') }}</li>
                            <li>{{ trans('message.term6c') }}</li>
                            <li>{{ trans('message.term6d') }}</li>
                            <li>{{ trans('message.term6e') }}</li>
                            <li>{{ trans('message.term6f') }}</li>
                            <li>{{ trans('message.term6g') }}</li>
                            <li>{{ trans('message.term6h') }}</li>
                            <li>{{ trans('message.term7') }}</li>
                            <li>{{ trans('message.term8') }}</li>
                            <li>{{ trans('message.term9') }}</li>
                            <li>{{ trans('message.term10') }}</li>
                            <li>{{ trans('message.term11') }}</li>
                            <li>{{ trans('message.term12') }}</li>
                            <li>{{ trans('message.term13') }}</li>
                        </ul>
                    </div>
               

    </div>



</section>


@stop
@section('footer_scripts')

@stop
