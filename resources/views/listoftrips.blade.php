@extends('layouts/home')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
<link href="{{ asset('public/css/datepicker.css') }}" rel="stylesheet">
<link href="{{ asset('public/css/bootstrap-timepicker.min.css') }}" rel="stylesheet">
<style type='text/css'>
    .driv_prof {  border-radius: 20%;  height:50px;}
    .pager li > a, .pager li > span {width: 100px;height: 41px;border-radius:5px;text-align:center;margin-top:-8px;padding-top:9px;}
    .pager {text-align: right; margin-right: 15px;}
</style>
@stop
{{-- Page content --}}
@section('content')
<!-- Start content -->
<section id="">
    <!-- Start content -->
    <div class="container">
        <div class="col-md-12 wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0s">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session('flash_alert_notice'))
            <div class="flash-message">
                <div class="alert alert-{{session('flash_action')}}">
                    <p>{{session('flash_alert_notice')}}</p>
                </div>
            </div>
            @endif
            <div class="title-area">
                <h2 class="title">List of Trips</h2>
                <span class="line"></span>
            </div>
            <div class="lstfiltr" style="position: static;">
                <!--<a href="javascript:void(0)" class="fltrtxt"><img src="{{'public/images/filtricon.png'}}" alt=""/> Filter</a>-->
                <div class="lstfiltr" style="position: static;">
                    <a href="javascript:void(0)" class="fltrtxt"><img src="{{'public/images/filtricon.png'}}" alt=""/> Filter</a>
                    <div class="fltdpdwn">
                        <form method="get" action="{{ route('user.trip-list') }}" name="filter_item">
                            <input type="hidden" name="filterData" value="12" />
                            <div class="col-xs-12 form-group">
                                <label class="custom-control custom-checkbox">
                                    <input value="search_location" checked name="checkbox" class="custom-control-input" type="radio">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Search by location</span>
                                </label>
                                <div class="col-xs-12 padd">
                                    <div class="col-md-4 form-group">
                                        <input type="text" class="form-control"  name="location_postalCode" id="location_postalCode" placeholder="Postal Code">
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <input type="text" class="form-control" name="location_city" id="location_city" placeholder="City">
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <div class="ctyslct">
                                            <select class="form-control" name="location_country" id="location_country"  >
                                                @if(!empty($countries))
                                                <option selected="" value=''>Select Country</option>
                                                @foreach($countries as $key=>$objCountry)                           
                                                @if(@Input::old('country') == @$objCountry['id'] || @$userinfo['country'] == @$objCountry['id'])
                                                <option value='{{$key}}' selected='selected'>{{$objCountry}}</option>
                                                @else
                                                <option value='{{$key}}'>{{$objCountry}}</option>
                                                @endif
                                                @endforeach
                                                @endif                            
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="custom-control custom-checkbox">
                                    <input value="search_destination" name="checkbox" class="custom-control-input" type="radio">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Search by Destination</span>
                                </label>
                                <div class="col-xs-12 padd">
                                    <div class="col-md-4 form-group">
                                        <input type="text" name="destination_postalCode" id="destination_postalCode" class="form-control" placeholder="Postal Code">
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <input type="text" name="destination_city" id="destination_city" class="form-control" placeholder="City">
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <div class="ctyslct">
                                            <select class="form-control" name="destination_country" id="destination_country">
                                                @if(!empty($countries))
                                                <option selected="" value=''>Select Country</option>
                                                @foreach($countries as $key=>$objCountry)                           
                                                @if(@Input::old('country') == @$objCountry['id'] || @$userinfo['country'] == @$objCountry['id'])
                                                <option value='{{$key}}' selected='selected'>{{$objCountry}}</option>
                                                @else
                                                <option value='{{$key}}'>{{$objCountry}}</option>
                                                @endif
                                                @endforeach
                                                @endif                           
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="custom-control custom-checkbox">
                                    <input value="search_date" name="checkbox" class="custom-control-input" type="radio">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Search by Date</span>
                                </label>
                                <div class="col-xs-12 padd">
                                    <div class="col-md-4 form-group">
                                        <input type="text" name="filter_Date" id="filter_Date" class="form-control" placeholder="mm-dd-yyyy">
                                        <a class="calicn" href="javascript:void(0)"><i class="fa fa-calendar" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>

<!--                            <div class="col-sm-8 col-xs-12 form-group">
                                <label class="custom-control custom-checkbox">
                                    <input value="search_amount" name="checkbox" class="custom-control-input" type="radio">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Search by ammount</span>
                                </label>
                                <div class="col-xs-12 padd">
                                    <div class="col-md-6 form-group">
                                        <input type="text" name="filter_amount" id="filter_amount" class="form-control" placeholder="Enter Ammount">
                                    </div>
                                </div>
                            </div>-->
                            <button type="submit" name="filterSearch" style="display:none;" id="filterSearch">Go</button>
                            <a href="javascript:void(0)" onclick="submitForm('filterSearch')" class="fltgobtn" data-toggle="modal">Go</a>

                        </form>
                    </div><script>
                        function submitForm(elementTrigger) {
                            $('#' + elementTrigger).trigger('click');
                        }
                    </script>
                </div>
            </div>
        </div>
        <div class="col-md-12 lstfltxt">

            <div class="col-sm-12 rgtbarnw wow zoomIn" data-wow-duration="0.8s" data-wow-delay="0s">
                <div class="row">

                    <div class="table-responsive text-center bypftbl">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Driver</th>
                                    <th>Location</th>
                                    <th>Destination</th>

                                </tr>
                            </thead>

                            <tbody>
                                <tr class="tableinhdtp">
                                    <td></td>
                                    <td>
                                        <table class="tableinhd">
                                            <thead>
                                                <tr>
                                                    <th>city</th>
                                                    <th>postal code</th>
                                                    <th>country</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </td>

                                    <td>
                                        <table class="tableinhd">
                                            <thead>
                                                <tr>
                                                    <th>city</th>
                                                    <th>postal code</th>
                                                    <th>country</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </td>


                                </tr>
                                <?php
                                $dataArr = array();
                                ?>

                                <?php $idsArr = array(); ?>
                                @if(!$trips->isEmpty())
                                @foreach($trips as $objtem)
                                <?php
                                if (in_array($objtem->id, $idsArr))
                                    continue;
                                $dataArr[] = $objtem;
 
                                $idsArr[] = $objtem->id;
                                ?>
                                <tr id="<?php echo $objtem->id; ?>">                                    
                                    <td class="itmimgtd">
<?php $profile_pic = isset($objtem->userimage) && !empty($objtem->userimage) ? $objtem->userimage : 'itmimg.png'; ?>
                                        <a href="{{ route('user.driver-detail', $objtem->userid) }}"> <span class = "itmimg"><img class="driv_prof" src="{{ route('home'). "/public/uploads/users/" . $profile_pic }}" alt=" " title="{{ $objtem->firstname }}"/></span></a>
                                    </td>                                    
                                    <td>
                                        <table class = "tblindata">
                                            <tbody style = "text-align: center;">
                                                <tr>
                                                    <td style='width:33%;text-align:center;'>{{ $objtem->trip_from }}</td>
                                                    <td style='width:33%;text-align:center;'>{{ $objtem->from_postalcode }}</td>
                                                    <td style='width:33%;text-align:center;'>{{ $countries[$objtem->from_country] }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>

                                    <td>
                                        <table class = "tblindata">
                                            <tbody>
                                                <tr>
                                                    <td style='width:33%;text-align:center;'>{{ $objtem->trip_to }}</td>
                                                    <td style='width:33%;text-align:center;'>{{ $objtem->to_postalcode }}</td>
                                                    <td style='width:33%;text-align:center;'>{{ $countries[$objtem->to_country] }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>



                                </tr>
                                @endforeach

                                @else
                                <tr>
                                    <td colspan="3">{{ 'No trips available.' }}</td>
                                </tr>
                                @endif
                            </tbody>

                        </table>
                    </div>

                    <div class = "fltbtns">
                        <div class = "row">
                            <div class = "col-xs-12 col-sm-3 accprbtn">
                                <?php
                                $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                                $url = str_replace('trip-list?', 'trip-list-map?', $url);
                                ?>
                                <a href = "<?php echo $url; ?>"><img src = "public/images/mapicn.png" alt = ""/> Map View</a>
                            </div>

                            <div class = "col-sm-7 col-xs-12 nxbcbtn">                                
<?php echo $trips->appends(Input::all())->render() ?>
                            </div>
                        </div>
                    </div>                    
                </div><!--End row -->
            </div><!--End right panel -->
        </div>
    </div>
    <!--End content -->
    <?php
    $dataArr = json_encode($dataArr);
    ?>
</section>
<!--End content -->
@stop
@section('footer_scripts')
<script>
    var dataArr = JSON.parse('<?php echo $dataArr; ?>');
    console.log(dataArr);
    $(document).ready(function() {
        var s = $("header");
        var pos = s.position();
        $(window).scroll(function() {
            var windowpos = $(window).scrollTop();
            if (windowpos >= pos.top & windowpos <= 10) {
                s.removeClass("stick");
            } else {
                s.addClass("stick");
            }
        });
    });

</script>

<script>
    $(document).ready(function() {
        $(".fltrtxt").click(function() {
            $(".fltdpdwn").toggle();
        });
        $("li a[rel='prev']").html("Back").css('background-color', '#000').css('color', '#ffffff').css('font-weight', 'bold');
        $("li a[rel='next']").html("Next").css('background-color', '#ffaf42').css('color', '#ffffff').css('font-weight', 'bold');
        $("li.disabled span").hide();
    });


    $(function() {
        $("#filter_Date").datepicker({
            minDate: new Date(),
            showButtonPanel: true,
            changeMonth: true,
            changeYear: true,
            changeYear:true,
                    dateFormat: 'yy-mm-dd'
                    //yearRange: "1900:2016"
        });
    });


</script>
<!-- Write your script here -->
@stop
