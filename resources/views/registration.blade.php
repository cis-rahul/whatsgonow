@extends('layouts/home')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
<style type='text/css'>
    .accprbtn button {background: #ffaf42;border-radius: 5px;color: #000;display: inline-block;font-family: "montserratregular";font-size: 18px;height: 49px;line-height: 49px;margin: 0;text-decoration: none;text-shadow: 1px 1px 1px #a0a0a0;width: 100%;text-align: center;}
    .has-error .form-control {border-color: #a94442 !important;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);}
</style>
@stop
{{-- Page content --}}
@section('content')
<!-- Start content -->
<div class="container">
    <div class="col-md-12 wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0s">
        <div class="title-area">
            <h2 class="title">{{ trans('message.RegistrationforNewUsers') }}</h2>
            <span class="line"></span>
        </div>
    </div>
    <div class="regfrm">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('flash_alert_notice'))
        <div class="flash-message">
            <div class="alert alert-{{session('flash_action')}}">
                <p>{{session('flash_alert_notice')}}</p>
            </div>
        </div>
        @endif
        <form action="{{route('user.signup')}}" method="POST" role="form" id="frm_signup" enctype="multipart/form-data">
            <div class="col-md-12 col-lg-9 lftbar rgfrmin wow zoomIn" data-wow-duration="0.8s" data-wow-delay="0.5s">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <input type="hidden" name="registration" id="registration" />
                <input type="hidden" name="currency" id="currency"/>
                <div class="col-xs-12 text-center">
                    <div class="uplsec">
                        <span><img width="135px" height="135px" src="{{ asset('public/images/usrimg.png') }}" alt=""/></span>
                        <div class="uplbtnmn">
                            <div class="uplbtn">
                                <input type="file" name="userimage" id="userimage" value=""/>
                                <img src="{{ asset('public/images/uplbtn1.png') }}" alt=""/>
                            </div>
                        </div>
                    </div>	
                </div><!--upload image-->
                <div class="col-md-6 form-group {{ $errors->first('username', 'has-error')}}">
                    <label>{{ trans('message.UserName') }}</label>
                    <input type="text" class="form-control required" id='username' name='username' placeholder="{{ trans('message.EnterUserName') }}" value='{{Input::old('username')}}' tabindex="1">
                    
                </div>
                <div class="col-md-6 form-group {{ $errors->first('password', 'has-error')}}">
                    <label>{{ trans('message.Password') }}</label>
                    <input type="password" class="form-control required" id='password' name='password' placeholder="{{ trans('message.Password') }}" tabindex="5">
                    
                </div>
                <div class="col-md-6 form-group {{ $errors->first('firstname', 'has-error')}}">
                    <label>{{ trans('message.FirstName') }}</label>
                    <input type="text" class="form-control required" id='firstname' name='firstname' placeholder="{{ trans('message.EnterFirstName') }}" value='{{Input::old('firstname')}}' tabindex="2">
                    
                </div>
                <div class="col-md-6 form-group {{ $errors->first('password_confirmation', 'has-error')}}">
                    <label>{{ trans('message.ConfirmPassword') }}</label>
                    <input type="password" class="form-control required" id='password_confirmation' name='password_confirmation' placeholder="{{ trans('message.ConfirmPassword') }}" tabindex="6">
                    
                </div>                
                <div class="col-md-6 form-group {{ $errors->first('lastname', 'has-error')}}">
                    <label>{{ trans('message.LastName') }}</label>
                    <input type="text" class="form-control required"  id='lastname' name='lastname' placeholder="{{ trans('message.EnterLastName') }}" value='{{Input::old('lastname')}}' tabindex="3">
                    
                </div>
                <div class="col-md-6 form-group {{ $errors->first('city', 'has-error')}}">
                    <label>{{ trans('message.City') }}</label>
                    <input type="text" class="form-control required" id='city' name='city'  placeholder="{{ trans('message.City') }}" value='{{Input::old('city')}}' tabindex="7">
                    
                </div>
                <div class="col-md-6 form-group {{ $errors->first('email', 'has-error')}}">
                    <label>{{ trans('message.Email') }}</label>
                    <input type="text" class="form-control required" id='email' name='email'  placeholder="{{ trans('message.EnterEmailAddress') }}" value='{{Input::old('email')}}' tabindex="4">
                    
                </div>
                <div class="col-md-6 form-group {{ $errors->first('country', 'has-error')}}">
                    <label>{{ trans('message.Country') }}</label>
                    <div class="ctyslct">
                        <select class="form-control required counrties" id='country' name='country'  tabindex="8">
                            @if(!empty($countries))
                            <option selected="" value="">{{ trans('message.PleaseSelect') }}</option>
                            @foreach($countries as $key=>$objCountry)                           
                            @if(@Input::old('country') == $key)
                                <option value='{{$key}}' selected='selected'>{{$objCountry}}</option>
                            @else
                                <option value='{{$key}}'>{{$objCountry}}</option>
                            @endif
                            @endforeach
                            @endif                            
                        </select>
                    </div>                    
                </div>
                <div class="col-md-6 form-group {{ $errors->first('phone', 'has-error')}}">
                    <label>{{ trans('message.Phone') }}</label>
                    <div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
                        <div class="input-group">
                            <span class="input-group-addon country_code">
                                +91
                            </span>
                            <input type="text" class="form-control required" id='phone' name='phone' placeholder="{{ trans('message.EnterYourPhoneNumber') }}" value='{{Input::old('phone')}}' tabindex="5">
                        </div>
                    </div>                    
                </div>
                <div class="col-md-6 form-group {{ $errors->first('postalcode', 'has-error')}}">
                    <label>{{ trans('message.PostalCode') }}</label>
                    <input type="text" class="form-control required" id='postalcode' name='postalcode' placeholder="{{ trans('message.PostalCode') }}" value='{{Input::old('postalcode')}}' tabindex="9">                    
                </div>
                <div class="col-md-6 form-group agrytrms agr2">
                    <label class="custom-control custom-checkbox">
                        <?php $termandcare = ""; ?>
                        @if(Input::old('checkbox'))
                            <?php $termandcare = "checked='checked'"; ?>                            
                        @endif
                        <input value="1" name="checkbox" id="checkbox" class="custom-control-input required" type="checkbox" <?php echo $termandcare ?> tabindex="10">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">{{ trans('message.Terms&Conditions') }}</span>
                    </label>
                </div>                
                <div class="col-xs-12 form-group prcsec rgnwus">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 accprbtn">                                             
                            <a href="javascript:void(0)" id='btn_registration' name='btn_registration'>{{ trans('message.Submit') }}</a>
                        </div>
                        <div class="col-xs-12 col-sm-6 askbtn">
                            <a href="javascript:void(0)" id='btn_register_driver' name='btn_register_driver'>{{ trans('message.BecomeaDriver') }}</a>
                        </div>
                    </div>
                </div>
            </div><!-- End left panel -->
        </form>
    </div>
</div>
<!-- End content -->
@stop

@section('footer_scripts')
<script type="text/javascript">
    $(document).ready(function(){        
        $("#btn_registration").click(function(){
            if($("#checkbox").is(":checked")){
                $("#registration").val('1');
                $("#frm_signup").submit();
            }else{
                alert("Please accept Term & Condition, It's mandatory");
            }
        });
        $("#btn_register_driver").click(function(){            
            if($("#checkbox").is(":checked")){
                $("#registration").val('2');
                $("#frm_signup").submit();
            }else{
                alert("Please accept Term & Condition, It's mandatory");
            }
        });
        //$("#frm_signup").validationEngine({maxErrorsPerField: 1});
    });
    $('body').on('change','input[type="file"]', function(event){        
        var dd= $(this).parents('.uplsec').parent();
        var src = URL.createObjectURL(event.target.files[0]);
        dd.find('span').find('img').attr('src',src);                
    });
</script>
<!-- Write your script here -->
@stop