@extends('layouts/home')
{{-- Page title --}}
@section('title')
<!-- Write your page title here -->
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!-- Write your css here -->
<style type='text/css'>
    .accprbtn button {background: #ffaf42;border-radius: 5px;color: #000;display: inline-block;font-family: "montserratregular";height: 49px;line-height: 49px;margin: 0;text-decoration: none;text-shadow: 1px 1px 1px #a0a0a0;width: 100%;text-align: center;}
    .has-error .form-control {border-color: #a94442 !important;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);}
</style>
@stop
{{-- Page content --}}
@section('content')

<!-- Start content -->
<div class="container">
    <div class="col-md-12 wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0s">
        <div class="title-area">
            <h2 class="title">{{ trans('message.UploadItem') }}</h2>
            <span class="line"></span>
        </div>
    </div>
    <div class="col-md-12 byfrmot">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if (session('flash_alert_notice'))
        <div class="flash-message">
            <div class="alert alert-{{session('flash_action')}}">
                <p>{{session('flash_alert_notice')}}</p>
            </div>
        </div>
        @endif
        <?php
        if (empty($items['id'])) {
            $action = route('item.store');
            $method = "";
            $itemid = "";
            $actionType = "<input type='hidden' name='actionType' id='actionType' value='Create'/>";
        } else {
            $action = route('item.update', $items['id']);
            $itemid = "<input type='hidden' name='item_id' value='{$items['id']}' />";
            $method = "<input type='hidden' name='_method' id='_method' value='PUT'/>";
            $actionType = "<input type='hidden' name='actionType' id='actionType' value='Edit'/>";
        }
        ?>              
        <form action="{{$action}}" method="POST" role="form" id="frm_items" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />            
            <?php echo $method;
            echo $itemid;
            echo $actionType; ?>
            <div class="col-md-9 col-sm-12 rgtbarnw upldfrm wow zoomIn" data-wow-duration="0.8s" data-wow-delay="0s">
                <div class="row">
                    <div class="uplimgs">
                        <div class="col-sm-3 col-xs-3">
                            <div class="imgs1">
                                <?php
                                $pathimg = asset('public/images/imgs1.jpg');
                                if (!empty($items['item_image1'])) {
                                    if (file_exists("public/uploads/items/" . $items['item_image1'])) {
                                        $pathimg = route('home') . "/public/uploads/items/" . $items['item_image1'];
                                    }
                                }
                                ?>
                                <span><img style="width:180px;height:180px;" class="img-responsive" src="{{ $pathimg }}" alt=""/></span>
                                <div class="imupbtn">                                    
                                    <img src="{{ asset('public/images/imupbtn.jpg') }}" alt=""/>
                                    <input type="file" value="" name="item_image1"/>                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="imgs1">
                                <?php
                                $pathimg = asset('public/images/imgs1.jpg');
                                if (!empty($items['item_image2'])) {
                                    if (file_exists("public/uploads/items/" . $items['item_image2'])) {
                                        $pathimg = route('home') . "/public/uploads/items/" . $items['item_image2'];
                                    }
                                }
                                ?>
                                <span><img style="width:180px;height:180px;" class="img-responsive" src="{{ $pathimg }}" alt=""/></span>
                                <div class="imupbtn">
                                    <img src="{{ asset('public/images/imupbtn.jpg') }}" alt=""/>
                                    <input type="file" value="" name="item_image2"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="imgs1">
                                <?php
                                $pathimg = asset('public/images/imgs1.jpg');
                                if (!empty($items['item_image3'])) {
                                    if (file_exists("public/uploads/items/" . $items['item_image3'])) {
                                        $pathimg = route('home') . "/public/uploads/items/" . $items['item_image3'];
                                    }
                                }
                                ?>
                                <span><img style="width:180px;height:180px;" class="img-responsive" src="{{ $pathimg }}" alt=""/></span>
                                <div class="imupbtn">
                                    <img src="{{ asset('public/images/imupbtn.jpg') }}" alt=""/>
                                    <input type="file" value="" name="item_image3"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="imgs1">
                                <?php
                                $pathimg = asset('public/images/imgs1.jpg');
                                if (!empty($items['item_image4'])) {
                                    if (file_exists("public/uploads/items/" . $items['item_image4'])) {
                                        $pathimg = route('home') . "/public/uploads/items/" . $items['item_image4'];
                                    }
                                }
                                ?>
                                <span><img style="width:180px;height:180px;" class="img-responsive" src="{{ $pathimg }}" alt=""/></span>
                                <div class="imupbtn">
                                    <img src="{{ asset('public/images/imupbtn.jpg') }}" alt=""/>
                                    <input type="file" value="" name="item_image4"/>
                                </div>
                            </div>
                        </div>
                    </div><!-- End upload images -->
                    <div class="mesuresec">
                        <div class="col-xs-4 col-sm-2 form-group {{ $errors->first('measures', 'has-error')}}">
                            <label>Measures</label>
                            <div class="ctyslct">                                
                                <?php $measures = empty($items['measures']) ? (!empty(Input::old('measures')) ? Input::old('measures') : "") : $items['measures']; ?>
                                <select class="form-control" id="measures" name="measures">
                                    <option value="">{{ trans('message.PleaseSelect') }}</option>
                                    <option value="1" <?php echo ($measures == '1') ? "selected='selected'" : "" ?>>CM</option>
                                    <option value="2" <?php echo ($measures == '2') ? "selected='selected'" : "" ?>>Inch</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-2 form-group {{ $errors->first('item_length', 'has-error')}}">
                            <label>{{ trans('message.Length') }}</label>                            
                            <input type="text" class="form-control numeric" placeholder="{{ trans('message.Length') }}" id="item_length" name="item_length" value="{{(!empty($items['item_length']))?$items['item_length']:Input::old('item_length')}}">
                        </div>
                        <div class="col-xs-4 col-sm-2 form-group {{ $errors->first('item_height', 'has-error')}}">
                            <label>{{ trans('message.High') }}</label>
                            <input type="text" class="form-control numeric" placeholder="{{ trans('message.High') }}" id="item_height" name="item_height" value="{{(!empty($items['item_height']))?$items['item_height']:Input::old('item_height')}}">
                        </div>
                        <div class="col-xs-4 col-sm-2 form-group {{ $errors->first('item_width', 'has-error')}}">
                            <label>{{ trans('message.Width') }}</label>
                            <input type="text" class="form-control numeric" placeholder="{{ trans('message.Width') }}" id="item_width" name="item_width" value="{{(!empty($items['item_width']))?$items['item_width']:Input::old('item_width')}}">
                        </div>
                        <div class="col-xs-4 col-sm-2 form-group {{ $errors->first('item_weight', 'has-error')}}">
                            <label>{{ trans('message.Weight') }}</label>
                            <input type="text" class="form-control numeric" placeholder="{{ trans('message.Weight') }}" id="item_weight" name="item_weight" value="{{(!empty($items['item_weight']))?$items['item_weight']:Input::old('item_weight')}}">
                        </div>
                        <div class="col-xs-4 col-sm-2 form-group {{ $errors->first('item_pieces', 'has-error')}}">
                            <label>{{ trans('message.Pieces') }}</label>
                            <input type="text" class="form-control" placeholder="0-100" id="item_pieces" name="item_pieces" value="{{(!empty($items['item_pieces']))?$items['item_pieces']:Input::old('item_pieces')}}">
                        </div>
                    </div><!--Measurement-->
                    <div class="ctgrys">
                        <div class="col-xs-12 col-sm-4 form-group {{ $errors->first('product_name', 'has-error')}}">
                            <label>{{ trans('message.ProductName') }}</label>
                            <input type="text" class="form-control required" placeholder="{{ trans('message.ProductName') }}" id="product_name" name="product_name" value="{{(!empty($items['product_name']))?$items['product_name']:Input::old('product_name')}}">
                        </div>
                        <div class="col-xs-12 col-sm-4 form-group {{ $errors->first('item_value', 'has-error')}}">
                            <label>{{ trans('message.SelectProductValue') }}</label>
                            <div class="ctyslct">
                                <select class="form-control required" id="item_value" name="item_value">
                                    <option value="">{{ trans('message.SelectProductValue') }}</option>
                                    @if(!empty($itemvalue))
                                    @foreach($itemvalue as $objItemValue)
                                    @if($objItemValue->id == Input::old('item_value') || $objItemValue->id == @$items['item_value'])
                                    <option value="{{$objItemValue->id}}" selected="selected">{{$objItemValue->value}}</option>
                                    @else
                                    <option value="{{$objItemValue->id}}">{{$objItemValue->value}}</option>
                                    @endif
                                    @endforeach
                                    @endif                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 form-group {{ $errors->first('currency_type', 'has-error')}}">
                            <label>{{ trans('message.Currency') }}</label>
                            <div class="ctyslct">                                
                                @if(!empty($currency))
                                @foreach($currency as $objCurrency)
                                @if($objCurrency->id == $userinfo->currency)
                                <input type="text" class="form-control" value="{{$objCurrency->currency_type}}" readonly="readonly"/>
                                @endif 
                                @endforeach
                                @endif                                                                                    
                                <input type="hidden" id="currency_type" name="currency_type" value="{{$userinfo->currency}}"/>
                            </div>
                        </div>
                    </div><!--category-->
                    <div class="ctgrys">
                        <div class="col-xs-12 col-sm-4 form-group {{ $errors->first('category_id', 'has-error')}}">
                            <label>{{ trans('message.Category') }}</label>
                            <div class="ctyslct">
                                <select class="form-control required" id="category_id" name="category_id">
                                    <option value="">{{ trans('message.PleaseSelect') }}</option>
                                    @if(!empty($item_category))                                    
                                    @foreach($item_category as $objItemCategory)
                                    @if($objItemCategory->id == Input::old('category_id') || $objItemCategory->id == @$items['category_id'])
                                    <option value="{{$objItemCategory->id}}" selected="selected">{{$objItemCategory->category_name}}</option>
                                    @else
                                    <option value="{{$objItemCategory->id}}">{{$objItemCategory->category_name}}</option>
                                    @endif
                                    @endforeach
                                    @endif                                            
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 form-group {{ $errors->first('subcategory_id', 'has-error')}}">
                            <label>{{ trans('message.SubCategoryOne') }}</label>
                            <div class="ctyslct">
                                <select class="form-control" id="subcategory_id" name="subcategory_id">
                                    <option value="">{{ trans('message.PleaseSelect') }}</option>                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 form-group {{ $errors->first('second_subcat_id', 'has-error')}}">
                            <label>{{ trans('message.SubCategoryTwo') }}</label>
                            <div class="ctyslct">
                                <select class="form-control" id="second_subcat_id" name="second_subcat_id">
                                    <option value="">{{ trans('message.PleaseSelect') }}</option>
                                </select>
                            </div>
                        </div>
                    </div><!--category-->
                    <div class="ctgrys">                        
                        <div class="col-xs-12 col-sm-6 form-group {{ $errors->first('seller_phone_number', 'has-error')}}">
                            <label>{{ trans('message.PickUpPlaceNumber') }}</label>
                            <div class="ctyslct">
                                <input type="text" class="form-control required" placeholder="{{ trans('message.No') }}" id="seller_phone_number" name="seller_phone_number" value="{{(!empty($items['seller_phone_number']))?$items['seller_phone_number']:Input::old('seller_phone_number')}}">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 form-group {{ $errors->first('seller_email', 'has-error')}}">
                            <label>{{ trans('message.PickUpEmail') }}</label>
                            <div class="ctyslct">
                                <input type="text" class="form-control required" placeholder="Email ID" id="seller_email" name="seller_email" value="{{(!empty($items['seller_email']))?$items['seller_email']:Input::old('seller_email')}}">
                            </div>
                        </div>
                    </div><!--category-->
                    <div class="col-xs-12 form-group {{ $errors->first('item_description', 'has-error')}}">
                        <label>{{ trans('message.ItemDescription') }}</label>
                        <textarea class="form-control required" placeholder="{{ trans('message.ItemDescription') }}" id="item_description" name="item_description">{{(!empty($items['item_description']))?$items['item_description']:Input::old('item_description')}}</textarea>
                    </div>

                    <div class="col-xs-12 form-group frmsec">
                        <label>{{ trans('message.From') }}</label>
                        <div class="row frmsec">
                            <div class="col-xs-12 col-sm-2 form-group {{ $errors->first('from_street_number', 'has-error')}}">
                                <input type="text" class="form-control" placeholder="{{ trans('message.No') }}" id="from_street_number" name="from_street_number" value="{{(!empty($items['from_street_number']))?$items['from_street_number']:Input::old('from_street_number')}}">
                            </div>
                            <div class="col-xs-12 col-sm-3 form-group {{ $errors->first('from_street', 'has-error')}}">
                                <input type="text" class="form-control  required" placeholder="{{ trans('message.Street') }}" id="from_street" name="from_street" value="{{(!empty($items['from_street']))?$items['from_street']:Input::old('from_street')}}">
                            </div>                           
                            <div class="col-xs-12 col-sm-2 form-group {{ $errors->first('from_postal_code', 'has-error')}}">
                                <input type="text" class="form-control  required" placeholder="{{ trans('message.PostalCode') }}" id="from_postal_code" name="from_postal_code" value="{{(!empty($items['from_postal_code']))?$items['from_postal_code']:Input::old('from_postal_code')}}">
                            </div>
                            <div class="col-xs-12 col-sm-2 form-group {{ $errors->first('from_city', 'has-error')}}">
                                <input type="text" class="form-control  required" placeholder="{{ trans('message.City') }}" id="from_city" name="from_city" value="{{(!empty($items['from_city']))?$items['from_city']:Input::old('from_city')}}">
                            </div>
                            <div class="col-xs-12 col-sm-3 form-group {{ $errors->first('from_country', 'has-error')}}" >
                                <div class="ctyslct">
                                    <select class="form-control  required" id="from_country" name="from_country">
                                        <option value="">{{ trans('message.PleaseSelect') }}</option>
                                        @if(!empty($countries))
                                        @foreach($countries as $objCountry)
                                        @if($objCountry->id == Input::old('from_country') || $objCountry->id == @$items['from_country'])
                                        <option value="{{$objCountry->id}}" selected="selected">{{$objCountry->name}}</option>
                                        @else
                                        <option value="{{$objCountry->id}}">{{$objCountry->name}}</option>
                                        @endif                                        
                                        @endforeach
                                        @endif                                            
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 form-group frmsec">
                        <label>To</label>
                        <div class="row frmsec">
                            <div class="col-xs-12 col-sm-2 form-group {{ $errors->first('to_street_number', 'has-error')}}">
                                <input type="text" class="form-control" placeholder="Number" id="to_street_number" name="to_street_number" value="{{(!empty($items['to_street_number']))?$items['to_street_number']:Input::old('to_street_number')}}">
                            </div>
                            <div class="col-xs-12 col-sm-3 form-group {{ $errors->first('to_street', 'has-error')}}">
                                <input type="text" class="form-control  required" placeholder="Street" id="to_street" name="to_street" value="{{(!empty($items['to_street']))?$items['to_street']:Input::old('to_street')}}">
                            </div>
                            <div class="col-xs-12 col-sm-2 form-group {{ $errors->first('to_postal_code', 'has-error')}}">
                                <input type="text" class="form-control  required" placeholder="{{ trans('message.PostalCode') }}" id="to_postal_code" name="to_postal_code" value="{{(!empty($items['to_postal_code']))?$items['to_postal_code']:Input::old('to_postal_code')}}">
                            </div>
                            <div class="col-xs-12 col-sm-2 form-group {{ $errors->first('to_city', 'has-error')}}">
                                <input type="text" class="form-control required" placeholder="{{ trans('message.City') }}" id="to_city" name="to_city" value="{{(!empty($items['to_city']))?$items['to_city']:Input::old('to_city')}}">
                            </div>
                            <div class="col-xs-12 col-sm-3 form-group {{ $errors->first('to_country', 'has-error')}}">
                                <div class="ctyslct">
                                    <select class="form-control  required " id="to_country" name="to_country" >
                                        <option value="">{{ trans('message.PleaseSelect') }}</option>
                                        @if(!empty($countries))
                                        @foreach($countries as $objCountry)
                                        @if($objCountry->id == Input::old('to_country') || $objCountry->id == @$items['to_country'])
                                        <option value="{{$objCountry->id}}" selected="selected">{{$objCountry->name}}</option>
                                        @else
                                        <option value="{{$objCountry->id}}">{{$objCountry->name}}</option>
                                        @endif                                         
                                        @endforeach
                                        @endif                                            
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="ofrsec">
                            <h4>{{ trans('message.OFFER') }}</h4>
                            <div class="form-group ofrcrpr">
                                <div class="col-xs-6 {{ $errors->first('item_currency', 'has-error')}}">
                                    <label class="col-xs-4">{{ trans('message.Currency') }}:</label>
                                    <div class="col-xs-8">                                        
                                        @if(!empty($currency))
                                        @foreach($currency as $objCurrency)
                                        @if($objCurrency->id == $userinfo->currency)
                                        <input type="text" class="form-control" value="{{$objCurrency->currency_type}}" readonly="readonly"/>
                                        @endif 
                                        @endforeach
                                        @endif                                                                                    
                                        <input type="hidden" id="item_currency" name="item_currency" value="{{$userinfo->currency}}"/>
                                    </div>
                                </div>
                                <div class="col-xs-6 {{ $errors->first('item_price', 'has-error')}}">
                                    <label class="col-xs-4">{{ trans('message.Price') }}:</label>
                                    <div class="col-xs-8">
                                        <input type="text" class="form-control required" placeholder="00.00" id="item_price" name="item_price" value="{{(!empty($items['item_price']))?$items['item_price']:Input::old('item_price')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="ofrckbx">
                                <div class="col-xs-6 form-group agrytrms">
                                    <label class="custom-control custom-checkbox">
                                        <input value="1" class="custom-control-input required" type="radio" name="offer_type" <?php echo (!empty($items['offer_type']) && $items['offer_type'] == '1') ? "checked='checked'" : ((!empty(Input::old('offer_type')) && Input::old('offer_type') == '1') ? "checked='checked'" : "") ?>>
                                        <span class="custom-control-indicator" style="color:white;"></span>
                                        <span class="custom-control-description ">{{ trans('message.Fix') }}</span>
                                    </label>
                                </div>
                                <div class="col-xs-6 form-group agrytrms">
                                    <label class="custom-control custom-checkbox">
                                        <input value="2" class="custom-control-input required" type="radio" name="offer_type" <?php echo (!empty($items['offer_type']) && $items['offer_type'] == '2') ? "checked='checked'" : ((!empty(Input::old('offer_type')) && Input::old('offer_type') == '2') ? "checked='checked'" : "") ?>>
                                        <span class="custom-control-indicator" style="color:white;"></span>
                                        <span class="custom-control-description">{{ trans('message.Negotiate') }}</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="crtpy wddslcr form-group">                            
                        <div class="col-xs-6 col-sm-6 {{ $errors->first('car_type', 'has-error')}}">                                
                            <label>{{ trans('message.TypeofCarNeeded') }}</label>
                            <select class="form-control  required" id="car_type" name="car_type">
                                <option value="">{{ trans('message.PleaseSelect') }}</option>
                                @if(!empty($cartype))
                                @foreach($cartype as $objCarType)
                                @if($objCarType->id == Input::old('car_type') || $objCarType->id == @$items['car_type'])
                                <option value="{{$objCarType->id}}" selected="selected">{{$objCarType->type}}</option>
                                @else
                                <option value="{{$objCarType->id}}">{{$objCarType->type}}</option>
                                @endif                                
                                @endforeach
                                @endif                                    
                            </select>
                        </div>
                        <div class="col-xs-6 col-sm-6 {{ $errors->first('car_model', 'has-error')}}">                                
                            <label>{{ trans('message.CarModel') }}</label>
                            <select id="car_model" name="car_model" class="form-control">
                                <option value="">{{ trans('message.PleaseSelect') }}</option>
                            </select>
                        </div>                                               
                    </div><!--category-->                    
                    <div class="col-xs-12 form-group prcsec">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 askbtn">
                                <a id="btn_save">{{ trans('message.Save') }}</a>
                            </div>
                            <div class="col-xs-12 col-sm-6 accprbtn">
                                <a href="{{ route('user.driver-list') }}">{{ trans('message.FindaDriver') }}</a>
                            </div>
                        </div>
                    </div>                    
                </div><!-- End row -->
            </div><!-- End right panel -->
        </form>
    </div>
</div>
<!-- End content -->
@stop
@section('footer_scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#item_pieces").focusout(function() {
            var itemPieces = $(this).val();
            if ($.isNumeric(itemPieces)) {
                if (itemPieces < 101 && itemPieces > 0) {
                    return true;
                } else {
                    alert("Please enter only numeric value between Range 1 to 100");
                    $(this).val("");
                    $(this).focus();
                }
            } else {
                alert("Please enter only numeric value between Range 1 to 100");
                $(this).val("");
                $(this).focus();
            }
        });
        var categoryid = "<?php echo empty($items['category_id']) ? Input::old('category_id') : $items['category_id']; ?>";
        var subcategoryid = "<?php echo empty($items['subcategory_id']) ? Input::old('subcategory_id') : $items['subcategory_id']; ?>";
        var secondsubcategoryid = "<?php echo empty($items['second_subcat_id']) ? Input::old('second_subcat_id') : $items['second_subcat_id']; ?>";
        var cartype = "<?php echo empty($items['car_type']) ? Input::old('car_type') : $items['car_type']; ?>";
        var carmodel = "<?php echo empty($items['car_model']) ? Input::old('car_model') : $items['car_model']; ?>";
        $("#btn_save").click(function() {
            $("#frm_items").submit();
        });
        $("#category_id").change(function() {
            $(".loader_div").show();
            $.ajax({
                url: "{{route('subcategory_by_categoryid')}}",
                type: 'POST',
                dataType: 'JSON',
                data: ({_token: "{{csrf_token()}}", cat_id: $(this).val()}),
                success: function(res) {
                    if (res['status']) {
                        var option = "<option value=''>Please Select</option>";
                        $.each(res['data'], function(key, val) {
                            if (subcategoryid == val['id']) {
                                option += "<option value='" + val['id'] + "' selected='selected'>" + val['subcat_name'] + "</option>";
                            } else {
                                option += "<option value='" + val['id'] + "'>" + val['subcat_name'] + "</option>";
                            }
                        });
                        $("#subcategory_id").html(option);
                        if (subcategoryid != "") {
                            $("#subcategory_id").change();
                        }
                    }
                    setTimeout(function() {
                        $(".loader_div").hide();
                    }, 1000);

                },
                error: function(res) {
                    $(".loader_div").hide();
                }
            });
        });

        $("#subcategory_id").change(function() {
            $(".loader_div").show();
            $.ajax({
                url: "{{route('subcategory_by_subcatid')}}",
                type: 'POST',
                dataType: 'JSON',
                data: ({_token: "{{csrf_token()}}", subcat_id: $(this).val()}),
                success: function(res) {
                    if (res['status']) {
                        var option = "<option value=''>Please Select</option>";
                        $.each(res['data'], function(key, val) {
                            if (secondsubcategoryid == val['id']) {
                                option += "<option value='" + val['id'] + "' selected='selected'>" + val['sub_cat_name'] + "</option>";
                            } else {
                                option += "<option value='" + val['id'] + "'>" + val['sub_cat_name'] + "</option>";
                            }
                        });
                        $("#second_subcat_id").html(option);
                    }
                    setTimeout(function() {
                        $(".loader_div").hide();
                    }, 1000);
                },
                error: function(res) {
                    $(".loader_div").hide();
                }
            });
        });
        $("#car_type").change(function() {
            var url = "{{route('carmodel')}}";
            $(".loader_div").show();
            $.ajax({
                url: url,
                data: ({cartypeid: $(this).val(), _token: '{{csrf_token()}}'}),
                type: 'POST',
                dataType: 'JSON',
                success: function(res) {
                    var html = "<option value=''>Please Select</option>";
                    ;
                    if (res['status']) {
                        $.each(res['data'], function(key, val) {
                            if (carmodel == val['id']) {
                                html += "<option value='" + val['id'] + "' selected='selected'>" + val['model'] + "</option>";
                            } else {
                                html += "<option value='" + val['id'] + "'>" + val['model'] + "</option>";
                            }
                        });
                    }
                    $("#car_model").html(html);
                    setTimeout(function() {
                        $(".loader_div").hide();
                    }, 1000);
                },
                error: function(res) {
                    alert('System Error');
                }
            });
        });
        if (categoryid != "") {
            $("#category_id").change();
        }
        if (cartype != "") {
            $("#car_type").change();
        }
    });
    $('body').on('change', 'input[type="file"]', function(event) {
        var dd = $(this).parents('.imgs1').parent();
        var src = URL.createObjectURL(event.target.files[0]);
        dd.find('span').find('img').attr('src', src);
        $(".warning-msg").fadeIn();
    });
</script>
<!-- Write your script here -->
@stop