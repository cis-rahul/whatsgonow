<?php

namespace config\DataTable;

class DataTable {

    function __construct() {
        
    }

    public function fetch_data() {
        $conn = mysqli_connect('localhost', 'root', 'root', 'whatsgonow');
        mysqli_set_charset($conn,"utf8");
        //for live
        //$conn = mysqli_connect('127.0.0.1','p135874d1','whatsgoON!6x','usr_p135874_2');
        //echo "fg";die;
        $params = $columns = $totalRecords = $data = array();
        $params = $_REQUEST;
        $columns = $_REQUEST['col'];
        $column = implode(',', $columns);
        $limit = $orderby = $join = $groupby = $where = $sqlTot = $sqlRec = "";
        // check search value exist
        if (!empty($params['search']['value'])) {
            $where .="WHERE ";
            for ($i = 0, $l = count($columns); $i < $l; ++$i) {
                if ($columns[$i] == 'count(*)') {
                    continue;
                }
                if ($i == 0) {
                    $where .=" ( $columns[$i] LIKE '" . $params['search']['value'] . "%' ";
                } else {
                    $where .=" OR $columns[$i] LIKE '" . $params['search']['value'] . "%' ";
                }
            }
            $where.=")";
        }

        if (isset($_REQUEST['where'])) {
            if ($where != '') {
                $where.=" and ";
            } else {
                $where.=' WHERE ';
            }
            $where.=$_REQUEST['where'];
        }

        if (isset($_REQUEST['join'])) {
            $join.=' INNER JOIN ';
            $i = 1;
            foreach ($_REQUEST['join'] as $key => $value) {
                if ($i > 1) {
                    $join.=' INNER JOIN ';
                }
                //print_r($value);die;
                $join.=$value[1] . ' ON ' . $value[2];
                //echo $join;die;
                $i++;
            }
        }
        if (isset($_REQUEST['groupby'])) {
            $groupby.=" group by " . $_REQUEST['groupby'];
        }
        $table = $_REQUEST['tablename'];
        $orderby = " ORDER BY " . $columns[$params['order'][0]['column']] . "   " . $params['order'][0]['dir'];
        $limit = "  LIMIT " . $params['start'] . " ," . $params['length'] . " ";
        $sqlTot = "SELECT $column FROM $table $join $where $groupby $orderby";
        $sqlRec = $sqlTot . " $limit";
        //die;
        $queryTot = mysqli_query($conn, $sqlTot) or die("database error:" . mysqli_error($conn));
        $totalRecords = mysqli_num_rows($queryTot);

        $queryRecords = mysqli_query($conn, $sqlRec) or die("error to fetch employees data");

//iterate on results row and create new index array of data
        while ($row = mysqli_fetch_row($queryRecords)) {
            $data[] = $row;
        }

        //print_r($data);
        //die;
        $json_data = array(
            "draw" => intval($params['draw']),
            "recordsTotal" => intval($totalRecords),
            "recordsFiltered" => intval($totalRecords),
            "data" => $data   // total data array
        );
        //echo "hii";
        //die;
       // print_r($json_data);
        //echo json_encode($json_data,JSON_UNESCAPED_UNICODE);
        return $json_data;  // send data as json format
    }

}
